\chapter{Conclusion}

This dissertation investigated several strategies for semantic relation extraction based on similarity measures. Such measures are designed to quantify semantic relatedness of lexical units, such as nouns, noun phrases and multiword expressions. These measures assign high scores to pairs of terms in a semantic relation (synonyms, hypernyms or co-hyponyms) and near-zero values to all other pairs. 

The work has brought six key contributions to the field of computational lexical semantics: 

\begin{enumerate}
  \item Section~\ref{similarity-measures-based-on-syntactic-distributional-analysis} of Chapter 1 presented a new similarity measure \textit{SDA-MWE} based on the syntactic distributional analysis and $p$-NN procedure.
The measure performs comparably to the baselines.
In contrast to the common approaches, it can deal with both single words and multiword expressions. 
We compared  relations extracted with this measure with relations of a thesaurus. While 7\% of the extracted relations is explicitly encoded in the thesaurus, 35\%-46\% are implicitly present in the resource via the short paths.

\item Section~\ref{similarity-measure-based-on-definitions} of Chapter 1 presented a method for semantic relation extraction \textit{DefVectors}. It relies on Wiktionary, Wikipedia, $k$-NN, $mk$-NN procedures and the Vector Space Model. 
The method performs comparably to the baselines.
In contrast to the corpus-based techniques, it operates on a small-scale set of definitions.
The proposed technique is implemented in an open source system.~\footnote{\url{https://github.com/jgc128/defvectors}}

\item Section~\ref{similarity-measure-based-on-lexico-syntactic} of Chapter 1 described a novel corpus-based semantic similarity measure \textit{PatternSim}, which makes use of the lexico-syntactic patterns.
The measure performs comparably to the baseline measures.
In contrast to the network-based techniques, it requires no semantic resources such as WordNet or dictionaries. 
Implementation of the  approach has been made available to the community.~\footnote{\url{https://github.com/cental/patternsim}}

\item Chapter 3, presented a large-scale comparative study of 37 baseline similarity measures based on corpora, definitions and semantic networks. 
We go further than most of the surveys and compare the measures with respect to the semantic relation types they provide. 
The main findings of this study are the following.  
The \textit{BDA-3-Cos} and the \textit{SDA-21-Cos} measures provides the best performance among corpus-based measures.
The \textit{Resnik} measure performs best among the network-based measures. 
The \textit{DefVectors-WktWiki} scores best among the definition-based measures. 
The studied measures are heterogeneous in terms of their lexical coverage, performances and semantic relation distributions. There is no single measure that outperforms all others on  all benchmarks.
While the semantic relation distributions of the studied measures differ, all these measures extract many co-hyponyms. 
Evaluation system used in these experiments has been made open source.~\footnote{\url{https://github.com/alexanderpanchenko/sim-eval}}

\item Chapter 4 provided two main contributions:
First, a systematic analysis of 16 baseline measures combined with 9 fusion methods. We are first to propose hybrid similarity measures based on all main types of resources -- text corpora, Web as a corpus, semantic networks, dictionaries and encyclopedias. 
Second, the hybrid supervised semantic similarity measures \textit{Logit-E15}, \textit{C-SVM-radial-E15} and \textit{C-SVM-linear-E15}. They combine 15 baseline measures in a statistical model trained on a set of semantic relations from a dictionary. 
These measures outperform by a large margin both baseline and unsupervised hybrid measures on all the benchmarks.
The key advantages of these measures with respect to the single measures are higher precision (better top results) and recall (better lexical coverage). 

\item Chapter 5 presented two text processing systems, which use the semantic similarity measure \textit{PatternSim}.
The first system lets users discover semantically similar words in an easy and interactive way.~\footnote{\url{http://serelex.cental.be},}~\footnote{\url{http://apps.microsoft.com/webpdp/en-US/app/lsse/48dc239a-e116-4234-87fd-ac90f030d72c},}~\footnote{\url{http://www.windowsphone.com/s?appid=dbc7d458-a3da-42bf-8da1-de49915e0318}} For a given input query, it returns a list of related terms and visualizes them with a graph or a set of images.
The system would not function without a similarity measure.
Implementation of the system  has been made available to the community.~\footnote{\url{https://github.com/pomanob/lsse},}~\footnote{\url{https://github.com/jgc128/serelex4win}}

The second system performs categorization of filenames from the P2P networks to detect child sexual abuse materials. 
In contrast to the first application, in this case, the similarity measure refines the baseline system.
The extracted  relations improve the accuracy of the application with help of the \textit{vocabulary projection} technique. 
Implementation of the system is open source.~\footnote{\url{https://github.com/alexanderpanchenko/stc}}
 
\end{enumerate}

The two main limitations of this thesis are the following:

\begin{enumerate}
  \item We deal only with the data in English with the exception of Section~\ref{similarity-measures-based-on-syntactic-distributional-analysis} that presents experiments with the French data. 
  
  \item The measures discussed in this work did not perform word sense disambiguation~\citep{lesk1986automatic,agirre1996wsd}. They take as input a term and return a set of its related terms. For instance, if the input term is ``python'', then the results would contain both terms related to snakes and programming languages. 
A measure which takes into account word senses, should take into account also context of the term. For instance, if the term ``python'' occurs in a context related to programming languages (such as words ``program'' and ``java''), then the results should contain only terms related to programming.     
 
\end{enumerate}  


Three practical questions regarding the semantic similarity measures are the following: 

\begin{itemize2}
  \item ``Should a given text processing application use a semantic similarity measure?'' 
  \item ``How to integrate a semantic similarity measure into a given application?''
  \item ``Which semantic similarity measure should be used in a given application?''
\end{itemize2}

This thesis mainly deals with the last question. Our general advice is to use the hybrid supervised similarity measures, such as \textit{C-SVM-radial-E15} wherever possible. However, they require various linguistic resources: corpora, dictionaries, free access to Web search engines, semantic networks, training data, etc. If all these components are available for the vocabulary of your application, we advice to use such advanced measures. However, in some domains, such resources  does not exist. In this case, first you should collect as much resources as possible covering the target vocabulary. Next, you should use the measures or a combination of measures, which match the available linguistic resources. For instance, if you have access to a text corpus and a dictionary then you should test a combination of corpus- and dictionary-based measures.  

Therefore, this thesis improved understanding of the existing approaches to semantic similarity and proposed several new ones. These novel techniques perform well according to both extrinsic and intrinsic evaluations. We conclude that the developed measures can be useful in a wide range of natural language processing and information retrieval applications.   

\newpage
Finally, this thesis has identified four prominent directions for the future work:

\begin{enumerate}
\item Applying the proposed measures to the NLP systems dealing with short texts, such as filenames, sentences, abstracts, short messages, tags, tweets or Facebook statuses. Examples of such systems include text categorization systems, text retrieval systems, systems measuring text similarity, machine translation systems, text clustering systems, etc. It is often desirable to enrich representation of the short texts (compare \textit{query expansion} or \textit{vocabulary projection}). One way to do it is to use only synonyms and other semantically related terms.
 
\item Development of the relation-specific similarity measures. Such measures would assign high scores only to hypernyms or hyponyms or synonyms, etc.
For certain applications we may prefer relations of a specific type. For instance, for the \textit{query expansion} we would prefer to use synonyms.
One way to implement such technology is to use the supervised similarity measures proposed in Chapter 4. These measures should be trained on a relation-specific data. Some additional relation-specific features may be also needed.

\item Development of the semantic similarity measures for other languages.
Multiple measures exist for English. However, such tools are not available for many other languages. Porting the measures to other languages may improve NLP applications of those languages. 
Some measures described in this thesis can be straightforwardly applied to other languages, e. g. \textit{BDA, SDA, LSA} and \textit{DefVectors}. The \textit{PatternSim} measure can be also ported to another language. In this case, a reasonable effort is needed to translate the extraction patterns.  

\item Supporting multiword expressions (MWEs) and named entities (NEs).
This work mostly focused on the similarity measures dealing with single nouns, with exception of the technique described in Section~\ref{similarity-measures-based-on-syntactic-distributional-analysis}. However, processing of MWEs and NEs is important for many text processing systems. 
Therefore, it would be interesting to extend the proposed approaches so they fully support MWEs and NEs.
    
\end{enumerate}






