\documentclass[11pt]{article}
\usepackage[utf8]{inputenc} % NEW DEFINITION
\usepackage{acl-hlt2011}
\usepackage{times}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{url}
\usepackage[table]{xcolor} % NEW DEFINITION
\usepackage{natbib} % NEW DEONFINITION
\usepackage{rotating} % NEW DEFINITI
\usepackage[linesnumbered,ruled,vlined,dontprintsemicolon]{algorithm2e} % NEW DEFINITION
\bibpunct{(}{)}{;}{a}{,}{,} % NEW DEFINITION
\newcommand{\gc}{\cellcolor[gray]{0.9}} % NEW DEFINITION
\newcommand{\rc}{\cellcolor[rgb]{1,0,0}} % NEW DEFINITION
\newcommand{\bc}{\cellcolor[rgb]{0,0,1}} % NEW DEFINITION
\newcommand{\grc}{\cellcolor[rgb]{0,0.5,0}} % NEW DEFINITION

\DeclareMathOperator*{\argmax}{arg\,max}
\setlength\titlebox{6.5cm}    % Expanding the titlebox

\title{Comparison of the Baseline Knowledge-, Corpus-, and Web-based Similarity Measures for Semantic Relations Extraction}

\author{Alexander Panchenko \\
  Center for Natural Language Processing (CENTAL)\\ 
	Université catholique de Louvain, Belgium \\
  {\tt alexander.panchenko@student.uclouvain.be} 
 \\}

\date{}

\begin{document}
\maketitle
\begin{abstract}
Unsupervised methods of semantic relations extraction rely on a similarity measure between lexical units. Similarity measures differ both in kinds of information they use and in the ways how this information is transformed into a similarity score. This paper is making a step further in the evaluation of the available similarity measures within the context of semantic relation extraction. We compare 21 baseline measures -- 8 knowledge-based, 4 corpus-based, and 9 web-based metrics with the BLESS dataset. Our results show  that existing similarity measures provide significantly different results, both in general performances and in relation distributions. We conclude  that the results suggest developing a combined similarity measure. 

\end{abstract}

\section{Introduction}

Semantic relations extraction aims to discover meaningful lexico-semantic relations such as synonyms and hyponyms between a given set of lexically expressed concepts. Automatic relations discovery is a subtask of automatic thesaurus construction (see~\citet{grefenstette1994explorations}, and ~\citet{panchenko2010}). 

A set of  semantic relations $R$ between a set of concepts $C$ is a binary relation $R \subseteq C\times T \times C$,  where $T$ is a set of semantic relation types. A relation $r \in R$ is a triple $\left\langle   c_i,t,c_j \right\rangle$ linking two concepts $c_i,c_j \in C$ with a semantic relation of type $t \in T$. We are dealing with six types of semantic relations: hyperonymy, co-hyponymy, meronymy, event (associative), attributes, and random: $T= \{ \textssf{  hyper, coord, mero, event, attri, random } \}$. We describe analytically and compare experimentally methods, which discover set of semantic relations $\hat{R}$ for a given set of concepts $C$. A semantic relation extraction algorithm aims to discover $\hat{R} \sim R$. 

One approach for semantic relations extraction is based on the lexico-syntactic patterns which are constructed either manually~\citep{hearst1992} or semi-automatically~\citep{snow2005learning}. The alternative approach, adopted in this paper, is unsupervised (see e.g.~\citet{lin1998automatic} or~\citet{sahlgren2006}). It relies on a \textit{similarity measure} between lexical units. Various measures are available. We compare 21 baseline measures: 8 knowledge-based, 4 corpus-based, and 9 web-based.  We would like to answer on two questions: ``What metric is most suitable for the unsupervised relation extraction?'', and ``Does various metrics capture the same semantic relations?''.  The second question is particularly interesting for developing of a meta-measure combining several metrics. This information may also help us choose a measure well-suited for a concrete application.  

We extend existing surveys in three ways. First, we ground our comparison on the BLESS dataset\footnote{\url{http://sites.google.com/site/geometricalmodels/sharedevaluation}}, which is open, general, and was never used before for comparing all the considered metrics. Secondly, we face corpus-, knowledge-, and web-based, which was never done before. Thirdly, we go further than most of the comparisons and thoroughly compare the metrics with respect to relation types they provide. We report empirical relation distributions for each measure and check if they are significantly different. Next, we propose a way to find the measures with the most and the least similar relation distributions. Finally, we report information about redundant measures in an original way -- in a form of an undirected graph. 

\section{Methodology}
\subsection{Similarity-based Semantic Relations Discovery}
\label{methods}

We use an unsupervised approach to calculate set of semantic relations $R$ between a given set of concepts $C$ (see algorithm~\ref{alg:unsup}). The \textit{method} uses one of 21 similarity \textit{measures} described in sections 2.2 to 2.4. First, it calculates the concept$\times$concept similarity matrix $\mathbf{S}$  with a  measure $sim$. Since some similarity measures output scores outside the interval $[0;1]$ we transform them with the function $normalize$ as following: $\mathbf{S} \leftarrow \frac{(\mathbf{S}-min(\mathbf{S}))}{max(\mathbf{S})}$. If we deal with a dissimilarity measure, we additionally transform its score $\mathbf{S}$ to similarity as following: $\mathbf{S} \leftarrow 1 - normalize(\mathbf{S}).$ Finally, the function $threshold$ calculates semantic relations $R$ between concepts $C$ with the k-NN thresholding: $\bigcup_{i=1}^{|C|}\left\{\left\langle c_i, t, c_j \right\rangle :  c_j \in \text{ top }k\% \text{ concepts } \wedge s_{ij} \geq \gamma \right\}.$  Here $k$ is the percent of the top similar concepts to a concept $c_{i}$, and $\gamma$ is a small value which ensures than nearly-zero pairwise similarities $s_{ij}$ will be ignored. Thus, the method links each concept $c_i$ with $k\%$ of its nearest neighbours. 
\begin{algorithm}[H]
\SetLine
\KwIn{ Concepts $C$, Sim.parameters $P$, Threshold $k$, Min.similarity value $\gamma$ }
\KwOut{ Unlabeled semantic relations $\hat{R}$}

$\mathbf{S} \leftarrow sim(C,P)$ \;
$\mathbf{S} \leftarrow normalize(\mathbf{S})$ \;
$\hat{R} \leftarrow threshold(\mathbf{S},k, \gamma)$ \;
\Return{ $\hat{R}$ } \;
\caption{Computing semantic relations}
\label{alg:unsup}
\end{algorithm}

Below we list the pairwise similarity measures $sim$ used in our experiments with references to the original papers, where all details can be found. 


\subsection{Knowledge-based Measures}
\label{sect:kb-measures}
The knowledge-based metrics use a hierarchical semantic network in order to calculate similarities. Some of the metrics also use counts derived from a corpus. We evaluate eight knowledge-based measures listed below. Let us describe them in the following notations:  $c_{r}$ is the root concept of the network; $h$ is the height of  the network; $len(c_i,c_j)$ is the length of the shortest path in the network between concepts;  $c_{ij}$ is a lowest common subsumer of concepts $c_i$ and $c_j$; $P(c)$ is the probability of the concept, estimated from a corpus (see below). Then, the Inverted Edge Count measure~\citep[p.~687]{jurafsky2009speech} is
\begin{equation}
s_{ij}=len(c_i,c_j)^{-1};
\end{equation}
\citet{leacock1998} measure is 
\begin{equation}
s_{ij}=-log\frac{len(c_i,c_j)}{2h};
\end{equation}
\cite{resnik1995} measure is 
\begin{equation}
 \label{resnik}
s_{ij}=-log(P(c_{ij}));
\end{equation} 
\citet{jiang1997} measure is 
\begin{equation}
s_{ij}=(     2log(P(c_{ij})) - (   log(P(c_i))  +  log(P(c_j))    )          )^{-1};
\end{equation} 
\citet{lin1998measure} measure is
\begin{equation}
s_{ij}=(     \frac{2log(P(c_{ij}))}{log(P(c_i) + log(P(c_j))  } ;
\end{equation}
\citet{wu1994verbs} measure is 
\begin{equation}
s_{ij}=\frac{2  len(c_{r},c_{ij})}{len(c_i,c_{ij})+len(c_j,c_{ij})+2len(c_{r},c_{ij})}.
\end{equation}
Extended Lesk \citep{banerjee2003extended} measure is
\begin{equation}
s_{ij}=\sum_{c_i \in C_i}\sum_{c_j \in C_j} sim_g(c_i,c_j),
\end{equation}
where $sim_g$ is a gloss-based similarity measure, and set $C_i$ includes concept $c_i$ and all concepts which are directly related to it. 

Gloss Vectors measure~\citep{patwardhan2006using} is calculated as a cosine (\ref{cos}) between context vectors $\mathbf{v}_i$ and $\mathbf{v}_j$ of concepts $c_i$ and $c_j$. A context vector calculated as following: 
\begin{equation}
\mathbf{v}_i = \sum_{ \forall j : c_j \in G_i  } \mathbf{f}_j.
\end{equation}
Here $\mathbf{f}_j$ is a first-order co-occurrence vector, derived from the corpus of all glosses, and $G_i$ is concatenation of glosses of the concept $c_i$ and all concepts which are directly related to it.  

We experiment with measures relying on the \textsc{WordNet 3.0}~\citep{miller1995wordnet} as a semantic network and \textsc{SemCor} as a corpus~\citep{miller1993semantic}.

\subsection{Corpus-based measures}
\label{sect:cb-measures}
We use four measures, which  rely on the bag-of-word distributional analysis (BDA)~\citep{sahlgren2006}. They calculate similarity of concepts $c_i, c_j$ as similarity of their  feature vectors $\mathbf{f}_i, \mathbf{f}_j$ with  the following formulas~\citep[p.~699]{jurafsky2009speech}: 
cosine 
\begin{equation}
\label{cos}
s_{ij}=\frac{\mathbf{f}_i \cdot \mathbf{f}_j }{ \left\| \mathbf{f}_i \right\| \left\| \mathbf{f}_j \right\|},
\end{equation}
Jaccard
\begin{equation}
\label{jaccard}
s_{ij}=\frac{\sum_{k=1}^{N} min(f_{ik}, f_{jk})}{\sum_{k=1}^{N} max(f_{ik}, f_{jk})},
\end{equation}
Manhattan
\begin{equation}
\label{manh}
s_{ij}=  \sum_{k=1}^{N} | f_{ik} - f_{jk}|,
\end{equation}
Euclidian
\begin{equation}
\label{manh}
s_{ij}=  \sqrt{\sum_{k=1}^{N} ( f_{ik} - f_{jk})^2}.
\end{equation}
The feature vector $\mathbf{f}_i$ is a first-order co-occurrence vector. The context of a concept includes all words from a sentence where it occurred, which pass a stop-word filter (around 900 words) and a stop part-of-speech filter (nouns, adjectives, and verbs are kept). The frequencies $f_{ij}$ are normalized with Poinwise Mutual Information (PMI): $f_{ij} = log (f_{ij} / (count(c_i) count(f_j)))$. In our experiments we use two general English corpora~\citep{baroni2009wacky}: \textsc{WaCypedia} (800M tokens), and \textsc{PukWaC} (2000M tokens). These corpora are POS-tagged with the TreeTagger~\citep{treetagger}.

\subsection{Web-based measures}
\label{sect:wb-measures}
The web-based metrics use the Web text search engines in order to calculate the similarities. They rely on the number of times words co-occur in the documents indexed by an information retrieval system. Let us describe these measures in the following notation: $h_i$ is the number of documents (hits) returned by the system by the query $"c_i"$; $h_{ij}$ is the number of hits returned by the query $"c_i \text{ AND } c_j"$; and $M$ is number of documents indexed by the system. We use two web-based measures: Normalized Google Distance (NGD)~\citep{cilibrasi2007google}: 
\begin{equation}
\label{ngd}
s_{ij}=\frac{max(log(h_i,h_j))-log(h_{ij})}{log(M)-min(log(h_i),log(h_j))},
\end{equation}
and PMI-IR similarity~\citep{turney2001mining} :
\begin{equation}
\label{ngs}
s_{ij}=log\left( \frac{h_{ij} \sum_i\sum_j{h_i h_j}}{h_i h_j \sum_i{h_{ij}}} \right).
\end{equation}
We experiment with 5 NGD measures based on Yahoo, YahooBoss~\footnote{\url{http://developer.yahoo.com/search/boss/}}, Google, Google over Wikipedia, and Factiva~\footnote{\url{http://www.factiva.com/}}; and with 4 PMI-IR measures based on YahooBoss, Google, Google over Wikipedia, and Factiva. We perform search among all indexed documents or within the domain \url{wikipedia.org} (we denote the latter measures with the postfix -W). 

\subsection{Classification of the measures}
It might help to understand the results if  we mention that (1) - (6) are measures of \textit{semantic similarity}, while (7) and (8) are measures of \textit{semantic relatedness}. Semantic relatedness is a more general notion than semantic similarity~\citep{budanitsky2001semantic}. A  measure of semantic similarity uses only hierarchical and equivalence relations of the semantic network, while a measure of semantic relatedness also use relations of other types.  Furthermore, measures (1), (2), (3), are "pure" semantic similarity measures since they use only semantic network, while (3), (4), and (5) combine information from a semantic network and a corpus.  

The corpus-based and web-based measures are calculated differently, but they are both clearly \textit{distributional} in nature. In that respect, the web-based measures use the Web as a corpus. Figure~\ref{fig:classif} contains a more precise classification of the considered measures, according to their properties.  Finally, both (8) and (9)-(12), rely on the vector space model. 

\begin{figure}
	\centering
		\includegraphics[width=0.5\textwidth]{figures/classif-gray}
		\caption{Classification of the measures used in the paper.}
	\label{fig:classif}
\end{figure}


\subsection{Experimental Setup}
\label{esetup}

We experiment with the knowledge-based measures implemented in the \textsc{WordNet::Similarity} package~\citep{pedersen2004wordnet}. Our own implementation is used in the experiments with the corpus-based measures and the web-based measures relying on the \textsc{Yahoo BOSS} search engine API. We use the \textsc{Measures of Semantic Relatedness } web service~\footnote{\url{http://cwl-projects.cogsci.rpi.edu/msr/}} to assess the other web measures. 

The evaluation was done with the BLESS set of semantic relations. It relates 200 target concepts to some 8625 relatum concepts with 26554 semantic relations (14440 are correct and 12154 are random). Every relation has one of the following six types:  hyponymy, co-hyponymy,  meronymy,  attribute,  event, and random. The distribution of relations among those types is given in table 1. Each concept is a single English word.

%(100 animate and 100 inanimate nouns)
%The dataset includes 14481 relations to nouns, 7089 relations to adjectives, and 4916 relations to verbs.

\section {Results}

\subsection{Comparing General Performance of the Similarity Measures}

In our evaluation semantic relations extraction was viewed as a retrieval task. Therefore, for every metric we calculated precision, recall, and F1-measure with respect to the golden standard. Let $\hat{R}$ be set of extracted semantic relations, and $R$ be set of semantic relations in the BLESS. Then 
$$
Precision=\frac{|R \cap \hat{R}|}{|\hat{R}|}, Recall=\frac{|R \cap \hat{R}|}{|R|}.
$$
An extracted relation $\left\langle c_i,t_?,c_j \right\rangle \in \hat{R}$ matches a relation from the evaluation dataset $\left\langle c_i,t,c_j\right\rangle \in R$ if $t \neq random$. Thus, an extracted relation is correct if it has any type in BLESS, but random.

General performance of the measures is presented in table~\ref{types} (columns 2-4). The Resnik measure~(\ref{resnik}) is the best among the knowledge-based measures; the NGD~(\ref{ngd}) measure relying on the Yahoo search engine is the best results among the web-based measures. Finally, the cosine measure~(\ref{cos}) (BDA-Cos)  is the best among all the measures. The table 2 demonstrate some extracted relations discovered with the BDA-Cos measure.

\begin{figure}
	\centering
		\includegraphics[width=0.5\textwidth]{figures/pr-graph-gray}
		\caption{Precision-recall graph of the six similarity measures (kNN threshold value $k=0-52\%$).}
	\label{fig:prgraph}
\end{figure}

In table 1 we ranked the measures based on their F-measure when precision is fixed at 80\% (see figure~\ref{fig:prgraph}). We have chosen this precision level, because it is a point when automatically extracted relations start to be useful. It is clear from the precision-recall graph (figure~\ref{fig:prgraph}) that if another precision level is fixed then ranking of the metrics will change. Analysis of this and similar plots for other measures shows us that: (1) the best knowledge-based metric is Resnik; (2) the BDA-Cos is the best among the corpus-based measures, but BDA-Jaccard is very close to it;  (3) the three best web-based measures are NGD-Google (within the precision range 100-90\%), NGD-Factiva (within the precision range 90\%-87\%), and NGD-Yahoo (starting from the precision level 87\%). In these settings, choose of the most suitable metric may depend on the application. For instance, if   just a few precise relations are needed then NGD-Google is a good choice. On the other hand, if we tolerate a slightly less precision, and if we need many relations then the BDA-Cos is the best choice.


\begin{figure}
	\centering
		\includegraphics[width=0.5\textwidth]{figures/learning-curve-gray}
	\caption{Learning curves of the BDA-Cos on the WaCypedia and PukWaC corpora (0.1M--2000M tokens).}
	\label{fig:lcurve}
\end{figure}

 Figure~\ref{fig:lcurve} depicts learning curve of the BDA-Cos measure.Dependence of the F-measure at the precision level of 80\% from the corpus size is not linear. F-measure improves up to 44\% when we increase corpus size from 1M to 10M tokens; increasing corpus from 10M to 100M tokens gives the improvement of 16\%; finally, increasing corpus from 100M to 2000M tokens gives the improvement of only 3\%. 


\begin{table*}
\begin{center}
\begin{tabular}{|l||c|c|c||c|c|c|c|c|c|c|}
  \multicolumn{1}{c}{} &  \multicolumn{3}{c}{General Performance} & \multicolumn{5}{c}{Semantic Relations Distribution}  \\  \hline
\bf Measure & $k$ & \bf Recall & \bf F1  &  \bf hyper$,\%$ & \bf coord$,\%$ & \bf attri$,\%$ & \bf mero$,\%$ & \bf event$,\%$ \\ \hline \hline
\bf  Resnik & \bf 40\% & \bf 0.59 & \bf 0.68  &  \bf 9 $\mid$ 14 & \bf \gc 77 $\mid$ 40 & \bf 4 $\mid$ 8 & \bf 6 $\mid$ 22 & \bf 4 $\mid$ 15 \\
Inv.Edge-Counts  &  38\%  &  0.56 & 0.66   & \gc 22 $\mid$ 15 & \gc 61 $\mid$ 40 & 4 $\mid$ 8 & 7 $\mid$ 22 & 6 $\mid$ 15 \\
Leacock-Chodorow & 38\%  & 0.56 & 0.66  &  \gc 22 $\mid$ 15 & \gc 61 $\mid$ 40 & 4 $\mid$ 8 & 7 $\mid$ 22 & 6 $\mid$ 15 \\
Wu Palmer & 37\% & 0.54 & 0.65  &  \gc 20 $\mid$ 15 & \gc 64 $\mid$ 42 &  3 $\mid$ 8 & 7 $\mid$ 22 & 5 $\mid$ 13 \\
Lin & 36\% & 0.53 & 0.64  &  \gc 30  $\mid$ 16 & \gc 52  $\mid$ 31 & 4  $\mid$ 7 & 8   $\mid$ 29 & 5 $\mid$ 16 \\
Gloss Overlap & 36\%  & 0.53 & 0.63  &   5 $\mid$ 6 &  \gc 52 $\mid$ 34 & 7 $\mid$ 12 & 18 $\mid$ 21 & 18 $\mid$ 27   \\ 
Jiang-Conrath & 35\%  & 0.52 & 0.63 &  \gc 38 $\mid$  16 & \gc 45 $\mid$  30 & 4 $\mid$  6 & 8 $\mid$  29 & 5 $\mid$  18 \\
Extended Lesk & 30\%  & 0.45 & 0.57  &  \gc 21 $\mid$ 14 & \gc 39 $\mid$ 30 &  1 $\mid$ 9 & 29 $\mid$ 28 & 9 $\mid$ 19 \\ \hline

\bf BDA-Cos  & \bf 52\%  & \bf 0.76 &  \bf 0.78  &    \bf \gc  9 $\mid$ 7 & \bf \gc 42 $\mid$ 27 & \bf 11 $\mid$ 20 & \bf 15 $\mid$ 17 & \bf 23 $\mid$ 30 \\
 BDA-Jaccard &  51\%  & 0.75 &  0.77 & \gc 10 $\mid$ 7 & \gc 45 $\mid$ 27 & 8 $\mid$ 16 & 16 $\mid$ 20 & 20 $\mid$ 27 \\
 BDA-Manhattan  & 37\%  & 0.54 & 0.65  &  \gc 7 $\mid$ 6 &  \gc 35 $\mid$ 24 & 17 $\mid$ 22 & 10 $\mid$ 15 & 31 $\mid$ 34 \\ 
 BDA-Euclidian  & 21\%  & 0.30 & 0.44  &  \gc 7 $\mid$ 7 & \gc 31 $\mid$ 18 & 20 $\mid$ 26 & 12 $\mid$ 13 & 30 $\mid$ 37 \\ \hline

%SDA-cos  &  ?\%  & ? &  ?  & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? \\
%SDA-jaccard  &  ?\%  & ? &  ?  & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? \\
%SDA-manhattan  &  ?\%  & ? &  ?  & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? \\
%SDA-euclidian &  ?\%  & ? &  ?  & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? & ? $\mid$ ? \\ \hline

 \bf NGD-Yahoo & \bf 46\% & \bf  0.68 & \bf 0.74 &   \bf \gc 7 $\mid$ 6 & \bf \gc 51 $\mid$ 30 & \bf 9 $\mid$ 18 & \bf 17 $\mid$ 20 & \bf 15 $\mid$ 25 \\
 NGD-Factiva & 47\% &  0.66 & 0.72 &   \gc 10 $\mid$ 8 & \gc 44 $\mid$ 28  & 8 $\mid$ 19 & 23 $\mid$ 22 & 16 $\mid$ 25 \\
 NGD-YahooBOSS &  35\% &  0.51 & 0.63 &  \gc 13 $\mid$ 10 & \gc 54 $\mid$ 36 & 4 $\mid$ 10 & 14 $\mid$ 20 & 15 $\mid$ 22 \\
 NGD-Google &  33\% &  0.48 & 0.60 &  1 $\mid$ 7  & \gc 41 $\mid$ 28  & \gc 45 $\mid$ 19  & 2 $\mid$ 19 & 11 $\mid$ 28 \\
 NGD-Google-W & 29\% &  0.43 & 0.56 &   8 $\mid$ 9 & \gc 45 $\mid$ 31 & 8 $\mid$ 14 & 20 $\mid$ 21 & 19 $\mid$ 25 \\
 PMI-YahooBOSS &   29\% &  0.43 & 0.56 & \gc 15 $\mid$ 12 & \gc 53 $\mid$ 38 & 3 $\mid$ 9 & 15 $\mid$ 20	 & 13 $\mid$ 20 \\
 PMI-Factiva & 25\% &  0.28 & 0.44 &   \gc 8 $\mid$ 8 & \gc 42 $\mid$ 30 & 10 $\mid$ 17 & 21 $\mid$ 20 & 18 $\mid$ 24 \\ 
 PMI-Google &  12\% &  0.18 & 0.29 &  \gc 8 $\mid$ 8 & \gc 55 $\mid$ 35 & 7 $\mid$ 15 & 17 $\mid$ 21 & 12 $\mid$ 22 \\
 PMI-Google-W & 9\% &  0.13 & 0.23 &  \gc 12 $\mid$ 11 & \gc 47 $\mid$ 38 & 7 $\mid$ 11 & 20 $\mid$ 20 & 13 $\mid$ 19 \\ \hline
%NGD-tasa & 55\% &  0.56 & 0.66 & \gc 9 $\mid$ 7 & \gc 41 $\mid$ 24 &  8 $\mid$ 20 & 24 $\mid$ 22 & 18 $\mid$ 27 \\
%NGD-G-google.com & 24\% &  0.35 & 0.49 & 6 $\mid$ 7 & \gc 38 $\mid$ 24 & \gc 27 $\mid$ 23  & 12 $\mid$ 18 & 17 $\mid$ 29 \\ \hline

 Random measure & &  &  &  8 $\mid$ 9  & 24 $\mid$ 25  & 20 $\mid$ 19  & 22 $\mid$  20 & 26 $\mid$ 27  \\
 BLESS dataset & &  &  &  9 & 25 & 20 & 19 & 27 \\

\hline
\end{tabular}
\end{center}
\caption{\label{types} Columns 2-4: Recall and F-measure when Precision$=0.8$ (correct relations of all types vs random relations). Columns 5-9: percent of extracted relations of a certain type with respect to all correctly extracted relations, when threshold $k$ equal $10\%$ or $40\%$. The best measure are sorted by F-measure; the best measures  are in bold. }
\end{table*}

\begin{table*}
\begin{center}
\begin{tabular}{|l|l|l|l|l|}
\hline \bf ant & \bf banana & \bf fork & \bf missile & \bf salmon \\ \hline
cockroach (coord) & mango (coord) &  prong (mero) & warhead (mero) & trout (coord) \\
grasshopper (coord) & pineapple (coord) & spoon (coord) & weapon (hyper) & mackerel (coord) \\
silverfish (coord) & papaya (coord) & knife (coord) & deploy (event) & herring (coord) \\
wasp (coord) & pear (coord) & lift (event) & nuclear (attri) & fish (event) \\
insect (hyper) & ripe (attri) & \bf fender (random) & bomb (coord) & tuna (coord) \\
arthropod (hyper) & peach (coord) & plate (coord) & destroy (event) &  oily (attri)\\
industrious (attri) & coconut (coord) &  rake (coord) & rocket (coord) & poach (event)\\
ladybug (coord) & fruit (hyper) & shovel (coord) & arm (hyper)  & catfish (coord)\\
bee (coord) & apple (coord) & handle (mero) & propellant (mero) & catch (event)\\
beetle (coord) & apricot (coord) & sharp (attri) & \bf bolster (random) & fresh (attri)\\
locust (coord) & strawberry (coord) & spade (coord)  & launch (event)  & cook (event)\\ 
dragonfly (coord) & ripen (event) & napkin (coord) & deadly (attri) & cod (coord)\\ 
hornet (coord) & plum (coord) & cutlery (hyper) & \bf country (random) & smoke (event)\\ 
creature (hyper) & grapefruit (coord) & head (mero) & strike (event) & seafood (hyper)\\ 
crawl (event) & cherry (coord) & scissors (coord) & defuse (event) & eat (event)\\ 
%tiny (attri) & sweet (attri) & small (attri) & gun (coord) & farm (event)\\ 

\hline
\end{tabular}
\end{center}
\caption{\label{examples} Examples of the discovered semantic relations with the bag-of-words distributional analysis (BDA-Cos).    }
\end{table*}


\subsection{Comparing Relation Distributions of the Similarity Measures }
In this section, we are trying to figure out what types of semantic relations the measures find. We compare distributions of semantic relations against the BLESS dataset. Generally, if two measures have equal general performances, one may want to choose a metric which provides more relations of a certain type, depending on the application. This information may be also valuable in order to decide which metrics to combine in a meta-metric. 

\begin{figure}
	\centering
		\includegraphics[width=0.5\textwidth]{figures/coord}
	\caption{Percent of co-hyponyms among all correctly extracted relations for the six best measures.}
	\label{fig:coord}
\end{figure}

\textbf{Distribution of Relation Types.}  In this section, we estimate empirical relation distribution of the metrics over five relation types: hyponymy, co-hyponymy, meronymy, attribute, and event. To do so we calculate percents of correctly extacted relations of type $t$ for a each measure:
$$
Percent=\frac{\hat{R}_t}{|R \cap \hat{R}|}, \text{ where } \bigcup_{t \in T} \hat{R}_t = |R \cap \hat{R}|.
$$
Here $|R \cap \hat{R}|$ is a set of all correctly extracted relations, and $\hat{R}_t$ is a set of extracted relations of  type $t$. Figure~\ref{fig:coord} demonstrates that percent of extracted relations of certain type depends on the value of $k$ (c.f. section~\ref{methods}). For instance, if $k=10\%$  then 77\% of extracted relations by Resnik are co-hyponyms, but if $k=40\%$ then the same measure outputs 40\% of co-hyponyms. We report relations distribution at two levels of the threshold $k$ -- 10\% and 40\%.  

The empirical distributions are reported in columns 5-9 of the table~\ref{types}. Each of those columns correspond to one semantic relation type $t$, and contains two numbers: $p_{10}$ -- percent of relations of type $t$ when $k=10\%$, and $p_{40}$ -- percent of  relations of type $t$ when $k=40\%$. We represent those two values in the following format: $p_{10} | p_{40}$. For instance, $77 | 40$ behind the Resnik measure means that when $k=10\%$ it extracts $77\%$ of co-hypernyms, and when $k=40\%$ it extracts $40\%$ of co-hypernyms. 

If the threshold $k$ is $10\%$ then the biggest fraction of extracted relations are co-hyponyms -- from 35\% for BDA-Manhattan to 77\% for Resnik measure. At this threshold level, the knowledge-based measures mostly return co-hyponyms (60\% in average) and hyperonyms (23\% in average). The corpus-based metrics mostly return co-hyponyms (38\% in average) and  event relations (26\% in average). The web-based measures return many (48\% in average) co-hyponymy relations. 

If the threshold $k$ is $40\%$ then relation distribution of  all the measures significantly changes. Most of the relations returned by the knowledge-based measures are co-hyponyms (36\%) and meronyms (24\%). The majority of relations discovered by the corpus-based metrics are co-hyponyms (33\% ), event relations (26\%), and meronyms (20.33\%). The web-based measures at this threshold value return many event relations (32\%).

Interestingly, for the most of the measures, percent of extracted hyponyms and co-hyponyms decreases as the value of $k$ increase, while the percent of other relations increases. In order to make it clear, we grayed cells of the table 1 when $p_{10} \geq p_{40}$.

\textbf{Similarity to the BLESS Distribution.} In this section, we check if relation distributions (see table 1) are completely biased by the distribution in the evaluation dataset. We compare relation distributions of the metrics with the distribution in the BLESS on the basis of the $\chi^2$ goodness of fit test~\footnote{Here and below, we calculate the $\chi^2$ statistic from the table 1 (columns 5-9), where percents are replaced with frequencies.}~\citep{agresti2000categorical} with $df=4$. A random similarity measure is completely biased by the distribution in the evaluation dataset: $\chi^2=5.36$, $p=0.252$ for $k=10\%$ and $\chi^2=3.17$, $p=0.53$ for $k=40\%$. On the other hand, distributions of all the 21 measures are significantly different from the distribution in the BLESS ($p < 0.001$). The value of chi-square statistic varies from $\chi^2=89.94$ (NGD-Factiva, $k=10\%$) to $\chi^2=4000$ (Resnik, $k=10\%$). 

%The distributions are more similar to the BLESS at the threshold level $k=40\%$ ($\chi^2=868$ in average for all measures) with respect to the threshold value $k=10\%$ ($\chi^2=1467$ in average). 

\textbf{Independence of Relation Distributions.} In this section, we check whether relation distributions of the various measures are significantly different. In order to do so, we perform the chi-square independence test on the table 1. Our experiments shown that there is a significant interaction between the type of the metric and the relations distribution: $\chi^2=10487, p<0.001, df=80$ for all the metrics; $\chi^2=2529, df=28, p<0.001$ for the knowledge-based metrics; $\chi^2=245, df=12, p<0.001$ for the corpus-based metrics; and $\chi^2=3158, df=32, p < 0.001$ for the web-based metrics. Thus, there is a clear dependence between the type of measure and the type of relation it extracts. 

%Finally, we found that relation distributions in the four  groups listed above are more similar (values of the chi-square statistic are lower) for the threshold value $k=40\%$ with respect to the value $k=10\%$.

\textbf{Most Similar and Dissimilar Measures.} In this section, we would like to find the most similar and disimilar measures. This information is particularly useful for the combination of the metrics. In order to find redundant measures, we calculate distance $x_{ij}$ beween  measures $sim_i$ and $sim_j$, based on the $\chi^2$-statistic:
\begin{equation}
x_{ij}=x_{ji}=\sum_{t\in\ T}\frac{(|\hat{R^i_t}| - |\hat{R^j_t}|)^2}{|\hat{R^j_t}|},
\end{equation}
where $\hat{R^i_t}$ is ensemble of correctly extracted relations of type $t$ with measure $sim_i$. We calculate these distances for all pairs of measures and then rank the pairs according to the value of $x_{ij}$. Table~\ref{pairs} present list of the most similar and dissimilar metrics obtained this way. Figure~\ref{fig:clusters} reports in a compact way all the pairwise similarities $(x_{ij})_{21 \times 21}$ between the 21 metrics. In this graph, an edge links two measures, which have the distance value $x_{ij} < 220$.  The graph was drawn with the \citet{fruchterman1991} force-directed layout algorithm. One can see that relation distributions of the web- and corpus-based measures are quite similar. The knowledge-based measures are much different from them, but similar among themselves.

%Finally, the figure~\ref{fig:clusters} once again suggest that relation distributions are more similar at the threshold level $k=40\%$ with respect to the threshold level $k=10\%$.

\textbf{Distribution of Similarity Scores.} In this section, we compare distributions of similarity scores across relation types with the following procedure:  (1) Pick a closest relatum concept $c_j$ per relation type $t$ for each target concept $c_i$. (2) Convert similarity scores associated to each target concept to z-scores.  (3) Summarize the distribution of similarities across relations by plotting the z-scores grouped by relations in a box plot. (4) Verify the statistical significance of the differences in similarity scores across relations by performing the Tukey's HSD test.

%\begin{enumerate}
%	\item Pick a closest relatum concept $c_j$ per relation type $t$ for each target concept $c_i$. 	
%	\item Convert similarity scores associated to each target concept to z-scores.  
%	\item Summarize the distribution of similarities across relations by plotting the z-scores grouped by relations in a box plot. 
%	\item Verify the statistical significance of the differences in similarity scores across relations by performing the Tukey's HSD test.
%\end{enumerate}

Figure~\ref{fig:boxplots} presents the distributions of similarities across various relation types for Resnik, BDA-Cos, and NGD-Yahoo. First, meaningful relation types for these three measures are significantly different ($p < 0.001$) from random relations. The only exception is the Resnnik measure -- its similarity scores for the attribute relations are not significantly different ($p=0.178$) from random relations. Thus, the best three measures provide scores which let us separate incorrect relations from the correct ones if an appropriate threshold $k$ is set. Second, the similarity scores have highest values for the co-hyponymy relations. Third, BDA-Cos, BDA-Jaccard, NGD-Yahoo, NGD-Factiva, and PMI-YahooBoss provide the best scores. They let us clearly ($p<0.001$) separate meaningful relations from the random ones. From the other hand, the poorest scores were provided by BDA-Manhattan, BDA-Euclidian, NGD-YahooBoss, and NGD-Google, because their scores let us clearly separate only co-hyponyms from the random relations. 

\textbf{Corpus Size.} Table 1 presented relation distribution of the BDA-Cos trained on the 2000M token corpus \textsc{ukWaC}. Figure~\ref{fig:bdadist} shows the relation distribution function of the corpus size. First, if corpus size increases then percent of attribute relations decreases, while percent of co-hyponyms increases. Second, corpus size does not drastically influence the distribution for big corpora. For instance, if we increase corpus size from 100M to 2000M tokens then the percent of relations change on 3\% for attributes,  on 3\% co-hyponyms, on 1\% events, on 0.7\% hyperonyms, and on 0.4\% meronyms.  

% The proportion of the co-hyponymy relations grows on 10\% when corpus size changes from 10M to 2000M tokens. Distribution of other relation types varies less (5\% for attribute, 4\% for event, 1\% for hyperonymy, and 0.7\% for meronymy relations).

\begin{figure}
	\centering
		\includegraphics[width=0.5\textwidth]{figures/plot-k10_russir}
	\caption{Semantic relations distribution function of corpus size (BDA-Cos measure, PukWaC corpus).}
	\label{fig:bdadist}
\end{figure}

\begin{figure*}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/boxplots}	
	\caption{Distribution of similarities accross relation types for Resnik, BDA-Cos, and NGD-Yahoo measures.}
	\label{fig:boxplots}
\end{figure*}


\begin{table*}

\begin{center}
\begin{tabular}{|l|l|l||l|l|l|}
\hline
\multicolumn{3}{|c||}{ \bf Most Similar Measures} & \multicolumn{3}{c|}{\bf Most Disimilar Measures}  \\
\hline
 $sim_i$	& $sim_j$	 & $x_{ij}$ &	$sim_i$	& $sim_j$	 & $x_{ij}$ \\ \hline
Leacock-Chodorow	& Inv.Edge-Counts	 & 0 & NGD-Google	& Extended Lesk	& 39935.16\\ 
BDA-Jaccard	& BDA-Cos & 7.17 & Jiang-Conrath & NGD-Google & 27478.90\\ 
NGD-YahooBOSS &	PMI-YahooBOSS & 19.58 & Lin	& NGD-Google	& 17527.22\\ 
Wu-Palmer	& Inv.Edge-Counts & 24.00 & NGD-Google	& Wu-Palmer	& 17416.95\\  
Wu-Palmer	& Leacock-Chodorow & 24.00 & NGD-Google	& PMI-YahooBOSS	& 13390.66 \\ 
BDA-Manhattan &	BDA-Euclidian & 25.37 & Inv.Edge-Counts &	NGD-Google &	12012.79 \\ 
PMI-Google-W	& NGD-Factiva & 27.65 & Leacock-Chodorow	& NGD-Google &	12012.79 \\ 
PMI-Google & NGD-Yahoo & 33.42 & NGD-Google &	Resnik &	11750.41\\ 
NGD-Google-W &	NGD-Factiva & 40.03 & NGD-Google & NGD-YahooBOSS	& 11556.69\\  
NGD-W &	PMI-Factiva & 42.17 & BDA-Euclidian	& Extended Lesk	& 8411.66\\ 
Gloss Overlap &	NGD-Yahoo & 53.64 & NGD-Factiva &	NGD-Google &	8066.75 \\
NGD-Factiva	& PMI-Factiva	& 58.13 & BDA-Euclidian &	Resnik &	6829.71 \\
Lin	& Jiang-Conrath	& 58.42 & PMI-Google-W	& NGD-Google &	6574.62 \\
Gloss Overlap	& NGD-Google-W &	62.46 & BDA-Manhattan	& Extended Lesk	& 6428.47 \\
				
\hline
\end{tabular}
\end{center}
\caption{\label{pairs} List of the most and least similar measures ($k=10\%$).    }
\end{table*}

\begin{figure*}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/clusters-gray}	
	\caption{Measures grouped according to similarity of their relation distributions with (15). An edge links measures $sim_i$ and $sim_j$ if $x_{ij} < 220$. The knowledge-, corpus-, and web-based measures are marked in red, blue, and green correspondingly and with the prefixes 'K','C',and 'W'. The best measures are marked with a big circle. }
	\label{fig:clusters}
\end{figure*}

\section{Related Work}
\label{sect:previous}
Prior research provide us information about general performances of the measures considered in this paper, but not necessarily on the task of semantic relations extraction. For instance, \citet{mihalcea2006corpus} compare two corpus-based (PMI-IR and LSA) and six knowledge-based measures on the task of text similarity computation. The authors report that PMI-IR is the best measure; that, similarly to our results, Resnik is the best knowledge-based measure; and that simple average over all 8 measures is even better than PMI-IR.  \citet{budanitsky2001semantic} report that  Jiang-Conrath is the best knowledge-based measure for the task of spelling correction. \citet{patwardhan2006using} evaluate six knowledge-based measures on the task of word sense disambiguation and report the same result. This contradicts our results, since we found Resnik to be the best knowledge-based measure. 
 
\citet{peirsman2008putting} compared general performances and relation distributions of distributional methods using a lexical database. \citet{sahlgren2006} evaluated syntagmatic and paradigmatic bag-of-word models. Our findings mostly fits well these and other (e.g.~\citet{curran2003distributional} or~\citet{bullinaria2007extracting}) results on the distributional analysis. \citet{lindsey2007wary} compared  web-based measures. Authors suggest that a small search domain is better than the whole Internet. Our results partially confirm this observation (NGD-Factiva outperforms NGD-Google), and partially contradicts it (NGD-Yahoo outperforms NGD-Factiva). 

~\citet{cruys2010mining} evaluates syntactic, and bag-of-words distributional methods and suggests that the syntactic models are the best for the extraction of tight synonym-like similarity.  ~\citet{wandmacher2005semantic} reports that LSA  produces 46.4\% of associative relations, 15.2\% of  synonyms, antonyms, hyperonyms, co-hyponyms, and meronyms, 5.6\% of syntactic relations, and 32.8\% of erroneous relations. We cannot compare these results to ours, since we did not evaluate neither LSA nor syntactic models. 

A common alternative to our evaluation methodology is to use the Spearman's rank correlation coefficient~\citep{agresti2000categorical} to compare the results with the human judgments, such as those obtained by~\citet{rubenstein1965} or~\citet{miller91}. 

\section{Conclusion and Future Work}

This paper has compared 21 similarity measures between lexical units on the task of semantic relation extraction. We compared their general performances and figured out that Resnik, BDA-Cos, and NGD-Yahoo provide the best results among knowledge-, corpus-, and web-based measures, correspondingly. We also found that (1) semantic relation distributions of the considered measures are significantly different; (2) all measures extract many co-hyponyms; (3) the best measures provide the scores which let us clearly separate correct relations from the random ones.  

The analyzed measures provide complimentary types of semantic information. This suggests developing a combined measure of semantic similarity.  A combined measure is not presented here since designing an integration technique is a complex research goal on its own right. We will address this problem in our future research. 

 %We expect that such a combined measure will generally perform better than the single metrics.

\section{Acknowledgments}
I would like to thank Thomas François who kindly helped with the evaluation methodology, and my supervisor Dr. Cédrick Fairon. The two anonymous reviewers, Cédrick Fairon, Thomas  François, Jean-Leon Bouraoui, and Andew Phillipovich provided comments and remarks, which considerably improved quality of the paper. This research is supported by Wallonie-Bruxelles International. 

\bibliographystyle{plainnat}
\bibliography{papers_u, theses, biblio, books, industry} 
 
\end{document}
