\documentclass[11pt]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIALIZTION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mode<presentation>
{
\usetheme{Warsaw}
\usecolortheme{whale}
\useoutertheme{smoothbars}
\usefonttheme{serif}
}

\setbeamertemplate{navigation symbols}{%
%% \insertslidenavigationsymbol
%% \insertframenavigationsymbol
%% \insertsubsectionnavigationsymbol
%% \insertsectionnavigationsymbol
%% \insertdocnavigationsymbol
\insertbackfindforwardnavigationsymbol
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{array}
\usepackage{multirow}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{xcolor}
\usepackage{euler}
\usepackage{stmaryrd}
\usepackage[vlined,linesnumbered]{algorithm2e}
\usepackage{cental}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TITLE PAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[\insertframenumber/\inserttotalframenumber]
{Comparison of the Baseline  Knowledge-, Corpus-, and Web-based Similarity
Measures \\for Semantic Relations Extraction}



\author[Alexander Panchenko]
{Alexander Panchenko \\ {\scriptsize \url{alexander.panchenko@student.uclouvain.be} }}

\institute[UCL]
{
  Center for Natural Language Processing (CENTAL)\\
  Université catholique de Louvain, Belgium
}

\date{31 July 2011 / GEMS 2011}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SLIDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \setcounter{tocdepth}{1}
  \frametitle{Plan}
  \tableofcontents
  \setcounter{tocdepth}{2}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Computing Semantic Relations from Heterogeneous Information Sources

\section{Introduction}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relations }

\begin{itemize}
	
		\item $r=\left\langle   c_i,t,c_j \right\rangle$ -- \textbf{semantic relation}, where $c_i,c_j \in C$,  $t \in T$
	\item $C$ -- \textbf{concepts} e.g. \textit{radio} or \textit{receiver operating characteristic}
	\item $T$ --	 \textbf{semantic relation types}, e.g. \textit{hyponymy} or \textit{synonymy}
	\item  $R \subseteq C \times T \times C$ --  set of \textbf{semantic relations}
	
	
\end{itemize}



\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relations Example: BLESS}
\textbf{Parameters:}
\begin{itemize}
	\item 200 source concepts $C_s$
	\item 8625 destination concepts $C_d$
	\item each concept $c \in \{C_r \cup C_d \}$ is a single English word
	\item $T= \{ \textssf{  hyper, coord, mero, event, attri, random } \}$
	\item 26554 semantic relations $R \subseteq C_s \times T \times C_d$
	%\item $\frac{26554}{200*8625} \approx 1.53 \% $
\end{itemize}

\pause

\textbf{Examples}, $R$:
\begin{itemize}
	\item $\left\langle \textssf{alligator,	coord,	snake} \right\rangle$ 
	\item $\left\langle \textssf{freezer,	attri,	empty} \right\rangle$ 
	\item $\left\langle \textssf{phone,	hyper,	device} \right\rangle$ 
	\item $\left\langle \textssf{radio, mero,	headphone} \right\rangle$ 
	\item $\left\langle \textssf{eagle, random, award} \right\rangle$ 
	\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Another  Example: Information Retrieval Thesaurus}

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/thesaurus2}
		\caption{A part of a the information retrieval thesaurus EuroVoc.}
\end{figure}

\pause

\textbf{$R=$}

\begin{itemize}
	\item $\left\langle   \textssf{energy-generating product, NT, energy industry} \right\rangle$
	\item $\left\langle   \textssf{energy technology, NT, energy industry} \right\rangle$
	\item $\left\langle   \textssf{petrolium, RT, fossil fuel} \right\rangle$
	\item $\left\langle   \textssf{energy technology, RT, oil technology} \right\rangle$
	\item ...
	
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Problem}

\begin{block}{Semantic Relations Extraction Method}
\textbf{Input:} lexically expressed concepts $C$, semantic relation types $T$
\textbf{Ouput:} lexico-semantic relations $\hat{R} \sim R$ 
\end{block}

\pause
\textbf{Solutions:}
\begin{itemize}
\item Pattern-based methods
\pause
\begin{itemize}
					\item Manually constructed  patterns (Hearst, 1992)
					\item Semi-automatically constructed patterns (Snow et al., 2004)
					\item Unsupervised patterns learning (Etzioni et al., 2005)
\end{itemize}
\pause
\item \alert{Unsupervised similarity-based methods} {\small (Lin, 1998; Sahlgren, 2006)}
\end{itemize}

\pause

\textbf{Research Questions} w.r.t. similarity-based methods:
\begin{itemize}
\item  Which similarity measure is the best for relations extraction? 
\pause
\item Do various measures capture relations of the same type?
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Motivation: Automatic Thesaurus Construction }

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/autheco2}
		\caption{A technology for automatic thesaurus construction.}
\end{figure}

\pause 

\textbf{Applications:}

\begin{itemize}
	\item Query expansion and query suggestion
	\pause
	
	\item Navigation and browsing on the corpus
	\pause
	
	\item Visualization of the corpus
	\pause
	
	\item ...
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{The Contributions}

\begin{itemize}

\item Studying \textbf{21 corpus-, knowledge-, and web-based} measures 
\pause

\item Using  the  \textbf{BLESS} dataset 
\pause

\item Analysis of the \textbf{semantic relation types} 
\pause

		\begin{itemize}
		\item Reporting empirical relation distributions
		\pause
		
		\item Finding most and least similar measures

		\end{itemize}
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methodology}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Similarity-based Semantic Relations Extraction}

\begin{block}{Semantic Relations Extraction Algorithm}

\begin{algorithm}[H]
\SetLine
\KwIn{ Concepts $C$, Parameters of similarity measure $P$, Threshold $k$, Min.similarity value $\gamma$ }
\KwOut{ Unlabeled semantic relations $\hat{R}$}

\alert{$\mathbf{S} \leftarrow sim(C,P)$ \;}
$\mathbf{S} \leftarrow normalize(\mathbf{S})$ \;
$\hat{R} \leftarrow threshold(\mathbf{S},k, \gamma)$ \;
\Return{ $\hat{R}$ } \;
\end{algorithm}

\end{block}

\pause
\begin{itemize}
	\item $sim$ -- one of 21 tested similarity measures
	\pause
	\item $normalize$ -- similarity score normalization	
	\pause
	\item $threshold$ -- kNN thresholding function 
	$R=\bigcup_{i=1}^{|C|}\left\{\left\langle c_i, t, c_j \right\rangle :  c_j \in \text{ top }k\% \text{ concepts } \wedge s_{ij} \geq \gamma \right\}.
	$
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Knowledge-based Measures}
\begin{block}{Description}
\begin{itemize}
	\item \textbf{Data:} semantic network (\textsc{WordNet 3.0}), corpus (\textsc{SemCor}).
	
	\pause
	
	\item \textbf{Variables:}
	\begin{itemize}
			\item  $h$ -- the height of  the network
			\pause
			
			\item $len(c_i,c_j)$ -- length of the shortest path between concepts
			\pause
			
			\item $P(c)$ -- probability of the concept, estimated from a corpus
			\pause
	\end{itemize}
\end{itemize}

\end{block}


\textbf{Inverted Edge Count}:
$$s_{ij}=len(c_i,c_j)^{-1}$$

\pause

\textbf{Leacock-Chodorow}:
$$s_{ij}=-log\frac{len(c_i,c_j)}{2h}$$


\end{frame}

\begin{frame}
\frametitle{Knowledge-based Measures (8)}

\textbf{Wu-Palmer}
$$
s_{ij}=\frac{2  \cdot len(c_{r},lcs(c_i,c_j))}{len(c_i,lcs(c_i,c_j))+len(c_j,lcs(c_i,c_j))+2 \cdot len(c_{root},lcs(c_i,c_j))}
$$

\pause

\textbf{Resnik}:
$$s_{ij}=-log(P(lcs(c_i,c_j)))$$

\pause

\textbf{Jiang-Conrath}: 
$$
s_{ij}= [  2 \cdot log(P(lcs(c_i,c_j))) -  log(P(c_i) - log(P(c_j) )          ]^{-1}
$$

\pause

\textbf{Lin}:
$$
s_{ij}=     \frac{2 \cdot log (P(lcs(c_i,c_j)))}{log(P(c_i) + log(P(c_j))  } 
$$


\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Knowledge-based Measures }

\begin{block}{Description}
\begin{itemize}
	\item \textbf{Data:} semantic network (\textsc{WordNet 3.0}).
	
	\pause
	
	\item \textbf{Variables:}
	\begin{itemize}
			\item $gloss(c)$ -- definition of the concept
			\pause
			
			\item $sim(gloss(c_i),gloss(c_j))$ -- similarity of concepts' glosses
			\pause
			
			\item $\mathbf{f}_i$ -- context vector of $c_i$, calculated on the corpus of all glosses
			\pause
	\end{itemize}
\end{itemize}

\end{block}


\textbf{Extended Lesk} (Banerjee and Pedersen, 2003):
$$
s_{ij}=\sum_{c_i \in C_i}\sum_{c_j \in C_j}sim(gloss(c_i),gloss(c_j)), \text{ where } C_i = \{ c: \exists \left\langle c, t, c_i \right\rangle \}.
$$

\pause

\textbf{Gloss Vectors} (Patwardhan and Pedersen, 2006):

$$
s_{ij} = \frac{\mathbf{v}_i \cdot \mathbf{v}_j}{\left\| \mathbf{v}_i \right\| \left\| \mathbf{v}_j \right\|}, \text{ where } \mathbf{v}_i = \sum_{ \forall j : c_j \in G_i  } \mathbf{f}_j , \text{ where } G_i = \bigcup_{c \in C_i} gloss(c)
$$
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Corpus-based Measures (4)}

\begin{block}{Description}
\begin{itemize}
	\item \textbf{Data:} corpus (\textsc{WaCypedia} (800M), \textsc{ukWaC} (2000M)).
	\pause
	
	\item \textbf{Variables:} $\mathbf{f}_i$ -- context vector for $c_i$ 
\end{itemize}
\end{block}

\pause
	
	Cosine:
	$$
	s_{ij}=\frac{\mathbf{f}_i \cdot \mathbf{f}_j }{ \left\| \mathbf{f}_i \right\| \left\| \mathbf{f}_j \right\|};
	$$
	\pause
	
	Jaccard:
	$$
	s_{ij}=\frac{\left\|  min(\mathbf{f}_i, \mathbf{f}_j) \right\|_1 }{\left\| max(\mathbf{f}_i, \mathbf{f}_j) \right\|_1};
	$$
	\pause
	
	Euclidian:
	$$
	s_{ij}=  \left\|  \mathbf{f}_i - \mathbf{f}_j \right\| ;
	$$
	
	
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Corpus-based Measures (4)}


Manhattan:
	$$
	s_{ij}= \left\|  \mathbf{f}_i - \mathbf{f}_j \right\|_1.
	$$
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Web-based Measures (9)}

\begin{block}{Description}
\begin{itemize}
	\item \textbf{Data:} number of the hits returned by an IR system (\textsc{Google, Yahoo, Yahoo BOSS, Factiva}).
	\pause
	
	\item \textbf{Variables:} 
		\begin{itemize}
		\item $h_i$ -- number of hits returned by query $"c_i"$ 
		\pause
		
		\item $h_{ij}$ -- number of hits returned by the query $"c_i \text{ AND } c_j"$
		\pause
		
		%\item $M$ is number of documents indexed by the system
		\pause
	\end{itemize}
\end{itemize}	
\end{block}

\textbf{Normalized Google Distance} (Cilibrasi and Vitanyi, 2007):
$$s_{ij}=\frac{max(log(h_i),log(h_j))-log(h_{ij})}{log(M)-min(log(h_i),log(h_j))}$$
\pause

\textbf{PMI-IR} (Turney, 2001): 
$$
s_{ij}= -log\frac{ P(c_i, c_j)}{P(c_i)P(c_j)} = -log \frac{h_{ij} \sum_i\sum_j{h_i h_j}}{h_i h_j \sum_i{h_{ij}}} .
$$
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{"Theoretical" Classification of the Similarity Measures}

\begin{figure}
	\centering
		\includegraphics[width=1.07\textwidth]{figures/classif}
\end{figure}
	
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{General Performance}
	\begin{block}{Evaluation Protocol}
	\begin{itemize}
		\item $Precision=\frac{|R \cap \hat{R}|}{|\hat{R}|}, Recall=\frac{|R \cap \hat{R}|}{|R|}, F1= 2 \cdot \frac{Precision \cdot Recall}{Precision + Recall}$
		\item $R$ -- all relations from BLESS, but random
		\item $\hat{R}$ -- extracted relations
	\end{itemize}	
	\end{block}
	
	\pause 
	
	\begin{figure}
	\centering
		\includegraphics[width=0.7\textwidth]{figures/pr-graph_russir}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{General Performance: Scores}

@ Precision = 0.80
	\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/general-results}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{General Performance: Learning curve of the BDA-Cos}

\begin{figure}
	\centering
		\includegraphics[width=0.8\textwidth]{figures/learning-curve-gray}
\end{figure}

\pause

\begin{itemize}
	\item $\Delta F1_{1M-10M} \cong 0.44$
	\pause
	
	\item $\Delta F1_{10M-100M}\cong 0.16$
	\pause
	
	\item $\Delta F1_{100M-1000M}\cong 0.03$
\end{itemize}

	
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Example of the Extracted Relations (BDA-Cos)}

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/example}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Comparing Relation Distributions}

\begin{block}{Evaluation Protocol}
\begin{itemize}
		\item $Percent=\frac{\hat{R}_t}{|R \cap \hat{R}|} \cdot 100$
		\item $\hat{R}_t$ is a set of extracted relations of  type $t, \bigcup_{t \in T} \hat{R}_t = |R \cap \hat{R}|$ 
\end{itemize}
\end{block}

\pause

\begin{itemize}
\textbf{Issue:} high sensitivity of the $Percent$ to $k$:
\end{itemize}

\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth]{figures/coord}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Comparing Relation Distributions: Scores @ $k=10\%$}

\begin{figure}
	\centering
		\includegraphics[width=0.7\textwidth]{figures/ten}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Comparing Relation Distributions: Scores @ $k=40\%$}

\begin{figure}
	\centering
		\includegraphics[width=0.7\textwidth]{figures/four}
\end{figure}
	
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}
%\frametitle{Comparing Relation Distributions: Scores}

%\begin{figure}
%	\centering
%		\includegraphics[width=0.7\textwidth]{figures/dist-results}
%\end{figure}
%\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Comparing Relation Distributions: Scores}

\begin{itemize}
	\item \textbf{Similarity to the BLESS:}
	\begin{itemize}
			\item Random measure: $\chi^2=5.36, p=0.252$			
			\item 21 measures: $\chi^2=89.94-4000, p < 0.001$
	\end{itemize}
	
	\pause
	
	\item \textbf{Independence of  the Relation Distributions:}
	\begin{itemize}
			\item 21 measures: $\chi^2=10487, p<0.001, df=80$ 
			\item knowledge-based measures: $\chi^2=2529, df=28, p<0.001$ 
			\item corpus-based measures: $\chi^2=245, df=12, p<0.001$ 
			\item web-based measures: $\chi^2=3158, df=32, p < 0.001$ 
	\end{itemize}
	
\end{itemize}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Relation Distributions: Distribution of the Scores}

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/boxplots}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Relation Distributions: Most Similar Measures}

\begin{block}{ Measures Dissimilarity}
\begin{itemize}
\item Calculate distance $x_{ij}$ between measures $sim_i$ and $sim_j$:
$$x_{ij}=x_{ji}=\sum_{t\in\ T}\frac{(|\hat{R^i_t}| - |\hat{R^j_t}|)^2}{|\hat{R^j_t}|}$$
\item $\hat{R^i_t}$ -- correctly extracted relations of type $t$ with measure $sim_i$
\end{itemize}
\end{block}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Relation Distributions: Most Similar Measures}

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/sim-ranks}
\end{figure}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Relation Distributions: Most Similar Measures}

\begin{itemize}
	\item Threshold the $21 \times 21$ matrix $\mathbf{X}$: if $x_{ij} < 220$ then $x_{ij}=0$
	\item Visualize the distances with the Fruchterman-Reingold (1991) graph layout
\end{itemize}

\begin{figure}
	\centering
		\includegraphics[width=1.05\textwidth]{figures/clusters-gray}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Conclusion:}
	\begin{block}{General Performance}
	\begin{itemize}
			\item Best \textbf{knowledge-based} measure -- Resnik (\textsc{WordNet})
			\item Best \textbf{corpus-based} and \textbf{the best} measure -- BDA-Cos (\textsc{ukWaC})
			\item Best \textbf{web-based} measure -- NGD-\textsc{Yahoo}
			\item Best measures clearly separate correct and random relations 
	\end{itemize}
	\end{block}
	
	\begin{block}{Relations Distributions}
	\begin{itemize}
			\item All measures extract many \textbf{co-hyponyms}
			\item The measures were grouped according to similarity of their relation distributions
			\item The measures provide \textbf{complimentary results} 
	\end{itemize}
	\end{block}
	
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Further Research:}
	\begin{block}{Methods}
	%\item \textbf{Method:}
		\begin{itemize}
			\item Develop a \textbf{combined similarity measure} -- linear combination, logistic regression, committees...
			\item \textbf{More measures} -- LSA, SDA, LDA, surface-based similarity, kernels, definition-based measures,...
			\item Working with \textbf{MWE}
			\item \textbf{Classify extracted relations}: hyponymy, synonymy, etc.
		\end{itemize}
\end{block}

\pause

\begin{block}{Evaluation}	
\begin{itemize}
		  \item Using a golden standard with \textbf{synonyms}
			\item Using a golden standard with \textbf{MWE} -- thesauri
			\item Use Speirman's correlation $r_s$ to compare the results
			\item An application-based evaluation -- \textbf{query expansion}
\end{itemize}
\end{block}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Questions}
	\Huge Thank you! Questions?

\end{frame}

\end{document}
