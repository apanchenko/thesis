\documentclass[11pt]{beamer}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INITIALIZTION 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\mode<presentation>
{
\usetheme{Warsaw}
\usecolortheme{whale}
\useoutertheme{smoothbars}
\usefonttheme{serif}
}

\setbeamertemplate{navigation symbols}{%
%% \insertslidenavigationsymbol
%% \insertframenavigationsymbol
%% \insertsubsectionnavigationsymbol
%% \insertsectionnavigationsymbol
%% \insertdocnavigationsymbol
\insertbackfindforwardnavigationsymbol
}

\definecolor{jaunepale}{HTML}{F8F6BA}
\definecolor{jaune}{HTML}{F1EA22}
\beamerboxesdeclarecolorscheme{boxcolored}{jaune}{jaunepale}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PACKAGES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{times}
\usepackage{array}
\usepackage{multirow}
\usepackage{enumerate}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{xcolor}
\usepackage{euler}
\usepackage{stmaryrd}
%\usepackage[plain]{algorithm}
%\usepackage[noend]{algorithmic}
\usepackage[vlined,linesnumbered]{algorithm2e}
\usepackage{cental}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% TITLE PAGE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title[\insertframenumber/\inserttotalframenumber]
{Computing Semantic Relations from Heterogeneous Information Sources}

\author[Alexander Panchenko]
{Alexander Panchenko \\ {\scriptsize \url{alexander.panchenko@student.uclouvain.be} }}

\institute[UCL]
{
  Center for Natural Language Processing (CENTAL)\\
  Université catholique de Louvain
}

\date[]{\scriptsize 11 February 2010}

%\subject{WSD}

%\pgfdeclareimage[height=0.5cm]{institution-logo}{Images/cental}
%\logo{\pgfuseimage{institution-logo}}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SLIDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \setcounter{tocdepth}{1}
  \frametitle{Plan}
  \tableofcontents
  \setcounter{tocdepth}{2}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Semantic Relations}
\subsection{Definition}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relations}

\begin{block}{Semantic Relations}
A set of \textbf{semantic relations} $R$ on a set concepts or terms $C$ is a binary relation $R \subseteq C \times T \times C$, where $T$ is a set of \textbf{semantic relations types} between the concepts $R$. Thus, a semantic relation $r \in R$ is a tuple $\left\langle   c_i,t,c_j \right\rangle$, where $c_i,c_j \in C$, and $t \in T$.
\end{block}

\textbf{Examples:}

\begin{itemize}
	\item $\left\langle   \textssf{mathematics, narrower, science} \right\rangle$
	\item $\left\langle   \textssf{algebra, narrower, mathematics} \right\rangle$
	\item $\left\langle   \textssf{oil technology, related, energy technology} \right\rangle$
	\item $\left\langle   \textssf{fishing, narrower, economic activity} \right\rangle$
	\item $\left\langle   \textssf{combustion gas, equivalence, exhaust gas} \right\rangle$
	
\end{itemize}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Types}

\begin{block}{Semantic Relation Types}

The most common semantic relation types $T$ in \textit{information-retrieval thesauri} and \textit{knowledge organization systems} are \textbf{hierarchical} (broader, narrower), \textbf{equivalence}, and \textbf{associative} (related): 
		\[ \abovedisplayskip=3pt  \belowdisplayskip=3pt 
		T= \{ \textssf{broader, narrower, equivalence, related} \}.
		\]
\end{block}


\begin{center}
    \includegraphics[width=0.9\textwidth]{figures/properties}     
\end{center}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Types between Concepts (Terms) of a Thesaurus. From ~\cite{ansi2005}}

\begin{center}
    \includegraphics[width=1.0\textwidth]{figures/types}     
\end{center}

\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Applications}
\begin{frame}
\frametitle{Applications of Semantic Relations}

Semantic relations between terms and concepts are used in various automatic text processing tasks:
\begin{itemize}
	\item \textbf{Information Retrieval}: query expansion, reformulation, and suggestion (e.g.~\cite{hersh2000assessing},~\cite{aronson1994exploiting})
	\item \textbf{Natural Language Processing}: word sense disambiguation~\cite{agirre1996word}, question answering~\cite{mann2002fine}, anaphora resolution~\cite{poesio2002acquiring}.
	\item \textbf{Text Classification and Clustering} (e.g~\cite{bang2006hierarchical},~\cite{dobrov2002sigir})
	\item ...
\end{itemize}



\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Automatic Semantic Relations Construction}

\begin{frame}
\frametitle{Manually Constructed Semantic Relations}

\begin{block}{Semantic Resource}
\textbf{Semantic resource} $(C,R)$ is a set of semantic relations $R$ between a set of concepts $C$. Examples include: lexical databases (e.g. WordNet), thesauri (e.g. Eurovoc), taxonomies (e.g.NACE), ontologies (e.g. Cyc), subject headings (e.g. MeSH), etc. 
\end{block}


\begin{itemize}
	\item 
	
Applications \textbf{require} semantic resource to: 
\begin{itemize}
	\item Deal with a domain-specific vocabulary $C$
	\item In the required language
	\item With the required semantic relation types $T$
\end{itemize}

\item Often an appropriate semantic resource is \textbf{not available}.

\item \textbf{Manual} construction: 
\begin{itemize}
	\item Long process
	\item High cost, involves a lot of human labor
	\item Result is subjective
	\item Difficult to update
\end{itemize}

\end{itemize}

\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Automatic Semantic Relations Construction}


\begin{block}{Computing Semantic Relations from Data (Unsupervised Statistical)}

\begin{algorithm}[H]
\SetLine
\KwIn{ Vocabulary $C$, Information source $I$, Similarity parameter $P_{sim}$, Threshold parameter $P_{thr}$ }
\KwOut{ Set of semantic relations $R$}

$\mathbf{S} \leftarrow sim(C,I,P_{sim})$ \;
$R \leftarrow threshold(\mathbf{S},P_{thr})$ \;

\Return{ $R$ } \;
\end{algorithm}
\end{block}


\begin{itemize}
\item \textbf{Semantic similarity measure} $sim$ is high for concept pairs with ``related meanings'' (e.g. linked with equivalence, hierarchical, or associative relations), low for all the rest pairs.


\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Automatic Semantic Relations Construction}


\begin{itemize}

\item \textbf{Similarity matrix} $\mathbf{S}$ contains information about semantic similarity between concepts $C$: $s_{ij} \in [0;1],$ where $ i,j=\overline{1,N}$.

\item \textbf{Thresholding function} $threshold$ calculates semantic relations from similarity matrix e.g.
\begin{itemize}
	
	\item Similarity threshold $\alpha$: $R = \left\{ {\left\langle {{c_i},\textssf{unknown},{c_j}} \right\rangle :{s_{ij}} \ge \alpha} \right\}.$
	\item $k$ most similar concepts: $R = \left\{ {\left\langle {{c_i},\textssf{unknown},{c_j}} \right\rangle :rank_{ij} \ge k} \right\}.$
	\item ...
	
	\end{itemize}
	
	\item \textbf{Information source} $I$ is the data used by a similarity function.
\end{itemize}



\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Heterogeneous Information Sources} 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Information Sources List}
\begin{frame}
\frametitle{Text-Based Heterogeneous Information Sources, $I$.}

%\caption{}

\begin{table}
\centering

%\footnotesize
\scriptsize
%\begin{tabular}{|l|p{5.5cm}|p{2.5cm}|}	
\begin{tabular}{|l|p{4.5cm}|p{3.5cm}|}	
\hline\hline 

\textbf{Information Source} &	\textbf{Description} &	\textbf{Examples}	\\ \hline \hline
Text^{\textbf{*}}  &	Text documents, and text corpora. & BNC, ANC, Reuters, Web, \textit{Wikipedia article text} \\ \hline
Dictionary  & Dictionaries, encyclopedias, glossaries and other resources providing definitions. & COBUILD, LDOCE, WordNet, Wiktionary, \textit{Wikipedia abstracts} \\ \hline
Taxonomy  & Thesauri, taxonomies, ontologies, and other \textit{explicit} semantic resources. & Eurovoc, Agrovoc, WordNet, EuroWordNet\\ \hline
Folksonomy^{\textbf{*}}  & Folksonomies and other \textit{implicit} semantic resources. & Delicious, Flickr, \textit{Wikipedia categories} \\ \hline
Index  & Information about word counts from IR engine. & Google/Yahoo!/Yandex index \\ \hline
Query Logs  & Search query and query-click logs of information retrieval systems. & Yahoo/Google/\textit{Wikipedia query and query-click logs} \\ \hline
Surface^{\textbf{*}}  & Linguistic information about surface of lexical units. & BNC,ANC, Reuters, Web, \textit{Wikipedia article text} \\ \hline
Links^{\textbf{*}}  & Structure of hyperlinks between text documents. & HTML hyperlinks, \textit{Wikipedia hyperlinks} \\ \hline
... & ... & ...\\ \hline
\end{tabular}
\end{table}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Semantic Similarity Measures }

\begin{frame}
\frametitle{Semantic Similarity Measures, $sim$}

{ \scriptsize
\begin{tabular}{|p{2.4cm}|p{8.3cm}|}	 

\hline\hline 

\textbf{Information Source} &	\textbf{Semantic Similarity Measure}  \\ \hline \hline
Text  & BOW	Distributional Analysis e.g.~\cite{bullinaria2007extracting},  HAL~\cite{lund1996producing}, SRCR~\cite{sahlgren2001vector}. \\ \hline
Text  & Syntactic Distributional Analysis e.g.~\cite{lin1998automatic},~\cite{pada2007dependency}.  \\ \hline
Text  & Latent Semantic Analysis (LSA) e.g.~\cite{dumais1997solution}, ~\cite{wandmacher2005semantic}. \\ \hline
Text  & Collocation-Based Measures e.g.~\cite{lin1998extracting},~\cite{lemaire2004incremental}. \\ \hline
Dictionary  & Gloss-Based Measures e.g. Extended Lesk \cite{banerjee2003extended}, \cite{pedersen2004wordnet}. \\ \hline
Taxonomy, \mbox{Folksonomy}  & Path-Length-Based Measures e.g. Lin \cite{lin1998information}, Path-Length-Based \cite{leacock1998using}, Resnik \cite{resnik1995using} similarity, Jiang-Conrath distance \cite{jiang1997semantic}.   \\ \hline

Folksonomy+Text  &  Folksonomy-Based Measures e.g.~\cite{wikijl} \\ \hline
Index  & Index-Based Measures e.g. GND \cite{cilibrasi2007google} \\ \hline
Query Logs  & Query-Log-Based Measures e.g.~\cite{baeza2007extracting}, ~\cite{chien2005semantic}. \\ \hline
Surface  &  Edit Distance e.g. \cite{baroni2002unsupervised}. \\ \hline
Surface  &  Morphology-Based Measures e.g. \cite{delger2009morphosemantic}, \cite{kevers2010symbolic}. \\ \hline
Links & Hyperlinks-Based Measures e.g.~\cite{chen2003building},~\cite{ito2008association}, ~\cite{witten2008effective}. \\ \hline
... & ... & ...\\ \hline
\end{tabular}
}

\end{frame}

\begin{frame}
\frametitle{Different Aspects of Terms Relatedness}

{ \scriptsize
\begin{tabular}{|p{2.4cm}|l|p{4cm}|}	 

\hline\hline 

\textbf{Information Source} &	\textbf{Semantic Similarity Measure} & \textbf{Shows Similarity of...} \\ \hline \hline
Text  & BOW	Distributional Analysis & bag-of-words contexts of the terms \\ \hline
Text  & Syntactic Distributional Analysis & syntactic contexts of terms   \\ \hline
Text  & Latent Semantic Analysis (LSA) & truncated eigenvectors of a term-document matrix which correspond to the terms \\ \hline
Text  & Collocation-Based Measures & collocations of the terms \\ \hline
Dictionary  & Gloss-Based Measures & term definitions \\ \hline
Taxonomy, \mbox{Folksonomy}  & Path-Length-Based Measures & term locations in a semantic resource \\ \hline
Folksonomy+Text  &  Folksonomy-Based Measures & bag-of-tags/categories representing terms \\ \hline
Index  & Index-Based Measures  & term occurrence distributions in a (huge general) document collection \\ \hline
Query Logs  & Query-Log-Based Measures & bag-of-links clicked by user representing the terms \\ \hline
Surface  &  Edit Distance & text strings representing the terms  \\ \hline
Surface  &  Morphology-Based Measures & bag-of-morphemes of the terms  \\ \hline
Links & Hyperlinks-Based Measures & bag-of-links representing the terms\\ \hline
... & ... & ...\\ \hline
\end{tabular}
}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General Model for Computing Semantic Relations }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Similarity Tensor}
\begin{frame}
\frametitle{Different Semantic Similarity Measures}

\begin{itemize}
	\item Different $K$ semantic similarity measures $\{ sim_i \}$ return different similarity matrices $\{ \mathbf{S}_i \}$.
	\item Concepts $C$ are data related with multiple similarities (linkages).
\end{itemize}

\begin{center}
    \includegraphics[width=0.9\textwidth]{figures/methods}     
\end{center}

	

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Complementarity of Semantic Similarity Measures}

\begin{block}{Hypothesis of Complementarity}
Different $K$ similarity matrices $\{ \mathbf{S}_i \}$ contain \textbf{complementary information} about semantic relatedness of concepts $C$. Thus, information containing in the set of matrices $\{ \mathbf{S}_i \}$ is more complete than information from any single similarity matrix $\mathbf{S}_i,i=\overline{1,K}$.
\end{block}

\begin{itemize}
	\item Goal -- to construct a \textbf{strong} semantic similarity model from the $K$ \textbf{weak} semantic similarity models $\{ \mathbf{S}_i \}$. 
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Constructing Similarity Tensor}

\begin{itemize}
	\item We combine similarity matrices $\{ \mathbf{S}_i \}$ in a \textbf{similarity tensor} $\boldsymbol{\mathscr{S}}$:
	$$
	\boldsymbol{\mathscr{S}}_{::i} =\mathbf{S}_i, i=\overline{1,K}.
	$$
\end{itemize}


\begin{center}
    \includegraphics[width=0.9\textwidth]{figures/st}     
\end{center}

%	$$
%	\begin{array}{l}
%	\boldsymbol{\mathscr{S}}_{::1} = \text{ BOW Distibutional Similarity} (sim_1) \\
%	\boldsymbol{\mathscr{S}}_{::2} = \text{ Syntactic Distributional Similarity } (sim_2) \\
%	\boldsymbol{\mathscr{S}}_{::3} = \text{ Syntactic Distributional Similarity } (sim_3) \\
%	\boldsymbol{\mathscr{S}}_{::4} = \text{ Path-Length-Based Similarity } (sim_4) \\
%	\boldsymbol{\mathscr{S}}_{::5} = \text{ Gloss-Based Similarity } (sim_5) \\
%	\boldsymbol{\mathscr{S}}_{::K} = \text{ Hyperlink-Based Similarity } (sim_K) \\
%	\end{array}
%	$$
\textit{\scriptsize Image is adopted from \cite{dunlavy2006multilinear}. }
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Decomposing Similarity Tensor}


\begin{itemize}
	\item Decomposing similarity tensor $\boldsymbol{\mathscr{S}}$ with PARAFAC on $G$ factors:
	$$
	\boldsymbol{\mathscr{S}}  \approx  \sum^{G}_{g=1} \lambda_g \mathbf{h}_g \circ \mathbf{a}_g \circ \mathbf{c}_g  = \boldsymbol{\lambda} \llbracket  \mathbf{H},\mathbf{A},\mathbf{C} \rrbracket .
	$$
	
	\begin{center}
    \includegraphics[width=0.9\textwidth]{figures/parafac}     
\end{center}
	
	\item $H$ -- hub score for each concept $c \in C$
	\item $A$ -- authority score for each concept $c \in C$ %with respect to each community $r \in R$. 
	\item $C$ -- importance of each similarity link type $sim_i$ %with respect to each community $r \in R$.
		
\end{itemize}
\textit{\scriptsize Details of interpretation \cite{dunlavy2006multilinear}. Image is adopted from [Kolda, 2006]. }

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Decomposing Similarity Tensor}

\begin{itemize}	
	\item \textbf{Goal:} generalize/fusion relatedness information from different semantic similarity measures $\{sim_i\}$.				
	\item \textbf{Implementation:} Alternating Least Squares (ALS), and some others. 	
	\item \textbf{Alternatives} to PARAFAC~\cite{harshman1970foundations}: Tucker \cite{tucker1966some}, NTF \cite{cichocki2007nonnegative}, HOSVD \cite{de2000multilinear} and other sparse tensor decompositions.		
	
	\item \textbf{Additional feature}: community identification (clustering) by inspecting the largest values in $\mathbf{h}_g, \mathbf{a}_g$ for each of $G$ communities.
	
\end{itemize}

\begin{block}{Computing Similarity Matrix}
 	$$
	\mathbf{S} = \alpha \mathbf{HH}^T + (1-\alpha)\mathbf{AA}^T, \;\; \text{where } \alpha \in [0;1].
	$$
	\end{block}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Computing Semantic Relations with Similarity Tensor}

\begin{itemize}

\begin{block}{Computing Semantic Relations from Data (Unsupervised Statistical)}
\begin{algorithm}[H]
\SetLine
\KwIn{ Vocabulary $C$, Information sources $\{ I_i \}$, Similarity parameter $\{ P_{sim} \}$, Threshold parameter $P_{thr}$ , $\alpha$, Number of concept communities $G$ }
\KwOut{ Set of semantic relations $R$}

\For{$i\leftarrow 1$ \KwTo $K$}{
		$\mathbf{S}_i \leftarrow sim_i(C,I_i,P_{sim_i})$ \;
		$\boldsymbol{\mathscr{S}}_{::i} = \mathbf{S}_i$
}

[ $\boldsymbol{\lambda}, \mathbf{H},\mathbf{A},\mathbf{C} ] \leftarrow parafac(\boldsymbol{\mathscr{S}},G)$ \;

$\mathbf{S} \leftarrow \alpha \mathbf{HH}^T + (1-\alpha)\mathbf{AA}^T$ \;

$R \leftarrow threshold(\mathbf{S},P_{thr})$ \;
\Return{ $R$ } \;
\end{algorithm}
\end{block}
		
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Resume}

\begin{itemize}

\item Similarity tensor let us integrate various complimentary similarity measures into one model. 
\item Does not count for joint dependencies between various similarity measures. 
		
\end{itemize}
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Feature Tensor}
\begin{frame}
\frametitle{From Feature Matrix to Feature Tensor}

\begin{itemize}
	\item A \textbf{feature matrix} $\mathbf{F}$ represents concepts $C$ in a feature space $\mathcal{L}$.
	\item A \textbf{feature tensor} $\boldsymbol{\mathscr{F}}$ combines concept representations in $K$ feature spaces $\{ \mathcal{L}_i \}$ and joint dependencies between them. 
\end{itemize}


\begin{center}
    \includegraphics[width=0.7\textwidth]{figures/ft2}     
\end{center}

\textit{\scriptsize We further develop the idea proposed in \cite{van2009non}. }

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Context-Paragraph Feature Tensor}

\begin{itemize}
	\item Combination of paragraph BOW and context window BOW feature spaces.
	
	\item Obtain a three-mode feature tensor $\boldsymbol{\mathscr{F}}$:
	
\begin{center}
    \includegraphics[width=1.0\textwidth]{figures/paragraph}     
\end{center}
	
	\item $(c,w_p,w_c) -$ number of times concept $c$ occurred with word $w_c$ in a context window AND with word $w_p$ in a paragraph. 
	
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Distributional Feature Tensors}

\begin{itemize}
	\item Combination of various distributional feature spaces:
	
	\begin{itemize}
		\item Context window / sentence / paragraph / document BOW.
		\item Syntactic context.
	\end{itemize}
	
\end{itemize}


\begin{center}
    \includegraphics[width=1.0\textwidth]{figures/dist2}     
\end{center}

\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Joint Information Sources}


\begin{itemize}
	\item \textbf{Joint information sources}: Text, Surface, Hyperlinks, Folksonomy (Tags).
	\item A multi-modal document:
\end{itemize}


\begin{center}
    \includegraphics[width=0.7\textwidth]{figures/doc}     
\end{center}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Multi-modal Feature Tensors}

\begin{itemize}
	\item Combination of joint information sources 
	
\begin{center}
    \includegraphics[width=1.0\textwidth]{figures/multi}     
\end{center}
	
	\item $(c,w_c,t) -$ number of times concept $c$ occurred with word $w_c$ in a context window AND in a document with tag $t$. 
	 
\begin{center}
%    \includegraphics[width=1.0\textwidth]{figures/multi}     
\end{center}
	
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Computing Semantic Relations with Feature Tensor}

\begin{block}{Computing Semantic Relations from Data (Unsupervised Statistical)}
\begin{algorithm}[H]
\SetLine
\KwIn{ Vocabulary $C$, Joint information sources $\{ I_i \}$, Construction parameter $P_{cst}$, Threshold parameter $P_{thr}$ , Number of concept communities $G$ }
\KwOut{ Set of semantic relations $R$}

$\boldsymbol{\mathscr{F}} \leftarrow construct(C,\{ I_i \},P_{cst})$ \;

[ $\boldsymbol{\lambda}, \mathbf{C},\mathbf{F}_1,...,\mathbf{F}_{K-1} ] \leftarrow parafac(\boldsymbol{\mathscr{F}},G)$ \;

$\mathbf{S} \leftarrow \mathbf{CC}^T \;

$R \leftarrow threshold(\mathbf{S},P_{thr})$ \;
\Return{ $R$ } \;
\end{algorithm}
\end{block}
		
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Resume}

\begin{itemize}
\item Feature tensors let us integrate various complimentary concept representations and joint dependencies between them.
\item Choosing a right combination of feature spaces is not trivial, and depends on the available data.  
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{References}

\begin{frame}[t,allowframebreaks,fragile]
\frametitle{References}

{\scriptsize

\bibliographystyle{apalike} 
\bibliography{industry,papers,books,theses}

}
\end{frame}



\end{document}
