\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e} 

\newenvironment{itemize2}
       {\begin{itemize}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{itemize}}
}

\newenvironment{enumerate2}
       {\begin{enumerate}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{enumerate}}
}

\urldef{\mailsa}\path|{Firstname.Lastname}@springer.com|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Lexico-Semantic Search Engine ``Serelex''}

% a short form should be given in case it is too long for the running head
\titlerunning{Lexico-Semantic Search Engine ``Serelex''}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Alexander Panchenko\inst{1,2} \and Pavel Romanov\inst{2} \and Olga Morozova\inst{1} \and Hubert Naets\inst{1} \and Andrey Philippovich\inst{2} \and C\'{e}drick Fairon\inst{1}}


\authorrunning{Alexander Panchenko et al.}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{  Universit\'{e} catholique de Louvain, Center for Natural Language Proceessing  
\and
Bauman Moscow State Technical University, Information Systems Department
%\mailsa\\
%\mailsb\\
%\mailsc\\
\url{alexander.panchenko@uclouvain.be}
}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{ \ldots } 
\tocauthor{ \ldots }
\maketitle

\begin{abstract}

We present a system which provides given a query a list of semantically related terms. The terms are ranked accordingly to an original semantic similarity measure learned from a huge corpus. Our system performs comparably to dictionary-based baselines with no need of any semantic resource such as WordNet. The further study shows that users are completely satisfied with  70\% of query results.     

%This paper presents a novel semantic similarity measure based on lexico-syntactic patterns such as those proposed by Hearst~\shortcite{hearst1992}. The measure achieves a correlation with human judgements up to 0.739. Additionally, we evaluate it on the tasks of semantic relation ranking and extraction. Our results show that the measure provides results comparable to the baselines without the need for any fine-grained semantic resource such as WordNet. 
\keywords{ semantic similarity measure, information extraction}
\end{abstract}


\section{Introduction}

%A traditional search engine, given a query, provides a list of related documents. We present a \textit{lexico-semantic} search engine, which, for the same query, returns a list of related terms. Examples of such systems include WordNet~\footnote{\scriptsize \url{http://wordnetweb.princeton.edu/perl/webwn}}, BabelNet~\footnote{\scriptsize \url{http://lcl.uniroma1.it/bnxplorer/}}, ConceptNet~\footnote{\scriptsize \url{http://conceptnet5.media.mit.edu/}}, UBY~\footnote{\scriptsize \url{https://uby.ukp.informatik.tu-darmstadt.de/webui/tryuby/}}, \url{Thesaurus.com},  and \url{VisualSynonyms.com}.


A traditional search engine retrieves for a given query a list of related documents. We present a \textit{lexico-semantic} search engine, which returns for the same query a list of related terms (hypernyms, synonyms, etc.) ranked according to a \textit{semantic similarity measure}. Differently from thesauri (e.g. WordNet, \url{Thesaurus.com} or \url{VisualSynonyms.com}), \textit{Serelex} relies on information extracted from text corpora. In comparison to other similar systems (e.g. BabelNet~\footnote{\scriptsize \url{http://lcl.uniroma1.it/bnxplorer/}}, ConceptNet~\footnote{\scriptsize \url{http://conceptnet5.media.mit.edu/}}, UBY~\footnote{\scriptsize \url{https://uby.ukp.informatik.tu-darmstadt.de/webui/tryuby/}}), \textit{Serelex} does not depend on a semantic resource like WordNet.   


%\url{http://www.lexfn.com/}
%UWN (Universal WordNet): \url{http://www.lexvo.org/uwn/}

%In general: 
%\url{http://www.tesaurus.ru}
%\url{http://www.eat.rl.ac.uk/}

%Visual:
%\url{JeuxDeMots.org}, 
%Slovesa~\footnote{\scriptsize \url{http://www.slovesa.ru/}}
%Данный вид интерфейса в был реализован в некоторых проектах и показал свою эффективность при визуализации лексико-семантических сетей:
%\url{http://visualworld.ru/search.jsp}
%\url{http://about.viwo.ru/semantic.html}
%\url{http://visualworld.ru/search.jsp}.  
%\url{http://dev.sigwp.org/WikiVisSL.Web/Visualizer.aspx}
%\url{http://www.touchgraph.com/seo} 

%Such systems measure semantic similarity of terms. 

Three established approaches to semantic similarity are based on WordNet, dictionaries and corpora. WordNet-based measures such as \textit{WuPalmer}~\cite{wu1994verbs}, \textit{LeacockChodorow}
~\cite{leacock1998} and \textit{Resnik}~\cite{resnik1995} achieve a high precision, but suffer from a limited coverage. Dictionary-based methods such as \textit{ExtendedLesk}~\cite{banerjee2003extended}, \textit{GlossVectors}~\cite{patwardhan2006using} and \textit{WiktionaryOverlap}~\cite{zesch2008extracting} have similar properties as they rely on manually-crafted resources. Corpus-based measures such as \textit{ContextWindow}~\cite{cruys2010mining}, \textit{SyntacticContext}~\cite{cruys2010mining} or~\textit{LSA}~\cite{landauer1998introduction} provide a decent coverage as they derive similarity scores directly from a corpus. However, these measures suffer from a lower precision as they rely on a simple representation based on the vector space model.

%To overcome shortcomings of the existing systems, we build upon an  original pattern-based similarity measure~\cite{panchenko2012konvens}. The proposed system performs comparably to the baselines, but requires no semantic resources such as WordNet. Furthermore, it has better lexical coverage than the dictionary-based systems, provides an original graph-based GUI, and is open source. 


To overcome shortcomings of the existing systems, we build upon an  original pattern-based similarity measure~\cite{panchenko2012konvens}. The proposed system
performs comparably to the baselines. Furthermore, it has larger lexical coverage than the dictionary-based systems, provides a graph-based GUI, and is open source. Finally, it is automatically updated with relations extracted from new documents. 


\section{The System}

The system is available at \url{http://serelex.cental.be}. Figure~\ref{fig:architecture} presents the structure of the system. It consists of an extractor, a server and a user interface. The extractor gathers semantic relations between words from a text corpus. Extraction is done offline. The extracted relations are stored in the database. The server provides fast access to the extracted relations over HTTP. A user interacts with the system through a web interface or an API. The system as well as the data and evaluation scripts are open source~\footnote{\scriptsize \url{http://serelex.cental.be/page/about}, available under conditions of LGPLv3 license.}.


%~\footnote{\scriptsize  \url{https://github.com/cental/patternsim}}

%~\footnote{\scriptsize  \url{http://cental.fltr.ucl.ac.be/team/~panchenko/sim-eval/}}.

%Source codes of the user interface and the server are available under conditions of LGPLv3 license~\footnote{\scriptsize \url{https://github.com/pomanob/lsse}}. 
   
\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth]{figures/arch2} 
		\caption{Structure of the lexico-semantic search engine ``Serelex''.}
			\label{fig:architecture}
\end{figure}

\textbf{Extractor.} The extractor is based on the semantic similarity measure \textit{PatternSim} and \textit{Efreq-Rnum-Cfreq-Pnum} re-ranking formula~\cite{panchenko2012konvens}. This corpus-based measure relies on handcrafted lexico-syntactic patterns and some corpus statistics. %The method takes as input a text corpus and returns a sparse similarity matrix $\mathbf{S}$. An element of this matrix $s_{ij}$ is a real number within the interval $[0;1]$ which represents the strength of semantic similarity between term $c_i$ and $c_j$. 
We used a combination of Wikipedia abstracts and ukWaC~\cite{baroni2009wacky} as a  corpus (5,387,431 documents, 2.915 $\cdot 10^9$ tokens, 7,585,989 lemmas, 17.64 Gb). Processing of the corpus took around 70 hours on a standard machine (Intel i5@2.40GHz, 4Gb RAM, HDD 5400rpm).
% which takes into account frequencies of extraction, number of extracted relations of a term, frequency of a term in the corpus, and number of distinct patterns extracted a relation.
The result of the extraction is 11,251,240 untyped semantic relations (e.g. $\langle canon, nikon, 0.62 \rangle$) between 419,751 terms. 
 
%$
%s_{ij} = \sqrt{p_{ij}} \cdot \frac{2\cdot\mu_b }{b_{i*}+b_{*j}} \cdot \frac{P(c_i,c_j)}{P(c_i)P(c_j)}.
%$

%Here $P(c_i,c_j)=\frac{e_{ij}}{\sum_{ij}e_{ij}}$ is the extraction probability of the pair $\langle c_i,c_j \rangle$. This formula reduces the weight of terms which have many relations to other words. To do so it relies on the number of extractions $b_{i*}$ with a frequency superior to $\beta$: $b_{i*} = \sum_{j:e_{ij} \geq \beta} 1$. Here $b_{*j} = \sum_{i:e_{ij} \geq \beta} 1$ and $\mu_b = \frac{1}{|C|}\sum_{i=1}^{|C|} b_{i*}$ is an average number of related words per term. The formula also penalizes relations to general words, such as ``item'' with the factor $\frac{1}{P(c_i)P(c_j)}$. Here $P(c_i)= \frac{f_i}{\sum_i f_i}$ is the probability of the word $c_i$, and $f_i$ is the frequency of $c_i$ in the corpus $D$. The formula also integrates information about the number of distinct patterns $p_{ij} = \overline{1,18}$ extracted given pair of terms $\langle c_i, c_j \rangle$. Relations extracted independently by several patterns are more robust than those extracted only by a single pattern.

%Once the reranking is done, the similarity scores are mapped to the interval $[0;1]$ as follows (line 5): $\acute{\mathbf{S}} =
%\frac{\mathbf{S}-min(\mathbf{S})}{max(\mathbf{S})}$.

%\begin{figure}
%{
%\centering
%\includegraphics[width=1.0\textwidth]{figures/algorithms}
%} 
%   \end{figure}
   
%\begin{algorithm}
%\scriptsize
%\SetLine
%\KwIn{ Text corpus, $D$}
%\KwOut{ Semantic sim. matrix, $\mathbf{S}$  }

%$K \leftarrow extract\_concord(D)$ \;
%$K_{lem} \leftarrow lemmatize\_concord(K)$ \;
%$K_C \leftarrow filter\_concord(K_{lem},C)$ \;
%$\mathbf{S} \leftarrow get\_extraction\_freq(K)$ \;
%$\mathbf{S} \leftarrow rerank(\mathbf{S},D)$ \;
%$\mathbf{S} \leftarrow normalize(\mathbf{S})$ \;
%\Return $\mathbf{S}$ \;
%\caption{ Semantic sim.measure.}
%\label{alg:patterns}
%\end{algorithm}

%\begin{algorithm}
%\scriptsize
%\SetLine
%\KwIn{ Query, $q$, Dictionary, $dict$}
%\KwOut{ Lemmatized query, $q^*$  }

%\uIf{$results\_count(q) > 1$ }{\Return $q$ \;} 
%\Else{
%$lemmas \leftarrow dict.lookup\_lemmas(q) \cup q$  \;

%\uIf{ $lemmas.count() == 1$ }{\Return $lemmas[0]$ \;} 
%\uElseIf{$lemmas.count() == 0$}{ \Return $q$ \;}
%\Else{
%$i^* = \arg\max_i results\_count(lemmas[i])$ \;
%\Return $lemmas[i^*]$ \;
%}
%}

%\caption{ Query lemmatization.}
%\label{alg:patterns}
%\end{algorithm} 

\textbf{Server.} The server returns a list of related words for each query ranked according to their semantic similarity measure stored in the database. The queries are lemmatized with the DELA dictionary~\footnote{\scriptsize \url{http://infolingu.univ-mlv.fr/}, available under conditions of LGPLLR license.}. An approximate search is performed for queries with no results. 

\textbf{User Interface.} One can access the system via a graphical user interface or a RESTful API. The GUI  consists of three key elements: a search field, a list of the results and a graph of the results (see Fig.~\ref{fig:gui}). A user  interacts with the system by issuing a query -- a single word such as ``mathematics'' or a multiword expression such as ``computational linguistics''. Query suggestions are sorted by term frequency in the corpus, by query frequency, and alphabetically. A list of the results contains 20 terms which are most semantically related to the query. The graph of results provides an alternative representation of the toplist. It let visualise semantic relations of a query with a force-directed graph layout algorithm based on the Barnes-Hut simulation~\cite{barnes1986hierarchical}. Lengths of the edges are proportional to the similarity of words. User can navigate through the graph by clicking on the nodes. In this case, a new search query is generated.

%Optionally, the system displays icons from \url{TheNounProject.com} for the retrieved terms.

%~\footnote{\scriptsize A JavaScript implementation is used: \url{https://github.com/anvaka/vivagraphjs}}
% (e. g. \url{http://serelex.cental.be/find/ubuntu}) which provides results in JSON format.

%{
%\scriptsize

%\texttt{ \{ "query": "ubuntu", "model": "norm60-corpus-all",  "relations": {[} }

%\texttt{ \{ "word": "debian", "value": 0.848965058911512 \}, }

%\texttt{ \{ "word": "mandriva", "value": 0.498265193741008 \}, }

%\texttt{ \{ "word": "fedora", "value": 0.333650822618753 \}, \ldots {]} \}  }
%}


\begin{figure}
	\centering
		\includegraphics[width=0.80\textwidth]{figures/jaguar} 
		\caption{Graphical user interface of the lexico-semantic search engine ``Serelex''.}
			\label{fig:gui}
\end{figure}

\section{Evaluation and Results}

We evaluated the system against four tasks: correlations with human judgements, semantic relation ranking, extraction of semantic relations, and user satisfaction. 

\textbf{Correlation with Human Judgements.} We used two standard datasets (RG~\cite{rubenstein1965} and WordSim353~\cite{finkelstein2001placing}) to  measure Spearman's correlation with human judgements. The first two columns in Table~\ref{tbl:results} report on scores of 9 baseline similarity measures -- three WordNet-based (\textit{WuPalmer, LecockChodorow}, and \textit{Resnik}), three corpus-based (\textit{ContextWindow, SyntacticContext,} and \textit{LSA}), three definition-based (\textit{WiktionaryOverlap, GlossVectors,} and \textit{ExtendedLesk}) -- as compared to \textit{Serelex}.  Our system performs comparably to the baselines including those based on WordNet and dictionaries.

%~\cite{miller91} \cite{rubenstein1965}
%Each pair is a tuple $\langle c_i, c_j, s_{ij} \rangle$, where
%$c_i,c_j$ are terms, and $s_{ij}$ is their similarity obtained by human
%judgement.
%Let $\mathbf{s}=(s_{a1},s_{b2},\ldots,s_{zN})$ be a vector of ground truth scores,
%and $\hat{\mathbf{s}}=(\hat{s}_{a1}, \hat{s}_{b2},\ldots, \hat{s}_{zN})$ be a
%vector of similarity scores calculated with a
%similarity measure. Then, The quality of a measure is assessed with Spearman's correlation between $\mathbf{s}$ and $\hat{\mathbf{s}}$.  

\textbf{Semantic Relation Ranking.} For this task, the system should rank semantically similar pairs of terms higher than the random ones. Each ``target'' term has roughly the same number of meaningful and random ``relatums''.  We used the \textsc{BLESS}~\cite{baroniwe} dataset (26,554 relations) and the \textsc{SN}~\cite{panchenko2012study} dataset (14,682 relations). Let $R$ be a set of correct relations and $\hat{R}_k$ be a set of relations   among the top $k\%$ nearest neighbors of the target terms. Then precision and recall at $k$ are defined as follows: $P(k)=\frac{|R \cap \hat{R}_k|}{|\hat{R}_k|},R(k)=\frac{|R \cap \hat{R}_k|}{|R|}$. Table~\ref{tbl:results} and Figure~\ref{fig:eval} (a) present the performance of baseline systems and \textit{Serelex}. The performance of our system is  comparable to the baselines. In terms of precision, it outperforms all 9 baselines, but its recall is seriously lower than those of baselines. At the same time, \textit{Serelex} achieves both high recall and precision on the BLESS dataset containing only hypernyms and co-hyponyms. 

\begin{table}
\tiny
\begin{center}
\begin{tabular}{|l|l|l|llll|llll|llll|}
\hline

\multicolumn{1}{|c|}{ \bf Similarity Measure } &  \multicolumn{1}{c|}{\bf RG} & \multicolumn{1}{c|}{\bf WS} &   \multicolumn{4}{c|}{ \bf BLESS } & \multicolumn{4}{c|}{\bf SN } & \multicolumn{4}{c|}{\bf BLESS (hypo, cohypo)}  \\
\hline

 & $\rho$ & $\rho$ &  P(10) & P (20) & P(50) & R(50) &  P(10) & P(20) & P(50)  &  R(50) &  P(10) &  P(20) & P(50) &  R(50) \\
\hline 

Random  & -0.047 &   -0.122 & 0.546 & 0.542 & 0.544 & 0.522 & 0.504 & 0.502 &  0.499 & 0.498 & 0.271 & 0.279 & 0.286 & 0.502 \\ 

WordNet-WuPalmer  & 0.775  & 0.331 & 0.974 & 0.929 &  0.702 &  0.674 & 0.982 & \bf  0.959 & 0.766 & \bf 0.763 & 0.977 & 0.932 & 0.547 & \bf 0.968  \\ 

WordNet-Leack.Chod. & \bf 0.789  & 0.295 & 0.953 & 0.901 &  0.702 &  0.648 & \bf 0.984 & 0.953  & 0.757 &  0.755 & 0.951 & 0.897 & 0.542 & 0.957  \\ 

WordNet-Resnik  & 0.757  & 0.331 & 0.970 & 0.933 & 0.700 & 0.647 & 0.948 & 0.908  & 0.724 & 0.722 & 0.968 & 0.938 & 0.542 & 0.956 \\ 

Corpus-ContextWindow  & 0.782  & 0.466 & 0.971 & 0.947 &  0.836 & \bf 0.772  &  0.974 & 0.932 & 0.742 & 0.740 & 0.908 & 0.828 & 0.502 & 0.886 \\ 

Corpus-SynContext & 0.786  & 0.491 & 0.985 & 0.953 & 0.811  & 0.749 & 0.978 & 0.945 & 0.751 & 0.743 & 0.979 & 0.921 & 0.536 & 0.947 \\  

Corpus-LSA-Tasa  & 0.605  & \bf 0.566 & 0.968 & 0.937 & 0.802 & 0.740 & 0.903 & 0.846  & 0.641 & 0.609 & 0.877 & 0.775 & 0.467 & 0.824 \\ 

Dict-WiktionaryOverlap  & 0.754  & 0.521 & 0.943 & 0.905 & 0.750 & 0.679 & 0.922 & 0.887 & 0.725 & 0.656 & 0.837 & 0.769 & 0.518 & 0.739 \\
 
Dict-GlossVectors & 0.738  & 0.322 & 0.894 & 0.860 & 0.742 & 0.686  & 0.932 & 0.899 & 0.722 & 0.709 & 0.777 & 0.702 & 0.449 & 0.793 \\ 

Dict-ExtenedLesk  & 0.718  & 0.409 & 0.937 & 0.866 & 0.711 & 0.657 & 0.952 & 0.873 & 0.655 & 0.654 & 0.873 & 0.751 & 0.464 & 0.820 \\

\textbf{Serelex (PatternSim)} & 0.737 & 0.520 & \bf 0.989 & \bf 0.957 & 0.909 & 0.389 & 0.952 & 0.924 & 0.867 & 0.400 & \bf 0.985 & \bf 0.947 & 0.865 & 0.739 \\
\hline
\end{tabular}
\end{center}
\caption{ Performance of the baseline similarity measures as compared to \textit{Serelex}.}
 %on human judgements (RG, WS) and semantic relation (BLESS,  SN) datasets.  
\label{tbl:results}
\end{table}

\textbf{Semantic Relation Extraction.} We estimated precision of the extracted relations for 49 words (the vocabulary of the RG dataset). Three annotators indicated whether the terms are semantically related or not. We calculated extraction precision at $k = \{1, 5, 10, 20, 50\}$. Average precision varies between 0.736 for the top relation and 0.599 for the top 50 (see Figure \ref{fig:eval} (b)). The inter-raters agreement in terms of Fleiss's kappa is substantial (0.61-0.80).

\textbf{User Satisfaction.} We also measured user satisfaction with the search. Each of 23 assessors was asked to issue 20 queries of their choice and to rank the top 20 as relevant, non-relevant, or as a mix of the two. We collected 460 judgements from the assessors and 233 judgements from anonymous users (see Fig.~\ref{fig:eval} (c)). Users and assessors issued 594 distinct queries. According to this experiment, the results are relevant in 70\% of the cases and irrelevant in 10\% of the cases. 


\section{Conclusion}

%In this work, we presented a lexico-semantic search engine. A comparison based on the four golden standards shown that the system provides results comparable to the dictionary-based baselines and does not require manually-crafted semantic resorces. Furthermore, the system achieves Precision@1 of around 80\%, and users are complitely satisfied with search results of about 70\% of queries. 

We presented a lexico-semantic search engine. Our results have shown that the system has precision comparable to the dictionary-based baselines and has better coverage as it extracts relations directly from texts. The system achieves a Precision@1 of around 80\%, and users are satisfied with 70\% of query results. 


%In future work, we are going to use a logistic regression to choose parameter values ($\alpha$ and $\beta$) and to combine different factors ($e_{ij}, e_{i*}, P(c_i), P(c_i,c_j),p_{ij}$, etc.) in one model. %We  would also like to compliment our approach with patterns learned automatically. 

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/eval2}		
		\caption{ Evaluation: (a) precision-recall graph of the semantic relation ranking task on BLESS; (b) semantic relation extraction task; (c) users' satisfaction of top 20 results. }
	\label{fig:eval}
\end{figure}


\bibliographystyle{splncs}
\bibliography{biblio2}

\end{document}
