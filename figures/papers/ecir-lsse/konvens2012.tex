\documentclass[11pt,a4paper]{article}
\usepackage{konvens2012}
\usepackage[pass]{geometry}
\usepackage{times}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{multirow}
\usepackage{url}
\usepackage{amssymb} % NEW
\usepackage[linesnumbered,ruled,vlined]{algorithm2e} % NEW
\usepackage{graphicx} % NEW 
\usepackage[table]{xcolor} % NEW DEFINITION

% NEW
\newenvironment{itemize2}
       {\begin{itemize}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{itemize}}
}

\newenvironment{enumerate2}
       {\begin{enumerate}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{enumerate}}
}

\DeclareMathOperator*{\argmax}{arg\,max}
\setlength\titlebox{6.5cm}    % Expanding the titlebox

\title{A Semantic Similarity Measure Based on Lexico-Syntactic Patterns}

\author{Alexander Panchenko, Olga Morozova and Hubert Naets\\
%\thanks{~~The authors contributed equally to this work.} \\
  Center for Natural Language Processing (CENTAL)\\ 
	Universit\'{e} catholique de Louvain -- Belgium \\
  {\tt \{Firstname.Lastname\}@uclouvain.be} 
 \\}
 
\date{}

\begin{document}
\maketitle
\begin{abstract}
 This paper presents a novel semantic similarity measure based on lexico-syntactic patterns such as those proposed by Hearst~\shortcite{hearst1992}. The measure achieves a correlation with human judgements up to 0.739. Additionally, we evaluate it on the tasks of semantic relation ranking and extraction. Our results show that the measure provides results comparable to the baselines without the need for any fine-grained semantic resource such as WordNet. 
\end{abstract}

\section{Introduction}

Semantic similarity measures are valuable
for various NLP applications, such as relation extraction, query expansion, and short text similarity. Three well-established approaches to semantic similarity are based on WordNet~\cite{miller1995wordnet}, dictionaries and corpora. WordNet-based measures such as \textit{WuPalmer}~\shortcite{wu1994verbs}, \textit{LeacockChodorow}
~\shortcite{leacock1998} and \textit{Resnik}~\shortcite{resnik1995} achieve high precision, but suffer from  limited coverage. Dictionary-based methods such as \textit{ExtendedLesk}~\cite{banerjee2003extended}, \textit{GlossVectors}~\cite{patwardhan2006using} and \textit{WiktionaryOverlap}~\cite{zesch2008extracting} have just about the same properties as they rely on manually-crafted semantic resources. Corpus-based measures such as \textit{ContextWindow}~\cite{cruys2010mining}, \textit{SyntacticContext}~\cite{lin1998automatic} or~\textit{LSA}~\cite{landauer1998introduction} provide decent recall as they can derive similarity scores directly from a corpus. However, these methods suffer from lower precision as most of them rely on a simple representation based on the vector space model. \textit{WikiRelate}~\cite{strube2006wikirelate} relies on texts and/or categories of Wikipedia to achieve a good  lexical coverage.    

To overcome coverage issues of the resource-based techniques while maintaining their precision, we adapt an approach to semantic similarity, based on lexico-syntactic patterns. Bollegala et al.~\shortcite{bollegala2007measuring} proposed to compute semantic similarity with automatically harvested patterns. In our approach, we rather rely on explicit relation extraction rules such as those proposed by~Hearst~\shortcite{hearst1992}.  
%Crafting such rules requires linguistic expertise, but these efforts pay off with high precision of the results.

Contributions of the paper are two-fold. First, we present a novel corpus-based semantic similarity (relatedness) measure \textit{PatternSim} based on lexico-syntactic patterns. The measure performs comparably to the baseline measures, but requires no semantic resources such as WordNet or dictionaries. Second, we release an Open Source implementation of the proposed approach. 


%\begin{figure*}
%	\centering
%		\includegraphics[width=0.8\textwidth]{figures/synonym2}
%	\caption{A \textsc{Unitex} graph implementing pattern (13) for synonym extraction
%	(subgraphs are marked with gray).}
%	\label{fig:unitex}
%\end{figure*}

%; $<$E$>$ defines zero; $<$DET$>$ defines determiners;  symbols and letters outside of the boxes are markup tags)

\section{Lexico-Syntactic Patterns}
%The main graph is
%a cascade of the subgraphs, each encoding one pattern.

We extended a set of the 6 classical \newcite{hearst1992} patterns (1-6) with 12 further patterns (7-18), which aim at extracting hypernymic and synonymic relations. The patterns are encoded in finite-state transducers (FSTs) with the help of the corpus processing tool
\textsc{Unitex}~\footnote{\scriptsize 
\url{http://igm.univ-mlv.fr/~unitex/}}:
\begin{enumerate2}
  \scriptsize
\item \texttt{such NP as NP, NP[,] and/or NP;} 
\item \texttt{NP such as NP, NP[,] and/or NP;} 
\item \texttt{NP, NP [,] or other NP;} 
\item \texttt{NP, NP [,] and other NP;} 
\item \texttt{NP, including NP, NP [,] and/or NP;} 
\item \texttt{NP, especially NP, NP [,] and/or NP;}
\item \texttt{NP: NP, [NP,] and/or NP;}
\item \texttt{NP is DET ADJ.Superl NP;} 
\item \texttt{NP, e.~g., NP, NP[,] and/or NP;}
\item \texttt{NP, for example, NP, NP[,] and/or NP;} 
\item \texttt{NP, i.~e.[,] NP;} 
\item \texttt{NP (or NP);} 
\item \texttt{NP means the same as NP;} 
\item \texttt{NP, in other words[,] NP;} 
\item \texttt{NP, also known as NP;} 
\item \texttt{NP, also called NP;} 
\item \texttt{NP alias NP;} 
\item \texttt{NP aka NP.}
\end{enumerate2}

Patterns are based on linguistic knowledge and thus provide a more precise representation than co-occurences or bag-of-word models. \textsc{Unitex} makes it possible to build negative and positive contexts, to exclude meaningless adjectives, and so on. Above we presented the key features of the patterns. However, they are more complex as they take into account  variation of natural language expressions. Thus, FST-based patterns can achieve higher recall than the string-based patterns such as those used by Bollegala et al.~\shortcite{bollegala2007measuring}. 

\section{Semantic Similarity Measures}

\begin{table}
\tiny
\begin{tabular}{|l|c|c|c|c|}
  \hline              
  Name &  \# Documents & \# Tokens & \# Lemmas & Size \\ \hline \hline          
  WaCypedia & 2.694.815 & 2.026 \cdot 10^9 & 3.368.147 & 5.88 Gb \\
  ukWaC & 2.694.643 & 0.889 \cdot 10^9 & 5.469.313 & 11.76 Gb \\
  WaCypedia + ukWaC & 5.387.431 & 2.915 \cdot 10^9 & 7.585.989 & 17.64 Gb\\
  \hline  
\end{tabular}
\caption{Corpora used by the \textit{PatternSim} measure.}
\label{tbl:corpora}
\end{table}

The outline of the similarity measure \textit{PatternSim} is provided in Algorithm~\ref{alg:patterns}. The method takes as input a set of terms of interest $C$. Semantic similarities between these terms are returned in a $C \times C$ sparse similarity matrix $\mathbf{S}$. An element of this matrix $s_{ij}$ is a real number within the interval $[0;1]$ which represents the strength of semantic similarity. The algorithm also takes as input a text corpus $D$. 

As a first step, lexico-syntactic patterns are applied to the input corpus $D$ (line 1). In our experiments we used three corpora: \textsc{WaCypedia}, \textsc{ukWaC} and the combination of both (see Table~\ref{tbl:corpora}). Applying a cascade of FSTs to a corpus is a memory and CPU consuming operation. To make processing of these huge corpora feasible, we splited the entire corpus into blocks of 250 Mb. Processing such a block took around one hour on an  Intel i5 M520@2.40GHz with 4 Gb of RAM. This is the most computationally heavy operation of Algorithm~\ref{alg:patterns}. The method retrieves all the concordances matching the 18 patterns. Each concordance is marked up in a specific way: 
\begin{itemize2}
  \scriptsize
  %\item \texttt{such diverse \{[occupations]\} as
  %\{[doctors]\}, \{[engineers]\} and \{[scientists]\}[PATTERN=1]}
  \item \texttt{such \{non-alcoholic [sodas]\} as \{[root beer]\} and \{[cream soda]\}[PATTERN=1]}
  \item \texttt{\{traditional[food]\}, such as \{[sandwich]\},\{[burger]\}, and \{[fry]\}[PATTERN=2]}
\end{itemize2}
Figure brackets mark the noun phrases, which are in the semantic relation; nouns and compound nouns stand between the square brackets. We extracted 1.196.468 concordances $K$  of this type from \textsc{WaCypedia} corpus and 2.227.025 concordances from \textsc{ukWaC} -- 3.423.493 in total.

For the next step (line 2), the nouns in the square brackets are lemmatized with the
 DELA dictionary\footnote{\scriptsize Available at
\url{http://infolingu.univ-mlv.fr/}}, which consists of around 300.000 simple
and 130.000 compound words. The concordances which contain at least two terms from the input vocabulary $C$ are selected (line 3). 
%If the noun to extract is a plural form of a noun in the dictionary, then it is re-written into the
%respective singular form. 

Subsequently, the similarity matrix $\mathbf{S}$ is filled with frequencies of pairwise extractions (line 4). At this stage, a semantic similarity score $s_{ij}$ is equal to the number of
co-occurences of terms in the square brackets within the same
concordance $e_{ij}$. Finally, the word pairs are re-ranked with one of the methods described below (line 5):
  
\begin{algorithm}
\footnotesize
\SetLine
\KwIn{ Terms $C$,  Corpus $D$}
\KwOut{ Similarity matrix, $\mathbf{S}$  $[C \times C]$ }

$K \leftarrow extract\_concord(D)$ \;
$K_{lem} \leftarrow lemmatize\_concord(K)$ \;
$K_C \leftarrow filter\_concord(K_{lem},C)$ \;
$\mathbf{S} \leftarrow get\_extraction\_freq(C,K)$ \;
%\ForEach {$k \in K_C$}{
%	$C_k \leftarrow get\_concord\_terms(k,C)$ \;
%	\For{i=1,$|C_k|$}{
%		\For{j=i+1,$|C_k|$}{
%			$s_{ij} \leftarrow s_{ij} + 1$ \;
%			$s_{ji} \leftarrow s_{ij}$ \;
%		}
%	}
%}

$\mathbf{S} \leftarrow rerank(\mathbf{S},C, D)$ \;
$\mathbf{S} \leftarrow normalize(\mathbf{S})$ \;
\Return $\mathbf{S}$ \;
\caption{ Similarity measure \textit{PatternSim}.}
\label{alg:patterns}
\end{algorithm} 

\textbf{Efreq} (no re-ranking). Semantic similarity $s_{ij}$ between $c_i$ and $c_j$ is equal to the frequency of extractions $e_{ij}$ between the terms $c_i,c_j \in C$ in a set of concordances $K$.

\textbf{Efreq-Rfreq}. This formula penalizes terms that are strongly related to many  words. In this case, semantic similarity of terms equals: 
$s_{ij} = \frac{2\cdot\alpha\cdot e_{ij}}{e_{i*} + e_{*j}},$ where $e_{i*} = \sum_{j=1}^{|C|}e_{ij}$ is a number of concordances containing word $c_i$ and $\alpha$ is an expected number of semantically related words per term ($\alpha=20$). Similarly, $e_{*j} = \sum_{i=1}^{|C|}e_{ij}$. 

\textbf{Efreq-Rnum}. This formula also reduces the weight of terms which have many relations to other words. Here we rely on the number of extractions $b_{i*}$ with a frequency superior to $\beta$: $b_{i*} = \sum_{j:e_{ij} \geq \beta} 1$. Semantic ranking is calculated in this case as follows: $s_{ij} = \frac{2\cdot\mu_b \cdot e_{ij}}{b_{i*} + b_{*j}}, $ where $\mu_b = \frac{1}{|C|}\sum_{i=1}^{|C|} b_{i*}$ -- is an average number of related words per term and $b_{*j} = \sum_{i:e_{ij} \geq \beta} 1$. We experiment with values of $\beta \in \{1,2,5,10\}$. 


\textbf{Efreq-Cfreq}. This formula penalizes relations to general words, such as ``item''. According to this formula, similarity equals:   
$s_{ij} = \frac{P(c_i,c_j)}{P(c_i)P(c_j)},$
where $P(c_i,c_j)=\frac{e_{ij}}{\sum_{ij}e_{ij}}$ is the extraction probability of the pair $\langle c_i,c_j \rangle$, $P(c_i)= \frac{f_i}{\sum_i f_i}$ is the probability of the word $c_i$, and $f_i$ is the frequency of $c_i$ in the corpus. We use the original corpus $D$ and the corpus of concordances $K$ to derive $f_i$.

\textbf{Efreq-Rnum-Cfreq}. This formula combines the two previous ones: 
$s_{ij} = \frac{2\cdot\mu_b }{b_{i*}+b_{*j}} \cdot \frac{P(c_i,c_j)}{P(c_i)P(c_j)}.$

\textbf{Efreq-Rnum-Cfreq-Pnum}. This formula integrates information to the previous one about the number of patterns $p_{ij} = \overline{1,18}$ extracted given pair of terms $\langle c_i, c_j \rangle$. The patterns, especially (5) and (7), are prone to errors. The pairs extracted independently by several patterns are more robust than those extracted only by a single pattern. The similarity of terms equals in this case:
$s_{ij} = \sqrt{p_{ij}} \cdot \frac{2\cdot\mu_b }{b_{i*}+b_{*j}} \cdot \frac{P(c_i,c_j)}{P(c_i)P(c_j)}.$

Once the reranking is done, the similarity scores are mapped to the interval $[0;1]$ as follows (line 6): $\acute{\mathbf{S}} =
\frac{\mathbf{S}-min(\mathbf{S})}{max(\mathbf{S})}$. The method described above is implemented in an Open Source system \textit{PatternSim}~\footnote{\scriptsize \url{https://github.com/cental/PatternSim}} (LGPLv3).


\begin{figure*}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/pr2} 
		\caption{Precision-Recall graphs calculated on the BLESS (hypo,cohypo,mero,attri,event) dataset: (a) variations of the \textit{PatternSim} measure; (b) the best \textit{PatternSim} measure as compared to the baseline similarity measures.}
	\label{fig:prgraph}
\end{figure*}

\section{Evaluation and Results}
\label{sec:length}

We evaluated the similarity measures proposed above on three tasks -- correlations with human judgements about semantic similarity, ranking of word pairs and extraction of semantic relations.~\footnote{Evaluation scripts and the results: {\scriptsize \url{http://cental.fltr.ucl.ac.be/team/panchenko/sim-eval}}} 

\subsection{Correlation with Human Judgements} 
 We use three standard human judgement datasets --
 MC~(Miller and Charles, 1991), RG~(Rubenstein and Goodenough, 1965) and
 WordSim353~\cite{finkelstein2001placing}, composed of  30, 65, and 353 pairs
 of terms respectively.
 %~\cite{miller91} \cite{rubenstein1965}
%Each pair is a tuple $\langle c_i, c_j, s_{ij} \rangle$, where
%$c_i,c_j$ are terms, and $s_{ij}$ is their similarity obtained by human
%judgement.
%Let $\mathbf{s}=(s_{a1},s_{b2},\ldots,s_{zN})$ be a vector of ground truth scores,
%and $\hat{\mathbf{s}}=(\hat{s}_{a1}, \hat{s}_{b2},\ldots, \hat{s}_{zN})$ be a
%vector of similarity scores calculated with a
%similarity measure. Then, The quality of a measure is assessed with Spearman's correlation between $\mathbf{s}$ and $\hat{\mathbf{s}}$.  
The quality of a measure is assessed with Spearman's correlation between vectors of scores.  

The first three columns of Table~\ref{tbl:results} present the correlations. The first part of the table reports on scores of 12 baseline similarity measures: three WordNet-based (\textit{WuPalmer, LecockChodorow}, and \textit{Resnik}), three corpus-based (\textit{ContextWindow, SyntacticContext,} and \textit{LSA}), three definition-based (\textit{WiktionaryOverlap, GlossVectors,} and \textit{ExtendedLesk}), and three \textit{WikiRelate} measures. The second part of the table presents various modifications of our measure based on lexico-syntactic patterns. The first two are based on \textsc{WaCky} and \textsc{ukWaC} corpora, respectively. All the remaining \textit{PatternSim} measures are based on both corpora (\textsc{WaCky+ukWaC}) as, according to our experiments, they provide better results. Correlations of measures based on patterns are comparable to those of the baselines. In particular, \textit{PatternSim} performs similarly to the measures based on WordNet and dictionary glosses, but requires no hand-crafted resources. Furthermore, the proposed measures outperform most of the baselines on the WordSim353 dataset achieving a correlation of 0.520.  

\subsection{Semantic Relation Ranking}

In this task, a similarity measure is used to rank pairs of terms.  Each ``target'' term has roughly the same number of meaningful and random ``relatums''. A measure should rank semantically similar pairs higher than the random ones. We use two datasets: \textsc{BLESS}~\cite{baroniwe} and
\textsc{SN}~\cite{panchenko2012study}. BLESS relates 200 target nouns to 8625 relatums  with 26.554 semantic relations (14.440 are
meaningful and 12.154 are random) of the
following types:  hypernymy, co-hyponymy,  meronymy,  attribute,  event, or random. SN relates 462 target nouns to 5.910 relatum with 14.682 semantic relations (7.341 are meaningful and 7.341 are random) of the following types: synonymy, hypernymy, co-hyponymy, and random. Let $R$ be a set of correct relations and $\hat{R}_k$ be a set of semantic relations   among the top $k\%$ nearest neigbors of target terms. Then precision and recall at $k$ are defined as follows: $P(k)=\frac{|R \cap \hat{R}_k|}{|\hat{R}_k|},R(k)=\frac{|R \cap \hat{R}_k|}{|R|}$. The quality of a measure is assessed with $P(10)$, $P(20)$, $P(50)$, and $R(50)$.  

Table~\ref{tbl:results} and Figure~\ref{fig:prgraph} present the performance of baseline and pattern-based measures on these datasets. Precision of the similarity scores learned from the \textsc{WaCky} corpus is higher than that obtained from the \textsc{ukWaC}, but recall of \textsc{ukWaC} is better since this corpus is bigger (see Figure~\ref{fig:prgraph} (a)). Thus, in accordance with the previous evaluation, the biggest corpus \textsc{WaCky+ukWaC} provides better results than the \textsc{WaCky} or the \textsc{ukWaC} alone. Ranking relations with extraction frequencies (\textit{Efreq}) provides results that are significantly worse than any re-ranking strategies. On the other hand, the difference between various re-ranking formulas is small with a slight advantage for \textit{Efreq-Rnum-Cfreq-Pnum}. 
 
The performance of the \textit{Efreq-Rnum-Cfreq-Pnum} measure is  comparable to the baselines (see Figure~\ref{fig:prgraph} (b)). Furthermore, in terms of precision, it outperforms the 9 baselines, including  syntactic distributional analysis (\textit{Corpus-SyntacticContext}). However, its recall is seriously lower than the baselines because of the sparsity of the pattern-based approach. The similarity of terms can only be calculated if they co-occur in the corpus within  an extraction pattern. Contrastingly, \textit{PatternSim} achieves both high recall and precision on BLESS dataset containing only hyponyms and co-hyponyms (see Table~\ref{tbl:results}). 

\begin{table*}
\tiny
\begin{center}
\begin{tabular}{|l|l|l|l|llll|llll|llll|}
\hline

\multicolumn{1}{|c|}{ \bf Similarity Measure } &  \multicolumn{1}{c|}{ \bf MC} & \multicolumn{1}{c|}{\bf RG} & \multicolumn{1}{c|}{\bf WS} &   \multicolumn{4}{c|}{ \bf BLESS (hypo,cohypo,mero,attri,event) } & \multicolumn{4}{c|}{\bf SN (syn,  hypo, cohypo)} & \multicolumn{4}{c|}{\bf BLESS (hypo, cohypo)}  \\
\hline

& $\rho$ & $\rho$ & $\rho$ & \it P(10) & \it P (20) & \it P(50) & \it R(50) & \it P(10) & \it P(20) & \it P(50)  & \it R(50) & \it P(10) & \it P(20) & \it P(50)  & \it R(50) \\
\hline \hline 

Random  & \it 0.056 & \it -0.047 &  \it -0.122 & 0.546 & 0.542 & 0.544 & 0.522 & 0.504 & 0.502 &  0.499 & 0.498 & 0.271 & 0.279 & 0.286 & 0.502 \\ 
\hline \hline 

WordNet-WuPalmer  & 0.742  & 0.775  & 0.331 & 0.974 & 0.929 &  0.702 &  0.674 & 0.982 & 0.959 & 0.766 & \bf 0.763 & 0.977 & 0.932 & 0.547 & \bf 0.968  \\ 

WordNet-Leack.Chod.  & 0.724 & \bf 0.789  & 0.295 & 0.953 & 0.901 &  0.702 &  0.648 & \bf 0.984 & 0.953  & 0.757 &  0.755 & 0.951 & 0.897 & 0.542 & 0.957  \\ 

WordNet-Resnik  & 0.784  & 0.757  & 0.331 & 0.970 & 0.933 & 0.700 & 0.647 & 0.948 & 0.908  & 0.724 & 0.722 & \bf 0.968 & 0.938 & 0.542 & 0.956 \\ 

Corpus-ContextWindow  & 0.693  & 0.782  & 0.466 & 0.971 & 0.947 &  0.836 & \bf 0.772  &  0.974 & 0.932 & 0.742 & 0.740 & 0.908 & 0.828 & 0.502 & 0.886 \\ 

Corpus-SynContext & \bf  0.790  & 0.786  & 0.491 & 0.985 & 0.953 & 0.811  & 0.749 & 0.978 & 0.945 & 0.751 & 0.743 & 0.979 & 0.921 & 0.536 & 0.947 \\  

Corpus-LSA-Tasa  & 0.694  & 0.605  & 0.566 & 0.968 & 0.937 & 0.802 & 0.740 & 0.903 & 0.846  & 0.641 & 0.609 & 0.877 & 0.775 & 0.467 & 0.824 \\ 

Dict-WiktionaryOverlap  & 0.759  & 0.754  & 0.521 & 0.943 & 0.905 & 0.750 & 0.679 & 0.922 & 0.887 & 0.725 & 0.656 & 0.837 & 0.769 & 0.518 & 0.739 \\
 
Dict-GlossVectors & 0.653  & 0.738  & 0.322 & 0.894 & 0.860 & 0.742 & 0.686  & 0.932 & 0.899 & 0.722 & 0.709 & 0.777 & 0.702 & 0.449 & 0.793 \\ 

Dict-ExtenedLesk  & 0.792  & 0.718  & 0.409 & 0.937 & 0.866 & 0.711 & 0.657 & 0.952 & 0.873 & 0.655 & 0.654 & 0.873 & 0.751 & 0.464 & 0.820 \\

WikiRelate-Gloss  &  0.460 & 0.460  & 0.200 & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- \\ 
WikiRelate-Leack.Chod.  & 0.410  & 0.500  & 0.480 & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- \\
WikiRelate-SVM  & -- &  -- & \bf 0.590 & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- & -- \\

\hline \hline

Efreq (WaCky) & 0.522 & 0.574 & 0.405 & 0.971 & 0.950 & 0.942 & 0.289 & 0.930 & 0.912 & 0.897 & 0.306 & 0.976 & 0.937 & 0.923 & 0.626 \\
Efreq (ukWaC) & 0.384 & 0.562 & 0.411 & 0.974 & 0.944 & 0.918 & 0.325 & 0.922 & 0.905 & 0.869 & 0.329 & 0.971 & 0.926 & 0.884 & 0.653 \\ 
Efreq & 0.486 & 0.632 & 0.429 & 0.980 & 0.945 & 0.909 & \bf 0.389 & 0.938 & 0.915 & 0.866 & \bf 0.400 & 0.976 & 0.929 & 0.865 & \bf 0.739 \\
Efreq-Rfreq & \bf 0.666 & \bf  0.739 & 0.508 & 0.987 & 0.955 & 0.909 & \bf 0.389 & 0.951 & 0.922 & 0.867 & \bf 0.400 & 0.983 & 0.940 & 0.865 & \bf 0.739 \\
Efreq-Rnum & 0.647 & 0.720 & 0.499 & 0.989 & 0.955 & 0.909 & \bf 0.389 & 0.951 & 0.922 & 0.867 & \bf 0.400 & 0.983 & 0.940 & 0.865 & \bf 0.739 \\
Efreq-Cfreq  & 0.600 & 0.709 & 0.493 & 0.989 & 0.956 & 0.909 & \bf 0.389 & 0.949 & 0.920 & 0.867 & \bf 0.400 & 0.986 & 0.948 & 0.865 & \bf 0.739 \\
Efreq-Cfreq (concord.) & \bf  0.666 & \bf  0.739 & 0.508 & 0.986 & 0.954 & 0.909 & \bf 0.389 & 0.952 & 0.921 & 0.867 & \bf 0.400 & 0.984 & 0.944 & 0.865 & \bf 0.739 \\
Efreq-Rnum-Cfreq & 0.647 & 0.737 & 0.513 & 0.988 & 0.959 & 0.909 & \bf 0.389 & \bf 0.953 & 0.924 & 0.867 & \bf 0.400 & \bf 0.987 & 0.947 & 0.865 & \bf 0.739 \\
Efreq-Rnum-Cfreq-Pnum & 0.647 & 0.737 & \bf  0.520 & \bf 0.989 & 0.957 & 0.909 & \bf 0.389 & 0.952 & 0.924 & 0.867 & \bf 0.400 & 0.985 & 0.947 & 0.865 & \bf 0.739 \\
\hline
\end{tabular}
\end{center}
\caption{ Performance of the baseline similarity measures as compared to various modifications of the \textit{PatternSim} measure 
 on human judgements datasets (MC, RG, WS) and semantic relation
datasets (BLESS and SN).  }
\label{tbl:results}
\end{table*}
%The best scores in a group are in
%bold.  All correlations are significant at $p \leq 0.05$.

\subsection{Semantic Relation Extraction}

We evaluated relations extracted with the \textit{Efreq} and
 the \textit{Efreq-Rnum-Cfreq-Pnum} measures for 49 words (vocabulary of the RG dataset). Three annotators indicated whether the terms were semantically related or not. We calculated for each of 49 words extraction precision at $k = \{1, 5, 10, 20, 50\}$. Figure \ref{fig:openextr} shows the results of this evaluation. For the \textit{Efreq} measure, average precision indicated by white squares varies between 0.792 (the top relation) and 0.594 (the 20 top relations), whereas it goes from 0.736 (the top relation) to 0.599 (the 20 top relations) for the \textit{Efreq-Rnum-Cfreq-Pnum} measure. The inter-raters agreement (Fleiss's kappa) is substantial (0.61-0.80) or moderate (0.41-0.60).

\section{Conclusion}

In this work, we presented a similarity measure based on manually-crafted lexico-syntactic patterns. The measure was evaluated on five ground truth datasets  (MC, RG, WordSim353, BLESS, SN) and on the task of semantic relation extraction. Our results have shown that the measure provides results comparable to the baseline WordNet-, dictionary-, and corpus-based measures and does not require semantic resources. 

In future work, we are going to use a logistic regression to choose parameter values ($\alpha$ and $\beta$) and to combine different factors ($e_{ij}, e_{i*}, P(c_i), P(c_i,c_j),p_{ij}$, etc.) in one model. %We  would also like to compliment our approach with patterns learned automatically. 

\begin{figure}
	\centering
		\includegraphics[width=0.47\textwidth]{figures/b}		
		\caption{Semantic relation extraction: precision at $k$. }
	\label{fig:openextr}
\end{figure}
%with the two kinds of \textit{PatternSim} measure

%($e_{ij}, e_{i*}, e_{*j}, b_{ij}, b_{i*},b_{*j}, P(c_i), P(c_j), P(c_i,c_j),$ and $p_{ij}$)
 
%\section*{Acknowledgments}
%Do not number the acknowledgment section. Do not include this section when submitting your paper for review.

\bibliographystyle{konvens2012}
\bibliography{biblio2}

\end{document}
