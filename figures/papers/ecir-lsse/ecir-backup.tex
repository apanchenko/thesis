\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e} 

\newenvironment{itemize2}
       {\begin{itemize}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{itemize}}
}

\newenvironment{enumerate2}
       {\begin{enumerate}
                \vspace{-0.25em}
                 \setlength{\abovedisplayskip}{0pt}
                 \setlength{\belowdisplayskip}{0pt}
                 \setlength{\itemsep}{4pt}
                 \setlength{\parskip}{0pt}
                 \setlength{\parsep}{0pt}
                 \setlength{\topsep}{0pt}
                 \setlength{\partopsep}{0pt}
         }
         {\vspace{-0.25em}
         \end{enumerate}}
}


\urldef{\mailsa}\path|{Firstname.Lastname}@springer.com|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Lexico-Semantic Search Engine ``Serelex''}

% a short form should be given in case it is too long for the running head
\titlerunning{Lexico-Semantic Search Engine ``Serelex''}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Alexander Panchenko\inst{1,2} \and Pavel Romanov\inst{2} \and Olga Morozova\inst{1} \and Andrew Philippovich\inst{2} \and C\'{e}drick Fairon\inst{1}}


\authorrunning{Alexander Panchenko et al.}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{  Universit\'{e} catholique de Louvain, Center for Natural Language Proceessing  
\and
Bauman Moscow State Technical University, Information Systems Department
%\mailsa\\
%\mailsb\\
%\mailsc\\
\url{alexander.panchenko@uclouvain.be}
}


%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%

\toctitle{ \ldots } 
\tocauthor{ \ldots }
\maketitle

\begin{abstract}
This paper presents a novel semantic similarity measure based on lexico-syntactic patterns such as those proposed by Hearst~\shortcite{hearst1992}. The measure achieves a correlation with human judgements up to 0.739. Additionally, we evaluate it on the tasks of semantic relation ranking and extraction. Our results show that the measure provides results comparable to the baselines without the need for any fine-grained semantic resource such as WordNet. 
\keywords{ information extraction, semantic similarity measures}
\end{abstract}


\section{Introduction}

A traditional search engine given a query provides a list of related documents. A lexico-semantic search engine, for the same query returns a list of related terms. Examples of such systems include WordNet~\footnote{\scriptsize \url{http://wordnetweb.princeton.edu/perl/webwn}}, BabelNet Xplorer~\footnote{\scriptsize \url{http://lcl.uniroma1.it/bnxplorer/}}, Concept Net~\footnote{\scriptsize \url{http://conceptnet5.media.mit.edu/}}, UBY~\footnote{\scriptsize \url{https://uby.ukp.informatik.tu-darmstadt.de/webui/tryuby/}}, \url{Thesaurus.com},  and \url{VisualSynonyms.com}.


%\url{http://www.lexfn.com/}
%UWN (Universal WordNet): \url{http://www.lexvo.org/uwn/}

%In general: 
%\url{http://www.tesaurus.ru}
%\url{http://www.eat.rl.ac.uk/}

%Visual:
%\url{JeuxDeMots.org}, 
%Slovesa~\footnote{\scriptsize \url{http://www.slovesa.ru/}}
%Данный вид интерфейса в был реализован в некоторых проектах и показал свою эффективность при визуализации лексико-семантических сетей:
%\url{http://visualworld.ru/search.jsp}
%\url{http://about.viwo.ru/semantic.html}
%\url{http://visualworld.ru/search.jsp}.  
%\url{http://dev.sigwp.org/WikiVisSL.Web/Visualizer.aspx}
%\url{http://www.touchgraph.com/seo} 

Semantic similarity measure is a basis of such systems. Three established approaches to semantic similarity are based on WordNet, dictionaries and corpora. WordNet-based measures such as \textit{WuPalmer}~\cite{wu1994verbs}, \textit{LeacockChodorow}
~\cite{leacock1998} and \textit{Resnik}~\cite{resnik1995} achieve high precision, but suffer from  limited coverage. Dictionary-based methods such as \textit{ExtendedLesk}~\cite{banerjee2003extended}, \textit{GlossVectors}~\cite{patwardhan2006using} and \textit{WiktionaryOverlap}~\cite{zesch2008extracting} have just about the same properties as they rely on manually-crafted semantic resources. Corpus-based measures such as \textit{ContextWindow}~\cite{cruys2010mining}, \textit{SyntacticContext}~\cite{lin1998automatic} or~\textit{LSA}~\cite{landauer1998introduction} provide decent recall as they can derive similarity scores directly from a corpus. However, these methods suffer from lower precision as they rely on a simple representation based on the vector space model.

To overcome shortcomings of the existing systems, we rely on an  original pattern-based similarity measure. The proposed system performs comparably to the baselines, but requires no semantic resources such as WordNet. On the other hand, it provides better lexical coverage than the dictionary-based systems, implements an original graph-based user interface, and has open source codes. 

\section{The System}

The system is available at \url{http://serelex.cental.be}. General architecture of the system is presented on Figure~\ref{fig:architecture}. It consists of an extractor, a server and a user interface subsystems. The goal of the extractor is to gather semantic relations between words from a text corpus. Extraction is done offline. The extracted relations are stored in a  database. The server provides fast access to the extracted relations over HTTP. The user interacts with the system through a standard web interface or an API. Source codes of the system as well as the data and evaluation scripts are available under conditions of LGPLv3 license~\footnote{\scriptsize \url{http://serelex.cental.be/page/about}}.


%~\footnote{\scriptsize  \url{https://github.com/cental/patternsim}}

%~\footnote{\scriptsize  \url{http://cental.fltr.ucl.ac.be/team/~panchenko/sim-eval/}}.

%Source codes of the user interface and the server are available under conditions of LGPLv3 license~\footnote{\scriptsize \url{https://github.com/pomanob/lsse}}. 
   
\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth]{figures/arch2} 
		\caption{Architecture of the lexico-semantic search engine ``Serelex''.}
			\label{fig:architecture}
\end{figure}

\textbf{Extractor.} Current version of the extractor is based the semantic similarity measure \textit{PatternSim}~\cite{panchenko2012konvens}. This corpus-based measure relies on 18 manually-crafted lexico-syntactic patterns such as: 
\begin{itemize}
  \scriptsize
\item \texttt{NP, including NP, NP [,] and/or NP;} 
\item \texttt{NP, NP [,] or/and other NP;}
\item \texttt{such NP as NP, NP[,] and/or NP;}
\item \texttt{NP, especially NP, NP [,] and/or NP;}
\item \texttt{NP: NP, [NP,] and/or NP;}
\item \texttt{NP, for example, NP, NP[,] and/or NP;} 
\item \texttt{NP means the same as NP;} 
\item \texttt{NP, also called NP;} 
\end{itemize}
The patterns are encoded in a cascade of finite-state transducers (FSTs) with the help of the corpus processing tool \textsc{Unitex}~\footnote{\scriptsize 
\url{http://igm.univ-mlv.fr/~unitex}}. The outline of the similarity measure is provided in Algorithm 1. The method takes as input a text corpus $D$. Semantic similarities between terms of this corpus $C$ are returned in a $C \times C$ sparse similarity matrix $\mathbf{S}$. An element of this matrix $s_{ij}$ is a real number within the interval $[0;1]$ which represents the strength of semantic similarity between term $c_i$ and $c_j$. Thus, a semantic relation is a triple $\langle c_i, c_j, s_{ij} \rangle \in C \times C \times \mathbb{R}_{[0;1]}}$. 

As a first step, patterns are applied to the input corpus $D$ (line 1). We used a combination of Wikipedia abstracts and ukWaC~\cite{baroni2009wacky} as a text corpus (5,387,431 documents, 2.915 $\cdot 10^9$ tokens, 7,585,989 lemmas, 17.64 Gb). Processing of the corpus took around 70 hours on an Intel i5@2.40GHz with 4 Gb of RAM. The method retrieves concordances matching the patterns e.g.: 
\begin{itemize}
  \scriptsize
  %\item \texttt{such diverse \{[occupations]\} as
  %\{[doctors]\}, \{[engineers]\} and \{[scientists]\}[PATTERN=1]}
  \item \texttt{such \{non-alcoholic [sodas]\} as \{[root beer]\} and \{[cream soda]\}[PATTERN=1]}
  \item \texttt{\{traditional[food]\}, such as \{[sandwich]\},\{[burger]\}, and \{[fry]\}[PATTERN=2]}
\end{itemize}
%Figure brackets mark the noun phrases, which are in the semantic relation; nouns and compound nouns stand between the square brackets.
We extracted 3,423,493 such concordances $K$. For the next step (line 2), the nouns in the square brackets are lemmatized with the
 DELA dictionary\footnote{\scriptsize 
\url{http://infolingu.univ-mlv.fr/}}. Subsequently, the similarity matrix $\mathbf{S}$ is filled with frequencies of pairwise extractions (line 3). At this stage, a semantic similarity score $s_{ij}$ is equal to the number of
co-occurences (extractions) of terms in the square brackets within the same
concordance $e_{ij}$. Finally, the word pairs are re-ranked (line 4) with the \textit{Efreq-Rnum-Cfreq-Pnum} formula~\cite{panchenko2012konvens}: 
$
s_{ij} = \sqrt{p_{ij}} \cdot \frac{2\cdot\mu_b }{b_{i*}+b_{*j}} \cdot \frac{P(c_i,c_j)}{P(c_i)P(c_j)}.
$

Here $P(c_i,c_j)=\frac{e_{ij}}{\sum_{ij}e_{ij}}$ is the extraction probability of the pair $\langle c_i,c_j \rangle$. This formula reduces the weight of terms which have many relations to other words. To do so it relies on the number of extractions $b_{i*}$ with a frequency superior to $\beta$: $b_{i*} = \sum_{j:e_{ij} \geq \beta} 1$. Here $b_{*j} = \sum_{i:e_{ij} \geq \beta} 1$ and $\mu_b = \frac{1}{|C|}\sum_{i=1}^{|C|} b_{i*}$ is an average number of related words per term. The formula also penalizes relations to general words, such as ``item'' with the factor $\frac{1}{P(c_i)P(c_j)}$. Here $P(c_i)= \frac{f_i}{\sum_i f_i}$ is the probability of the word $c_i$, and $f_i$ is the frequency of $c_i$ in the corpus $D$. The formula also integrates information about the number of distinct patterns $p_{ij} = \overline{1,18}$ extracted given pair of terms $\langle c_i, c_j \rangle$. Relations extracted independently by several patterns are more robust than those extracted only by a single pattern.

Once the reranking is done, the similarity scores are mapped to the interval $[0;1]$ as follows (line 5): $\acute{\mathbf{S}} =
\frac{\mathbf{S}-min(\mathbf{S})}{max(\mathbf{S})}$. The result of the extraction is 11,251,240 semantic relations between 419,751 terms. 

%\begin{figure}
{
\centering
\includegraphics[width=1.0\textwidth]{figures/algorithms}
} 
%   \end{figure}
   
%\begin{algorithm}
%\scriptsize
%\SetLine
%\KwIn{ Text corpus, $D$}
%\KwOut{ Semantic sim. matrix, $\mathbf{S}$  }

%$K \leftarrow extract\_concord(D)$ \;
%$K_{lem} \leftarrow lemmatize\_concord(K)$ \;
%$K_C \leftarrow filter\_concord(K_{lem},C)$ \;
%$\mathbf{S} \leftarrow get\_extraction\_freq(K)$ \;
%$\mathbf{S} \leftarrow rerank(\mathbf{S},D)$ \;
%$\mathbf{S} \leftarrow normalize(\mathbf{S})$ \;
%\Return $\mathbf{S}$ \;
%\caption{ Semantic sim.measure.}
%\label{alg:patterns}
%\end{algorithm}

%\begin{algorithm}
%\scriptsize
%\SetLine
%\KwIn{ Query, $q$, Dictionary, $dict$}
%\KwOut{ Lemmatized query, $q^*$  }

%\uIf{$results\_count(q) > 1$ }{\Return $q$ \;} 
%\Else{
%$lemmas \leftarrow dict.lookup\_lemmas(q) \cup q$  \;

%\uIf{ $lemmas.count() == 1$ }{\Return $lemmas[0]$ \;} 
%\uElseIf{$lemmas.count() == 0$}{ \Return $q$ \;}
%\Else{
%$i^* = \arg\max_i results\_count(lemmas[i])$ \;
%\Return $lemmas[i^*]$ \;
%}
%}

%\caption{ Query lemmatization.}
%\label{alg:patterns}
%\end{algorithm} 

\textbf{Server.} The server returns a list related words for each query. It relies on the similarity matrix $\mathbf{S}$ saved in a database. The queries are lemmatized with the DELA dictionary as specified in Algorithm 2. An approximate search is performed for queries which yield no results. 

\textbf{User Interface.} Graphical user interface consists of three key elements: a search field, a list of the results and a graph of the results (see Fig.~\ref{fig:gui}). User starts interaction with the system by issuing a query -- a single word such as ``mathematics'' or a noun phrase such as ``computational linguistics''. Currently, lexicon of the system consists of 419.751 terms. Query suggestions are sorted by term frequency in the corpus, alphabetically and by query frequency. List of the results contains 20 terms most semantically similar to the query. Optionally, the system shows icons corresponding to each word (see Fig.~\ref{fig:gui}) freely available from \url{TheNounProject.com}.   

The graph of the results provides an alternative representation of the toplist. It visualizes semantic relations of the query with a force-directed graph layout algorithm~\footnote{\scriptsize A JavaScript implementation is used: \url{https://github.com/anvaka/vivagraphjs}} based on the Barnes-Hut simulation~\cite{barnes1986hierarchical}. Lengths of the edges are proportional to semantic similarity of the words. For instance, the edge ``animal-dog'' is the shortest one as it is the first search result. A user can navigate on the graph by clicking on the nodes. In this case, a new search query is issued.

One can also access the system via a RESTful API (e. g. \url{http://serelex.cental.be/find/ubuntu}) which provides results in JSON format.

%{
%\scriptsize

%\texttt{ \{ "query": "ubuntu", "model": "norm60-corpus-all",  "relations": {[} }

%\texttt{ \{ "word": "debian", "value": 0.848965058911512 \}, }

%\texttt{ \{ "word": "mandriva", "value": 0.498265193741008 \}, }

%\texttt{ \{ "word": "fedora", "value": 0.333650822618753 \}, \ldots {]} \}  }
%}

\begin{figure}
	\centering
		\includegraphics[width=0.45\textwidth]{figures/gui} 
		\caption{Graphical user interface of the lexico-semantic search engine ``Serelex''.}
			\label{fig:gui}
\end{figure}

\section{Evaluation and Results}

We evaluated the system on four tasks -- correlations with human judgements about semantic similarity, semantic relation ranking, extraction of semantic relations, and user satisfaction. 

\textbf{Correlation with Human Judgements.} We use two standard human judgement datasets RG~\cite{rubenstein1965} and WordSim353~\cite{finkelstein2001placing}, composed of  65 and 353 pairs of terms respectively.
 %~\cite{miller91} \cite{rubenstein1965}
%Each pair is a tuple $\langle c_i, c_j, s_{ij} \rangle$, where
%$c_i,c_j$ are terms, and $s_{ij}$ is their similarity obtained by human
%judgement.
%Let $\mathbf{s}=(s_{a1},s_{b2},\ldots,s_{zN})$ be a vector of ground truth scores,
%and $\hat{\mathbf{s}}=(\hat{s}_{a1}, \hat{s}_{b2},\ldots, \hat{s}_{zN})$ be a
%vector of similarity scores calculated with a
%similarity measure. Then, The quality of a measure is assessed with Spearman's correlation between $\mathbf{s}$ and $\hat{\mathbf{s}}$.  
The quality of a measure is assessed with Spearman's correlation between vectors of scores.  

The first two columns of Table~\ref{tbl:results} report on scores of 9 baseline similarity measures -- three WordNet-based (\textit{WuPalmer, LecockChodorow}, and \textit{Resnik}), three corpus-based (\textit{ContextWindow, SyntacticContext,} and \textit{LSA}), three definition-based (\textit{WiktionaryOverlap, GlossVectors,} and \textit{ExtendedLesk}) -- as compared to \textit{Serelex}.  The system performs comparably to the baselines including those based on WordNet and dictionary glosses, but requires no hand-crafted resources.

\textbf{Semantic Relation Ranking.} In this task, a similarity measure is used to rank pairs of terms.  Each ``target'' term has roughly the same number of meaningful and random ``relatums''. The system should rank semantically similar pairs higher than the random ones. We use two datasets: \textsc{BLESS}~\cite{baroniwe} containg 26,554 relations (hypernyms, co-hyponyms,  meronyms,  attributes, events, and randoms) and \textsc{SN}~\cite{panchenko2012study} containing 14,682 relations (synonyms, hypernyms, co-hyponyms, and randoms). Let $R$ be a set of correct relations and $\hat{R}_k$ be a set of semantic relations   among the top $k\%$ nearest neigbors of target terms. Then precision and recall at $k$ are defined as follows: $P(k)=\frac{|R \cap \hat{R}_k|}{|\hat{R}_k|},R(k)=\frac{|R \cap \hat{R}_k|}{|R|}$. 

Table~\ref{tbl:results} and Figure~\ref{fig:eval} (a) present the performance of baseline measures and \textit{Serelex}. The performance of our system is  comparable to the baselines. In terms of precision, it outperforms all 9 baselines, but its recall is seriously lower than those of baselines because of the sparsity of the pattern-based approach. Contrastingly, \textit{Serelex} achieves both high recall and precision on BLESS dataset containing only hyponyms and co-hyponyms. 

\begin{table}
\tiny
\begin{center}
\begin{tabular}{|l|l|l|llll|llll|llll|}
\hline

\multicolumn{1}{|c|}{ \bf Similarity Measure } &  \multicolumn{1}{c|}{\bf RG} & \multicolumn{1}{c|}{\bf WS} &   \multicolumn{4}{c|}{ \bf BLESS } & \multicolumn{4}{c|}{\bf SN } & \multicolumn{4}{c|}{\bf BLESS (hypo, cohypo)}  \\
\hline

 & $\rho$ & $\rho$ &  P(10) & P (20) & P(50) & R(50) &  P(10) & P(20) & P(50)  &  R(50) &  P(10) &  P(20) & P(50) &  R(50) \\
\hline 

Random  & -0.047 &   -0.122 & 0.546 & 0.542 & 0.544 & 0.522 & 0.504 & 0.502 &  0.499 & 0.498 & 0.271 & 0.279 & 0.286 & 0.502 \\ 

WordNet-WuPalmer  & 0.775  & 0.331 & 0.974 & 0.929 &  0.702 &  0.674 & 0.982 & 0.959 & 0.766 & \bf 0.763 & 0.977 & 0.932 & 0.547 & \bf 0.968  \\ 

WordNet-Leack.Chod. & \bf 0.789  & 0.295 & 0.953 & 0.901 &  0.702 &  0.648 & \bf 0.984 & 0.953  & 0.757 &  0.755 & 0.951 & 0.897 & 0.542 & 0.957  \\ 

WordNet-Resnik  & 0.757  & 0.331 & 0.970 & 0.933 & 0.700 & 0.647 & 0.948 & 0.908  & 0.724 & 0.722 & 0.968 & 0.938 & 0.542 & 0.956 \\ 

Corpus-ContextWindow  & 0.782  & 0.466 & 0.971 & 0.947 &  0.836 & \bf 0.772  &  0.974 & 0.932 & 0.742 & 0.740 & 0.908 & 0.828 & 0.502 & 0.886 \\ 

Corpus-SynContext & 0.786  & 0.491 & 0.985 & 0.953 & 0.811  & 0.749 & 0.978 & 0.945 & 0.751 & 0.743 & 0.979 & 0.921 & 0.536 & 0.947 \\  

Corpus-LSA-Tasa  & 0.605  & \bf 0.566 & 0.968 & 0.937 & 0.802 & 0.740 & 0.903 & 0.846  & 0.641 & 0.609 & 0.877 & 0.775 & 0.467 & 0.824 \\ 

Dict-WiktionaryOverlap  & 0.754  & 0.521 & 0.943 & 0.905 & 0.750 & 0.679 & 0.922 & 0.887 & 0.725 & 0.656 & 0.837 & 0.769 & 0.518 & 0.739 \\
 
Dict-GlossVectors & 0.738  & 0.322 & 0.894 & 0.860 & 0.742 & 0.686  & 0.932 & 0.899 & 0.722 & 0.709 & 0.777 & 0.702 & 0.449 & 0.793 \\ 

Dict-ExtenedLesk  & 0.718  & 0.409 & 0.937 & 0.866 & 0.711 & 0.657 & 0.952 & 0.873 & 0.655 & 0.654 & 0.873 & 0.751 & 0.464 & 0.820 \\

\textbf{Serelex (PatternSim)} & 0.737 & 0.520 & \bf 0.989 & 0.957 & 0.909 & 0.389 & 0.952 & 0.924 & 0.867 & 0.400 & \bf 0.985 & 0.947 & 0.865 & 0.739 \\
\hline
\end{tabular}
\end{center}
\caption{ Performance of the baseline similarity measures as compared to \textit{Serelex}  
 on human judgements (RG, WS) and semantic relation (BLESS,  SN) datasets.  }
\label{tbl:results}
\end{table}

\textbf{Semantic Relation Extraction.} We evaluated precision of the extracted relations for 49 words -- vocabulary of the RG dataset. Three annotators indicated whether the terms were semantically related or not. We calculated for each of 49 queries extraction precision at $k = \{1, 5, 10, 20, 50\}$. Figure \ref{fig:eval} (b) presents the results of this evaluation. Average precision varies between 0.736 (the top relation) and 0.599 (the 50 top relations). The inter-raters agreement in terms of Fleiss's kappa is substantial (0.61-0.80).

\textbf{User Satisfaction.} We also measured user satisfaction with the search. Each of 19 assessors was asked to issue 20 queries of their choice and rank top 20 results as relevant, non-relevant, or as a mix of the two. We collected 380 such judgements from the assessors and 136 judgements from anonymous users (see Fig.~\ref{fig:eval} (c)). Users and assessors issued 460 distinct queries. According to this study, the system provides relevant results in 70\% of the cases and unrelevant in 10\% of the cases. 

\section{Conclusion}

In this work, we presented a similarity measure based on manually-crafted lexico-syntactic patterns. The measure was evaluated on five ground truth datasets  (MC, RG, WordSim353, BLESS, SN) and on the task of semantic relation extraction. Our results have shown that the measure provides results comparable to the baseline WordNet-, dictionary-, and corpus-based measures and does not require semantic resources. 

%In future work, we are going to use a logistic regression to choose parameter values ($\alpha$ and $\beta$) and to combine different factors ($e_{ij}, e_{i*}, P(c_i), P(c_i,c_j),p_{ij}$, etc.) in one model. %We  would also like to compliment our approach with patterns learned automatically. 

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/eval}		
		\caption{ Evaluation: (a) precision-recall graph of semantic relation ranking task; (b) semantic relation extraction task; (c) users' satisfaction of top 20 results. }
	\label{fig:eval}
\end{figure}


\bibliographystyle{splncs}
\bibliography{biblio2}

\end{document}
