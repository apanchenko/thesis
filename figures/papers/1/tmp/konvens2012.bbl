\begin{thebibliography}{}

\bibitem[\protect\citename{Banerjee and Pedersen}2003]{banerjee2003extended}
Satanjeev Banerjee and Ted Pedersen.
\newblock 2003.
\newblock {Extended gloss overlaps as a measure of semantic relatedness}.
\newblock In {\em IJCAI}, volume~18, pages 805--810.

\bibitem[\protect\citename{Baroni and Lenci}2011]{baroniwe}
Marco Baroni and Alexandro Lenci.
\newblock 2011.
\newblock How we blessed distributional semantic evaluation.
\newblock {\em GEMS (EMNLP), 2011}, pages 1--11.

\bibitem[\protect\citename{Bollegala \bgroup et al.\egroup
  }2007]{bollegala2007measuring}
D.~Bollegala, Y.~Matsuo, and M.~Ishizuka.
\newblock 2007.
\newblock Measuring semantic similarity between words using web search engines.
\newblock In {\em WWW}, volume 766.

\bibitem[\protect\citename{Finkelstein \bgroup et al.\egroup
  }2001]{finkelstein2001placing}
Lev Finkelstein, Evgeniy Gabrilovich, Yossi Matias, Ehud Rivlin, Zach Solan,
  Gadi Wolfman, and Eytan Ruppin.
\newblock 2001.
\newblock Placing search in context: The concept revisited.
\newblock In {\em WWW 2001}, pages 406--414.

\bibitem[\protect\citename{Hearst}1992]{hearst1992}
Marti~A. Hearst.
\newblock 1992.
\newblock Automatic acquisition of hyponyms from large text corpora.
\newblock In {\em ACL}, pages 539--545.

\bibitem[\protect\citename{Landauer and Dumais}1997]{landauer1997solution}
Thomas~K. Landauer and Susan~T. Dumais.
\newblock 1997.
\newblock A solution to plato's problem: The latent semantic analysis theory of
  acquisition, induction, and representation of knowledge.
\newblock {\em Psych. review}, 104(2):211.

\bibitem[\protect\citename{Leacock and Chodorow}1998]{leacock1998}
Claudia Leacock and Martin Chodorow.
\newblock 1998.
\newblock {Combining Local Context and WordNet Similarity for Word Sense
  Identification}.
\newblock {\em An Electronic Lexical Database}, pages 265--283.

\bibitem[\protect\citename{Lin}1998]{lin1998automatic}
Dekang Lin.
\newblock 1998.
\newblock {Automatic retrieval and clustering of similar words}.
\newblock In {\em ACL}, pages 768--774.

\bibitem[\protect\citename{Miller and Charles}1991]{miller91}
George~A. Miller and Walter~G. Charles.
\newblock 1991.
\newblock {Contextual correlates of semantic similarity}.
\newblock {\em Language and Cognitive Processes}, 6(1):1--28.

\bibitem[\protect\citename{Miller}1995]{miller1995wordnet}
G.~A. Miller.
\newblock 1995.
\newblock Wordnet: a lexical database for english.
\newblock {\em Communications of ACM}, 38(11):39--41.

\bibitem[\protect\citename{Panchenko and Morozova}2012]{panchenko2012study}
A.~Panchenko and O.~Morozova.
\newblock 2012.
\newblock A study of hybrid similarity measures for semantic relation
  extraction.
\newblock {\em Innovative Hybrid Approaches to the Processing of Textual Data,
  (EACL)}, pages 10--18.

\bibitem[\protect\citename{Patwardhan and Pedersen}2006]{patwardhan2006using}
Siddharth Patwardhan and Ted Pedersen.
\newblock 2006.
\newblock {Using WordNet-based context vectors to estimate the semantic
  relatedness of concepts}.
\newblock {\em Making Sense of Sense: Bringing Psycholinguistics and
  Computational Linguistics Together}, page~1.

\bibitem[\protect\citename{Paumier}2003]{paumier2003}
S\'ebastien Paumier.
\newblock 2003.
\newblock {\em {De la reconnaissance de formes linguistiques \`a l'analyse
  syntaxique}}.
\newblock {Ph.D.} thesis, Universit\'e de Marne-la-Vall\'ee.

\bibitem[\protect\citename{Resnik}1995]{resnik1995}
Philip Resnik.
\newblock 1995.
\newblock {Using Information Content to Evaluate Semantic Similarity in a
  Taxonomy}.
\newblock In {\em IJCAI}, volume~1, pages 448--453.

\bibitem[\protect\citename{Rubenstein and Goodenough}1965]{rubenstein1965}
Herbert Rubenstein and John~B. Goodenough.
\newblock 1965.
\newblock Contextual correlates of synonymy.
\newblock {\em Communications of the ACM}, 8(10):627--633.

\bibitem[\protect\citename{Van~de Cruys}2010]{cruys2010mining}
Tim Van~de Cruys.
\newblock 2010.
\newblock {\em {Mining for Meaning: The Extraction of Lexico\-semantic
  Knowledge from Text}}.
\newblock {Ph.D.} thesis, University of Groningen.

\bibitem[\protect\citename{Wu and Palmer}1994]{wu1994verbs}
Zhibiao Wu and Martha Palmer.
\newblock 1994.
\newblock {Verbs semantics and lexical selection}.
\newblock In {\em Proceedings of ACL'1994}, pages 133--138.

\bibitem[\protect\citename{Zesch \bgroup et al.\egroup
  }2008]{zesch2008extracting}
Torsen Zesch, Christof M{\"u}ller, and Irina Gurevych.
\newblock 2008.
\newblock Extracting lexical semantic knowledge from wikipedia and wiktionary.
\newblock In {\em Proceedings of LREC'08}, pages 1646--1652.

\end{thebibliography}
