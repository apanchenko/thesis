\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}

\usepackage{url}
\urldef{\mailsa}\path|{Firstname.Lastname}@springer.com|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Detection of Child Sexual Abuse Media: Classification of the Associated Filenames }

% a short form should be given in case it is too long for the running head
\titlerunning{Detection of Child Sexual Abuse Media}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Alfred Hofmann \and Ursula Barth\and Ingrid Haas\and Frank Holzwarth}
%
\authorrunning{Alfred Hofmann et al.}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{Springer-Verlag, Computer Science Editorial, Germany\\
\mailsa\\
\mailsb\\
\mailsc\\
}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%


\toctitle{Detection of Child Sexual Abuse Media } 
\tocauthor{Alfred Hofmann et al.}
\maketitle

\begin{abstract}
%The goal of our project is to develop a software for identification of child abuse media and its  originators on peer-to-peer (P2P) networks.

This paper address the problem of automatic pedophile content identification.  We present a system for filename categorization. In our initial experiments, we used regular pornography data as a substitution of child pornography. The system separates filenames of pornographic media from the others with an accuracy up to 91\%.
\keywords{short text classification, P2P networks}
\end{abstract}

\section{Introduction}

The goal of the XYZ project\footnote{ \scriptsize
%\url{http://scc-sentinel.lancs.ac.uk/XYZ/}
\url{http://anonymized.uri.com/}
} is to develop a toolkit to help law enforcement agencies across the EU identify child sexual abuse (CSA) media and its originators on P2P networks. Until now, the usual way to identify such media was through manual analysis. Such an approach is impractical as thousands of new files appear every day. We describe a text processing module of XYZ, designed to recognize the CSA media by their filenames. These media are further prioritized with a content-based media analysis~\cite{ulges2011automatic} and a user behaviour analysis.

Text categorization is a task which received much attention and robust methods were developed~\cite{sebastiani2002machine,joachims98}. Filename classification is a special case of short text classification~\cite{sriram2010short}. This task is challenging as filenames may be very short and/or meaningless. Recent research on cyberpedophilia is focused on chat analysis. 
Pendar et al.~\cite{pendar2007toward} have built a system which separates a predator and a victim based on chats from the {\footnotesize \url{perverted-justice.com}}. McGhee et al.~\cite{mcghee2011learning} have  used the same dataset to classify chat lines. Bogdanova et al.~\cite{bogdanova2012impact} have  built a system which identifies  pedophiles among chat users based on emotion-based features from the same chat data. Peersman et al.~\cite{peersman2011predicting} have  built a system for age detection in chats  based on Netlog corpus~\cite{kestemont2012netlog}. A shared task ``Sexual Predator Identification'' has been introduced at PAN'12~\cite{kontostathisidentifying,peersman2012conversation}. To the best of our knowledge, identification of CSA media based on its text description was not yet investigated. 

Contributions of the paper are the following. First, we present two datasets which can be used to train/test filename classifiers. Second, we perform a computational study of various approaches to filename classification. Finally, we present an open source system for short text classification, which recognizes pornographic filenames with an accuracy up to 91\%. It implements an original \textit{vocabulary projection} technique, which helps to overcome vocabulary mismatch. 

%The key output of the project -- the XYZ software toolkit -- will be used by law enforcement to help detect, filter, and prioritize new instances of child abuse media on P2P networks. XYZ system is operates alongside existing P2P monitoring tools like PeerPrecision. Using trace data from these monitors as input, XYZ identifies candidate suspect media that contain Child Sexual Abuse (CSA) based on a combination of advanced pattern recognition techniques. The language analysis tool detects potential CSA media in P2P networks based on a combination of several sources of evidence: the modelling of offender file sharing behaviour; the analysis of file sharing patterns and query patterns; language processing of file names and queries. Candidate media discovered by language analysis are fed to a content-based media analysis tool, which prioritizes and filters material further. 

%Thus, the XYZ system performs automatic identification of new CSA content and its distributors, using evidence in the form of textual queries and filenames in P2P networks, of user's file sharing behaviour, and of the image and video content itself. It is of particular importance to search for new material as these previously unknown files are usually introduced on the network by people who one believed to be close to the victim or the abuse. These files have also a greater chance to be related to on-going situation of abuse. 


%This paper describes language processing module of XYZ which recognizes the CSA media by their filenames. In the following section, we outline the architecture of the module. Next, we present classification results on the ``standard" pornography data used as a substitute for child pornography data.  

\section{Filename Classification}

%Figure~\ref{fig:architecture} presents structure of the language analysis module and outlines how it is integrated with the XYZ toolkit.

The file classification module~\footnote{\scriptsize The source codes as well as the datasets are available from \url{http://anonymized.uri.com/}} is designed to recognize  abusing media based on textual descriptions of the associated files (titles, descriptions, etc.). The module consists of a feature extractor and a classifier.  
 %(e.g. \texttt{\foonotesize{0012664321.mpg}})

%\begin{figure}
%\centering
%\includegraphics[height=3.2cm]{figures/architecture}
%\caption{Structure of the language analysis module of the XYZ forensics toolkit. }
%\label{fig:architecture}
%\end{figure}

\textbf{Feature Extraction.} First, the textual description of a file  (title, tags, and description) is tokenized with a special procedure taking into account common separators such as underscores. Texts are cleaned up from numbers and special symbols. Next, the filename is lemmatized with \textsc{TreeTagger}~\cite{schmid1994probabilistic}. The standard stopwords are removed with the exception of the ``sex-related'' ones such as ``him'', ``her'', ``woman'', etc. Finally, a file is represented as a unit length bag-of-words vector of lemmas. %We did not perform any feature selection -- all extracted lemmas are used as features. 

Filenames usually contain only a few meaningful words. If none of them match the vocabulary of a pre-trained classifier, then classification is not possible. To deal with this issue we have developed a technique of \textit{vocabulary projection}. It projects an out of vocabulary lemma into the vocabulary of a classifier with help of 11,251,240 semantic relations over 419,751 terms learned from \textsc{WakCy+ukWaC} text corpus~\cite{baroni2009wacky} with the \textit{PatternSim} semantic similarity measure~\cite{panchenko2012konvens}. This procedure, for each out-of-vocabulary lemma $w$ looks up $n \in [10;20]$ most semantically similar words. Then, those related words which are present in the vocabulary of the classifier are added to the bag-of-words vector instead of $w$.    

%These language phenomena hamper standard text classifiers, which stumble against big number of out-of-vocabulary words (those which were not present in a training dataset). This issue is particularly severe for filenames which contain only few meaningful words.   


% Text normalization consists of rewriting text using a more conventional spelling in order to make it more readable for a human or for a machine. Hence, this module takes a potentially noisy text as input, and produces the corresponding cleaned text as output. Filenames and their text descriptions contain highly non-standard language patterns, such as abridgments, abbreviations, spelling errors, and so on. These language phenomena hamper standard text classifiers, which stumble against big number of out-of-vocabulary words. Therefore, the goal of the text normalization is to improve the feature extraction procedure. For instance, we would prefer to treat all variations of the word ``porn'' including its abbreviations and forms with spelling errors as a single feature. 

%The text normalization is performed by an algorithm which learns rewriting rules from a parallel aligned corpora. This normalization stems from previous works on SMS normalization~\cite{beaufort2010b}. This method shares similarities with both spell checking and machine translation approaches. In our system, all lexicons, language models and sets of rules are compiled into finite-state machines and combined with the input filename by composition, a special operation defined on weighted transducers and on weighted automata. We use our own finite-state tools: a finite-state machine library and its associated compiler~\cite{beaufort2008a}. In conformance with the format of the library, the compiler builds finite-state machines from weighted rewrite rules, weighted regular expressions and n-gram models. The algorithm and its models are described in~\cite{beaufort2010b}. In order to learn a normalization model we apply a sequence alignment at the character-level obtained with algorithm described in~\cite{beaufort2009b}. Natural language analysis is then applied on cleaned texts using a morphosyntactic analysis and lemmatization. At this point we rely on the natural language processing system \textit{Alias}~\cite{beaufort2008a} which is also based on weighted transducers and automata.  

\textbf{Classification.} Binary statistical classifiers are used to separate regular files from those containing pornographic content. We experimented with Support Vector Machines, Regularized Logistic Regression, Perceptron, Least Square Method, and Least Squares. In our experiments, we rely on implementation available from \textsc{LibSVM}~\cite{chang2011libsvm}, \textsc{LibLINEAR}~\cite{fan2008liblinear}, and MATLAB. Cross-validation was used to assess performance of the classifiers. In order to avoid overfitting, vocabulary of the classifier was extracted from the training fold only.

%A training dataset is a set of tuples $\langle class, filename\rangle$,  where  $filename$ is a text (title, tags and description of associated with a file) and $class$ is either "positive" or "negative".  

\textbf{Training Data.} In our system, positive training examples are texts associated with pornographic resources, while negative training examples are texts associated with non-pornographic content. In the experiments described here, ``regular'' pornographic filenames were used as a substitute for child pornography filenames. First, such data share important characteristics with CSA material like sex-related vocabulary, file extensions, etc; CSA is a special case of pornographic data.  Second, at the current stage of the project, CSA data were not yet provided by our law enforcement collaborators. Thus, we constructed ourselves two  datasets from openly available data: \textit{Gallery} and \textit{PirateBay}. 

The \textit{Gallery} dataset contains 106,350 texts. Positive examples of this dataset were gathered from four  sites: PicHunter, PornoHub, RedTube and Xvideos~\footnote{\scriptsize \url{www.pichunter.com}, \url{www.porno-hub.com}, \url{www.redtube.com}, \url{www.xvideos.com}}. Each of 51,350 positive training examples is composed of a title and tags of a porn video or a porn gallery e.g.:

\begin{itemize}  
  \item \texttt{\footnotesize Husband catches his wife fucking with his brother}
  \item \texttt{\footnotesize Slim Can Bearly Take The Dick}
\end{itemize}

Negative training examples in this dataset are 55,000 randomly selected titles from the English Wikipedia, each composed of at least 4 words e.g.:
\begin{itemize}  
  \item \texttt{\footnotesize Contractors and General Workers Trade Union}
  \item \texttt{\footnotesize 1957-58 American Hockey League season}  
\end{itemize}

%Thus, the dataset is composed of 106.350 titles. Examples of two negative and two positive training examples from this dataset are provided below:%in Figure~\ref{fig:data}.

%{
%\scriptsize
%\begin{itemize}
%\item \texttt{<text class='negative'>}
%\texttt{<original>Contractors and General Workers Trade Union</original>}

%\texttt{<lemmas>contractors#NNS#contractor and#CC#and general#JJ#general workers#NNS#worker trade#NN#trade union#NN#union</lemmas></text>}

%<text class='negative'>
%<original>1957-58 American Hockey League season</original>
%<lemmas>1957-58#CD#1957-58 american#JJ#American hockey#NN#hockey league#NN#league season#NN#season</lemmas>
%</text>

%\item \texttt{<text class='positive'>}
%\texttt{<original>Husband catches his wife fucking with his brother .</original>}
%\texttt{<lemmas>husband#NN#husband catches#VVZ#catch his#PP$#his wife#NN#wife fucking#VVG#fuck with#IN#with his#PP$#his                brother#NN#brother .#SENT#.</lemmas></text>}


%<text class='positive'>
%<original>Slim Can Bearly Take The Dick .</original>
%<lemmas>slim#JJ#slim can#MD#can bearly#RB#bearly take#VV#take the#DT#the dick#NN#dick .#SENT#.</lemmas>
%</text>
%\end{itemize}
%}

The \textit{PirateBay} dataset consists of data available from ThePirateBay torrent tracker~\footnote{\scriptsize\url{http://thepiratebay.org/}}. The files of this tracker are organized in six main categories such as ``applications'' or ``porn'', and 39 subcategories such as ``applications-android'' or ``porn-movies''. We crawled titles, tags and descriptions of 100,064 files from all  categories. So each sub-category is represented with around 3,000 files. From this material, we constructed a dataset with 16,863 positive training examples (porn category) and 83,201 negative training examples (all other categories). We constructed two versions of this dataset. The first includes texts and tags associated with the files, while the second consists of texts, descriptions and tags.

%It is important to mention that since the text normalization component was not yet fully integrated in the language analysis module, we used a simplified text normalization procedure in the experiment described here. First, the titles were cleaned up from the numbers and the special symbols. Second, they were POS tagged and lemmatized with TreeTagger~\cite{schmid1994probabilistic}.

%\begin{figure}
%\centering
%\includegraphics[height=1.8cm]{figures/ucl-data-2}
%\caption{Gallery dataset: examples of negative and positive training texts. }
%\label{fig:data}
%\end{figure}

%<text class='negative'>
%<original>Contractors and General Workers Trade Union</original>
%<lemmas>contractors#NNS#contractor and#CC#and general#JJ#general workers#NNS#worker trade#NN#trade union#NN#union</lemmas>
%</text>

%<text class='negative'>
%<original>1957-58 American Hockey League season</original>
%<lemmas>1957-58#CD#1957-58 american#JJ#American hockey#NN#hockey league#NN#league season#NN#season</lemmas>
%</text>

%<text class='positive'>
%<original>Husband catches his wife fucking with his brother .</original>
%<lemmas>husband#NN#husband catches#VVZ#catch his#PP$#his wife#NN#wife fucking#VVG#fuck with#IN#with his#PP$#his                brother#NN#brother .#SENT#.</lemmas>
%</text>

%<text class='positive'>
%<original>Slim Can Bearly Take The Dick .</original>
%<lemmas>slim#JJ#slim can#MD#can bearly#RB#bearly take#VV#take the#DT#the dick#NN#dick .#SENT#.</lemmas>
%</text>

\begin{table}
\tiny
\centering
\begin{tabular}{|l|l|c|}
\hline
\bf Classifier & \bf Training Time & \bf Accuracy \\ \hline   

\bf C-SVM, linear kernel & 8m 59s & \bf 96.97 \\
C-SVM, linear kernel (L2-reg., L2-loss) & 0.459s & 96.52 \\ %primal
%C-SVM, linear kernel (L2-reg., L1-loss, dual) & 0.366s & 96.47 \\
C-SVM, linear kernel (L1-reg., L2-loss) & 0.162s & 96.47 \\
%C-SVM, linear kernel (L2-reg., L2-loss, dual) & 0.364s & 96.45 \\
Least Squares (sum of error squares) &	2m 9s &	 96.40  \\
Logistic Regression (L2-reg.) & 1.176s & 96.27 \\
Logistic Regression (L2-reg., L2-loss) & 0.548s & 96.24 \\ %primal
Perceptron ($\epsilon$ $\leq 1\%$, 570 iterations) &	12m 30s & 94.03  \\
Logistic Regression (L1-reg.) & 0.388s & 93.95 \\
Least Mean Squares (LMS), $\rho = 10$ & 6.88s & 91.85  \\
$\nu$-SVM, RBF kernel & 26m 49s & 88.35 \\
$\nu$-SVM, linear kernel & 12m 48s & 88.20 \\
$\nu$-SVM, sigmoid kernel & 14m 5s & 87.45 \\
$\nu$-SVM, polynomial  kernel & 4m 39s & 79.77  \\
C-SVM, polynomial kernel & 15m 11s & 51.71 \\
C-SVM, RBF kernel & 22m 20s & 51.71 \\
C-SVM, sigmoid kernel & 14m 58s & 51.71 \\
\hline

\end{tabular}
\caption{ Classifiers with default meta-parameters (\textit{Gallery} dataset). }
\label{tbl:results}
\end{table}

\section{Results}

The experiments with various learning algorithms are presented in  Table~\ref{tbl:results}. Our results suggest that a Support Vector Machine or a Logistic Regression distinguishes a pornographic text from a non-pornographic one with an accuracy up to 97\%. In particular, the best results (96.97\%) are obtained by a C-SVM with linear kernel. Fig.~\ref{fig:gallery2pirate} (b) depicts results of the metaparameter optimization of this model with the grid search. As one can see, this procedure improves the accuracy only by 0.37\%. Such a small variance of the model is useful for our application as the system should be automatically retrained by the Police on  real data.

Table~\ref{tbl:results2} reports on performance of the best model trained and applied to different datasets. As one can observe, the classifier is able to model well both \textit{Gallery} and \textit{PirateBay} datasets -- accuracy of classification achieves 97-98\%. Furthermore, the model does not seem to be particularly overfitted. Accuracy of the classifier trained on the \textit{Gallery} dataset and applied on the \textit{PirateBay} dataset and vise-versa achieves up to 91\%. Figure~\ref{fig:gallery2pirate} (a) presents further information about  the classifier trained on the \textit{Gallery} dataset and tested on the \textit{PirateBay} dataset. The two constantly misclassified sub-categories are ``other-other'' and ``porn-games''. Filenames of the latter are indeed difficult to classify as they are similar to those of video games (e.g. ``3D SexVilla Crack'').


According to the results summarized in Fig.~\ref{fig:gallery2pirate} (a) and Table~\ref{tbl:results2}, training a model on the noisy descriptions of the \textit{PirateBay} seems to hamper accuracy of the classifier by around 3\%. On the other hand, using those descriptions at the classification time provides an improvement of 6\%. Finally, the \textit{vocabulary projection} technique indeed helps to deal with the vocabulary mismatch problem for short input texts (e.g. \textit{PirateBay Title+Tags}). It improves accuracy of 4.66\% in comparison to the baseline.   


%definitely helps. For instance, accuracy of the classifier trained on the \textit{Gallery} dataset and applied on the \textit{PirateBay Title+Desc+Tags} is around 6\% higher as compared to the \textit{PirateBay Title+Tags} dataset.    


\begin{table}
\tiny

%\footnotesize
\centering
\begin{tabular}{|l|l|l|l|}

\hline
\bf Training Dataset & \bf Test Dataset & \bf Accuracy  & \textbf{Accuracy} (Voc. projection) \\ \hline   

Gallery (train) & Gallery (valid) & 96.41 & \textbf{96.83} (+0.42) \\
PirateBay Title+Desc+Tags (train) & PirateBay Title+Desc+Tags (valid) &  \textbf{98.92} & 98.86 (-0.06)\\
PirateBay Title+Tags (train) & PirateBay Title+Tags (valid) & \textbf{97.73} & 97.63 (-0.10) \\
Gallery (train) & PirateBay Title+Desc+Tags (train) & 90.57 & \textbf{91.48} (+0.91) \\
Gallery (train) & PirateBay Title+Tags  & 84.23 & \textbf{88.89} (+4.66) \\
PirateBay Title+Desc+Tags (train) & Gallery (train)  & 88.83 & \textbf{89.04} (+0.21) \\
PirateBay Title+Tags (train) & Gallery (train) & 91.16 & \textbf{91.30} (+0.14) \\
\hline

\end{tabular}
\caption{ Performance of the C-SVM (linear, L2-reg., L2-loss) classifier. }
\label{tbl:results2}

\end{table}

\begin{figure}
\centering
\includegraphics[height=3.4cm]{figures/gallery2pirate} 
\includegraphics[height=3.3cm]{figures/ucl-scale}

\caption{Classifier (C-SVM, linear, L2-reg., L2-loss) trained on the \textit{Gallery} dataset: (a) classification of the \textit{PirateBay} dataset; (b) optimization of the metaparameters. }
\label{fig:gallery2pirate}
\end{figure}

%\begin{figure}
%\centering
%\includegraphics[height=5.2cm]{figures/ucl-prediction-ratio}
%\caption{Prediction speed function on the test sample size. }
%\label{fig:speed}
%\end{figure}

\section{Conclusion}

We have presented a filename classification module of XYZ. 
Our results confirm the correctness of the chosen methodology for filename classification as the system achieves accuracy of 91\% when trained and tested on independent datasets. At the next step, we are going to use the system for the classification of different kinds of porn, and to distinguish CSA media from other porn data. %We expect that the latter task CSA  from regular porn is much more challenging.

 %provided by our police collaborators. When deployed, the system will be autonomously retrained on the new data. We expect that this will not hamper the performance, as our approach does not require meta-parameter tuning.  
 
%However, separating CSA from regular porn is much more challenging. 
  
%A C-SVM with linear kernel trained on an unigram model seems to be a suitable to filename categorization. Feature mapping technique helps to deal with vocabulary mismatch between training and test data.  

%In this paper, we described a text-based filename classifier, designed to identify child abuse media on peer-to-peer (P2P) networks. Our method relies on a statistical text classifier and a statistical text normalizer. We conducted extensive experiments on the standard porn data crawled from popular web galleries and torrent trackers. These data were used as a substitute of child pornography data. Our experiments have shown that the classifier based on the Support Vector Machine and the unigram model is able to distinguish filenames of pornographic media  from the filenames of other media with accuracy of 90-98\%, depending on the dataset.  

%
%\subsubsection*{Acknowledgments.} The heading should be treated as a
%subsubsection heading and should not be assigned a number.

\bibliographystyle{splncs}
\bibliography{all}

\end{document}
