\documentclass[runningheads,a4paper]{llncs}

\usepackage{amssymb}
\setcounter{tocdepth}{3}
\usepackage{graphicx}

\usepackage{url}
\urldef{\mailsa}\path|{Firstname.Lastname}@springer.com|    
\newcommand{\keywords}[1]{\par\addvspace\baselineskip
\noindent\keywordname\enspace\ignorespaces#1}

\begin{document}

\mainmatter  % start of an individual contribution

% first the title is needed
\title{Detection of Child Sexual Abuse Media: Classification of the Associated Filenames }

% a short form should be given in case it is too long for the running head
\titlerunning{Detection of Child Sexual Abuse Media}

% the name(s) of the author(s) follow(s) next
%
% NB: Chinese authors should write their first names(s) in front of
% their surnames. This ensures that the names appear correctly in
% the running heads and the author index.
%
\author{Alfred Hofmann \and Ursula Barth\and Ingrid Haas\and Frank Holzwarth}
%
\authorrunning{Alfred Hofmann et al.}
% (feature abused for this document to repeat the title also on left hand pages)

% the affiliations are given next; don't give your e-mail address
% unless you accept that it will be published
\institute{Springer-Verlag, Computer Science Editorial, Germany\\
\mailsa\\
\mailsb\\
\mailsc\\
}

%
% NB: a more complex sample for affiliations and the mapping to the
% corresponding authors can be found in the file "llncs.dem"
% (search for the string "\mainmatter" where a contribution starts).
% "llncs.dem" accompanies the document class "llncs.cls".
%


\toctitle{Normalization and } 
\tocauthor{Authors' Instructions}
\maketitle

\begin{abstract}
 The goal of our project is to develop a novel forensics software toolkit to help law  enforcement agencies identify new or previously unknown child abuse media and its  originators on peer-to-peer (P2P) networks, such as BitTorrent, eDonkey, or Kad. This paper  presents our approach to text categorization of filenames used in the toolkit. Our method is based on a statistical text classifier and a statistical text normalizer. We present the   experiments on the ``regular" porn data (popular web galleries and torrent trackers) used as a substitute of child pornography data. The results show that our approach is able to separate filenames of pornographic medias from the others with accuracy up to 98\%.
\keywords{text classification, text normalization, P2P networks, CSA}
\end{abstract}

\section{Introduction}

The goal of the XYZ project~\footnote{
%\url{http://scc-sentinel.lancs.ac.uk/XYZ/}
\url{http://anonymized.uri.com/}
} is to develop a novel forensics software toolkit to help law enforcement agencies across the EU identify new or previously unknown child abuse media and its originators on peer-to-peer (P2P) networks. Until now, the only way to identify such media is through manual analysis by law enforcement personnel. However, such a manual approach is difficult or impossible given the large number of files that need to be reviewed individually. The limited resources that law enforcement agencies possess make it impractical for them to examine the thousands of new files that may appear on P2P networks every day. 


The key output of the project -- the XYZ software toolkit -- will be used by law enforcement to help detect, filter, and prioritize new instances of child abuse media on P2P networks. XYZ system is operates alongside existing P2P monitoring tools like PeerPrecision. Using trace data from these monitors as input, XYZ identifies candidate suspect media that contain Child Sexual Abuse (CSA) based on a combination of advanced pattern recognition techniques. The language analysis tool detects potential CSA media in P2P networks based on a combination of several sources of evidence: the modelling of offender file sharing behaviour; the analysis of file sharing patterns and query patterns; language processing of file names and queries. Candidate media discovered by language analysis are fed to a content-based media analysis tool, which prioritizes and filters material further. 

Thus, the XYZ system performs automatic identification of new CSA content and its distributors, using evidence in the form of textual queries and filenames in P2P networks, of user's file sharing behaviour, and of the image and video content itself. It is of particular importance to search for new material as these previously unknown files are usually introduced on the network by people who one believed to be close to the victim or the abuse. These files have also a greater chance to be related to on-going situation of abuse. 

This paper describes language processing module of XYZ which recognizes the CSA media by their filenames. In the following section, we outline the architecture of the module. Next, we present classification results on the ``standard" pornography data used as a substitute for child pornography data.  

\section{Filename Classification}

Figure~\ref{fig:architecture} presents architecture of the language analysis module and outlines how it is integrated with the XYZ toolkit. The file classification module is used to recognize newly published files in a P2P network which contain abusing content. It serves as a fast coarse filter. The list of the candidate files is transferred to the slow media analysis component, where the content of each file is further analyzed~\cite{ulges2011automatic}. The goal of the file classification component is to decide if an input file contains some CSA material using solely the textual descriptions of the file (titles, tags, descriptions, etc.). This kind of classification is challenging for several reasons. First, titles may be meaningless, such as ``0012664321.mpg''. Second, text descriptions and metadata may be short or absent. Finally, text descriptions often use highly non-standard language (abridgments, abbreviations, spelling errors, technical terms, etc). If an input file is unknown, features are extracted from its text description (see Section~\ref{featureextraction}) which are used to classify the file (see Section~\ref{classification}). 
 
 
\begin{figure}
\centering
\includegraphics[height=5.2cm]{figures/architecture}
\caption{Architecture of the language analysis module of the XYZ forensics toolkit. }
\label{fig:architecture}
\end{figure}

\subsection{Feature Extraction: Normalization, Tokenization and Lemmatization}
\label{featureextraction}

To extract features from texts, we perform a text normalization and a standard linguistic analysis. 

Text normalization consists of rewriting text using a more conventional spelling in order to make it more readable for a human or for a machine. Hence, this module takes a potentially noisy text as input, and produces the corresponding cleaned text as output. Filenames and their text descriptions contain highly non-standard language patterns, such as abridgments, abbreviations, spelling errors, and so on. These language phenomena hamper standard text classifiers, which stumble against big number of out-of-vocabulary words. Therefore, the goal of the text normalization is to improve the feature extraction procedure. For instance, we would prefer to treat all variations of the word ``porn'' including its abbreviations and forms with spelling errors as a single feature. 

The text normalization is performed by an algorithm which learns rewriting rules from a parallel aligned corpora. This normalization stems from previous works on SMS normalization~\cite{beaufort2010b}. This method shares similarities with both spell checking and machine translation approaches. In our system, all lexicons, language models and sets of rules are compiled into finite-state machines and combined with the input filename by composition, a special operation defined on weighted transducers and on weighted automata. We use our own finite-state tools: a finite-state machine library and its associated compiler~\cite{beaufort2008a}. In conformance with the format of the library, the compiler builds finite-state machines from weighted rewrite rules, weighted regular expressions and n-gram models. The algorithm and its models are described in~\cite{beaufort2010b}. In order to learn a normalization model we apply a sequence alignment at the character-level obtained with algorithm described in~\cite{beaufort2009b}. This algorithm gradually learns the best way of aligning strings.

 Natural language analysis is then applied on cleaned texts using a morphosyntactic analysis and lemmatization. At this point we rely on the natural language processing system \textit{Alias}~\cite{beaufort2008a} which is also based on weighted transducers and automata.  

\subsection{Classification}
\label{classification}

Binary linear statistical classifiers are used to separate regular files from those containing pornographic content. We experimented with Support Vector Machines, Regularized Logistic Regression, Perceptron, Least Square Method, and Least Squares. We relied on the LibSVM~\cite{chang2011libsvm} and LibLINEAR~\cite{fan2008liblinear} classification software, as well as the baseline algorithms implemented in MATLAB. In our system, a filename is represented as a unit length vector of lemmas extracted from textual description of the file (title, tags, and description). All the standard stopwords (except the "sex-related" ones such as him, her, woman, man, etc.) are removed from the texts. We did not perform any feature selection -- all lemmas extracted from the training fold are used as features. 

Positive training examples are texts associated with pornographic resources, while negative training examples are texts associated with non-pornographic content. A training dataset is a set of tuples $<class, filename>$,  where  $filename$ is a text (title, tags and description of associated with a file) and $class$ is either "positive" or "negative". Following the standard machine learning methodology, cross-validation is used to assess performance of the classifiers. In order to avoid overfitting, vocabulary of the classifier is extracted from the training fold only. 

Experiments described in this paper are conducted on a dataset of ``regular'' pornographic files two reasons. First, the CSA data is a special case of pornographic content. Therefore, the ``standard'' porn is useful to develop the system, as well as for initial selection of parameter configurations. In these experiments the names of regular pornographic files serve as a substitute for child pornography filenames. Pornography can be expected to share important characteristics with CSA material (like general sex-related vocabulary, types of file extensions, text indicating technical parameters of the media file, etc.). Second, at the time of writing this article, the CSA data were not yet provided by our law enforcement partners due to various administrative and legal issues. We constructed ourselves two training datasets from the data openly available on the Internet: \textit{Gallery} and \textit{PirateBay}~\footnote{Available at \url{http://anonimized.uri.com/}}. 

The \textit{Gallery} dataset consists of titles of a web porn galleries and titles of Wikipedia articles. Positive examples of this dataset are crawled from the four porn sites: PicHunter\footnote{\url{http://www.pichunter.com/}}, PornoHub~\footnote{\url{http://www.porno-hub.com/}}, RedTube~\footnote{\url{http://www.redtube.com/}} and Xvideos~\footnote{\url{http://www.xvideos.com/}}. Each of 51.350 positive examples is composed of a title of a porn video/gallery and tags associated with it.  Negative training examples in this dataset are 55.000 randomly selected titles of the English Wikipedia, each composed of at least 4 words. Thus, the dataset is composed of 106.350 titles. Examples of two negative and two positive training examples from this dataset are provided in Figure~\ref{fig:data}.

The \textit{PirateBay} dataset consists of data available from ThePirateBay torrent tracker~\footnote{\url{http://thepiratebay.org/}}. Files in this tracker are organized by users in six main categories (``applications'', ``audio'', ``games'', ``other'', ``porn'', and ``video'') and 39 subcategories such as ``applications-android'' or ``audio-flac''. We crawled titles, tags and descriptions for 100.064 files from all the categories. So each sub-category is represented with around 3.000 files. From this material, we constructed a dataset with 16.863 positive training examples (porn category) and 83.201 negative training examples (all other categories). As the descriptions of the files are very noisy, we constructed two versions of this dataset. The first includes texts and tags associated with the torrents, while the second consists of texts, descriptions and tags.

%It is important to mention that since the text normalization component was not yet fully integrated in the language analysis module, we used a simplified text normalization procedure in the experiment described here. First, the titles were cleaned up from the numbers and the special symbols. Second, they were POS tagged and lemmatized with TreeTagger~\cite{schmid1994probabilistic}.

\begin{figure}
\centering
\includegraphics[height=5.2cm]{figures/data}
\caption{Gallery dataset: examples of two negative training examples (titles of Wikipedia articles) and two positive training examples (titles of Web porn galleries). }
\label{fig:data}
\end{figure}

%<text class='negative'>
%<original>Contractors and General Workers Trade Union</original>
%<lemmas>contractors#NNS#contractor and#CC#and general#JJ#general workers#NNS#worker trade#NN#trade union#NN#union</lemmas>
%</text>

%<text class='negative'>
%<original>1957-58 American Hockey League season</original>
%<lemmas>1957-58#CD#1957-58 american#JJ#American hockey#NN#hockey league#NN#league season#NN#season</lemmas>
%</text>

%<text class='positive'>
%<original>Husband catches his wife fucking with his brother .</original>
%<lemmas>husband#NN#husband catches#VVZ#catch his#PP$#his wife#NN#wife fucking#VVG#fuck with#IN#with his#PP$#his                brother#NN#brother .#SENT#.</lemmas>
%</text>

%<text class='positive'>
%<original>Slim Can Bearly Take The Dick .</original>
%<lemmas>slim#JJ#slim can#MD#can bearly#RB#bearly take#VV#take the#DT#the dick#NN#dick .#SENT#.</lemmas>
%</text>

\begin{table}
\centering
\begin{tabular}{|l|l|c|}
\hline
\bf Classifier & \bf Training Time & \bf Accuracy \\ \hline \hline  

C-SVM, linear kernel & 8m 59s & 96.97 \\
C-SVM, linear kernel (L2-reg., L2-loss, primal) & 0.459s & 96.52 \\
C-SVM, linear kernel (L2-reg., L1-loss, dual) & 0.366s & 96.47 \\
C-SVM, linear kernel (L1-reg., L2-loss) & 0.162s & 96.47 \\
C-SVM, linear kernel (L2-reg., L2-loss, dual) & 0.364s & 96.45 \\
Least Squares (sum of error squares) &	2m 9s &	 96.40  \\
Logistic Regression (L2-reg.) & 1.176s & 96.27 \\
Logistic Regression (L2-reg., L2-loss, primal) & 0.548s & 96.24 \\
Perceptron ($\epsilon$ $\leq 1\%$, 570 iterations) &	12m 30s & 94.03  \\
Logistic Regression (L1-reg.) & 0.388s & 93.95 \\
Least Mean Squares (LMS) $\rho= 10$ & 6.88s & 91.85  \\
nu-SVM, RBF kernel & 26m 49s & 88.35 \\
nu-SVM, linear kernel & 12m 48s & 88.20 \\
nu-SVM, sigmoid kernel & 14m 5s & 87.45 \\
nu-SVM, polynomial  kernel & 4m 39s & 79.77  \\
C-SVM, polynomial kernel & 15m 11s & 51.71 \\
C-SVM, RBF kernel & 22m 20s & 51.71 \\
C-SVM, sigmoid kernel & 14m 58s & 51.71 \\
\hline

\end{tabular}
\caption{ Performance of different binary classifiers with default meta-parameters trained and tested on the \textit{Gallery} dataset with help of 10-fold cross-validation. }
\label{tbl:results}
\end{table}

\section{Results}

Results of the experiments with the \textit{Gallery} dataset are presented in Table~\ref{tbl:results}. Our results show that a Support Vector Machine (SVM) or a Regularized Logistic Regression (LR) can distinguish a Wikipedia title from a pornographic video title with accuracy of 96-97\%. In particular, the best results were obtained with C-SVM with linear kernel (96.97\%). We tested also other kernels, but the linear kernel appeared to provide the best results. The training of a model with the linear kernel is also much faster. These results suggest that the titles of encyclopedia are linearly separable from the pornographic titles in the vector space of around 40.000 lemmas. Therefore the complex kernels are not required. Figure~\ref{fig:metaparams} depicts results of the metaparameter optimization of the C-SVM with linear kernel with the grid search. As we can see, this procedure improved the accuracy only by 0.37\%. Thus, variance of C-SVM with linear kernel in the context of our application is small. This is a useful property in the context of our application, because our system should be autonomously retrained by police on a database of the CSA data.  

Table~\ref{tbl:results2} reports on performance of the C-SVM with the linear kernel trained and applied to different datasets. As one can observe, this classifier is able to model well both versions of the \textit{PirateBay} dataset. The accuracy of classification achieves 97-98\%. Furthermore, the model does not seem to particularly overfitted to a certain dataset. Accuracy of the classifier trained on the \textit{Gallery} dataset and applied on the \textit{PirateBay} dataset and vise-versa is around 91\%.

Figure~\ref{fig:gallery2pirate} presents performance of the classifier trained on the \textit{Gallery} dataset and applied to the different parts of the \textit{PirateBay} dataset. The only category which is constantly misclassified is ``porn-games''. Textual descriptions of the files in this category are indeed difficult to classify as they are similar to those of video games and contain few porn-related keywords e.g.:
\begin{itemize}
\item \texttt{3D SexVilla Crack};
\item \texttt{ARTIFICIAL GIRL 2 MEGA PACKAGE rar};
\item \texttt{Egirl 1 5 Crack rar};
\item \texttt{Funky Teacher Nikusukes Pink School Excursion};
\item \texttt{Cracked ThriXXX Games 3D SexVilla 2 017 001 Incl AMD Patch};
\item \texttt{Mother Daughter Pristesses};
\item \texttt{Please Love Me Seriously};
\item \texttt{Ultimate Magic Girl};
\item \texttt{Custom Time After School Customize Lovers [3D]}.
 \end{itemize}

According to the results summarized in Figure~\ref{fig:gallery2pirate} and Table~\ref{tbl:results2}, training a model on the noisy descriptions of the \textit{PirateBay} seems to hamper accuracy of the classifier by around 3\%. From the other hand, using those descriptions at the classification time definitely helps. For instance, accuracy of the classifier trained on the \textit{Gallery} dataset and applied on the \textit{PirateBay Title+Desc+Tags} is around 6\% higher as compared to the \textit{PirateBay Title+Tags} dataset.    

Our results confirm the correctness of the chosen methodology for the filename classification. A C-SVM with linear kernel trained on an unigram model seems to be a suitable to filename categorization. Custom feature extraction module, which performs text normalization and morphological analysis helps to deal with noisy and scarce language of filenames.  

\begin{figure}
\footnotesize
\centering
\includegraphics[height=6.2cm]{figures/ucl-scale}
\caption{Optimization of the metaparameters of the classifier (C-SVM, linear kernel) trained on the \textit{Gallery} dataset with the grid search. }
\label{fig:metaparams}
\end{figure}

\begin{table}
\footnotesize
\centering
\begin{tabular}{|l|l|l|}

\hline
\bf Training Dataset & \bf Test Dataset & \bf Accuracy  \\ \hline \hline  

Gallery (train) & Gallery (valid) & 96.52 $\pm$ 0.09 \\
PirateBay Text+Desc+Tags (train) & PirateBay Title+Desc+Tags (valid) &  98.72 $\pm$ 0.11 \\
PirateBay Text+Tags (train) & PirateBay Title+Tags (valid) & 97.70 $\pm$ 0.13 \\
Gallery & PirateBay Text+Desc+Tags & 90.54 $\pm$ 0.09 \\
Gallery & PirateBay Text+Tags  & 84.26 $\pm$ 0.09 \\
PirateBay Text+Desc+Tags & Gallery  & 88.49 $\pm$ 0.09 \\
PirateBay Text+Tags & Gallery  & 91.32 $\pm$ 0.09 \\
\hline

\end{tabular}
\caption{ Performance of the binary classifier (C-SVM, linear kernel, L2-reg., L2-loss, primal) trained and applied on different datasets.}
\label{tbl:results2}

\end{table}

\begin{figure}
\centering
\includegraphics[height=5.2cm]{figures/gallery2pirate}
\caption{Performance of the C-SVM classifier with linear kernel (L2-reg., L2-loss, primal) trained on the \textit{Gallery} dataset and tested on the \textit{PirateBay} datasets. }
\label{fig:gallery2pirate}
\end{figure}

%\begin{figure}
%\centering
%\includegraphics[height=5.2cm]{figures/ucl-prediction-ratio}
%\caption{Prediction speed function on the test sample size. }
%\label{fig:speed}
%\end{figure}

\section{Conclusion}

In this paper, we described a text-based filename classifier, designed to identify child abuse media on peer-to-peer (P2P) networks. Our method relies on a statistical text classifier and a statistical text normalizer. We conducted extensive experiments on the standard porn data crawled from popular web galleries and torrent trackers. These data were used as a substitute of child pornography data. Our experiments have shown that the classifier based on the Support Vector Machine and the unigram model is able to distinguish filenames of pornographic media  from the filenames of other media with accuracy of 90-98\%, depending on the dataset.  

However, separating CSA from regular porn is much more challenging due to overlapping vocabularies and because of the lower number of available CSA filenames. Therefore, we expect that the accuracy of such classifier will be lower. The next stage of our project is training the system on the real CSA data provided by our police collaborators. When deployed, the system will be autonomously retrained on the new data. We expect that this will not hamper the performance, as our approach does not require meta-parameter tuning.  

%\subsubsection*{Acknowledgments.} The heading should be treated as a
%subsubsection heading and should not be assigned a number.

\bibliographystyle{splncs}
\bibliography{all}

\end{document}
