\beamer@endinputifotherversion {3.10pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Similarity Measures}{11}{0}{2}
\beamer@sectionintoc {3}{Evaluation}{24}{0}{3}
\beamer@sectionintoc {4}{Results}{28}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{32}{0}{5}
