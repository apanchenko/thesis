\beamer@endinputifotherversion {3.10pt}
\select@language {english}
\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Methodology}{11}{0}{2}
\beamer@subsectionintoc {2}{1}{Similarity-based Relation Extraction}{11}{0}{2}
\beamer@subsectionintoc {2}{2}{Single Similarity Measures}{14}{0}{2}
\beamer@subsectionintoc {2}{3}{Hybrid Similarity Measures}{23}{0}{2}
\beamer@sectionintoc {3}{Evaluation}{31}{0}{3}
\beamer@sectionintoc {4}{Results}{35}{0}{4}
\beamer@sectionintoc {5}{Conclusion}{39}{0}{5}
