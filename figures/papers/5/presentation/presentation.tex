\documentclass{beamer}
\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amssymb,amsfonts,amsmath,mathtext}
\usepackage{cite,enumerate,float,indentfirst}
\usepackage[dvips]{graphicx}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e} % NEW
\DeclareMathOperator*{\argmax}{arg\,max}
\setbeamertemplate{footline}[page number]

\title[\insertframenumber/\inserttotalframenumber]
{A Study of Heterogeneous Similarity Measures for Semantic Relation Extraction }

\author[Alexander Panchenko ]
{\textbf{Alexander Panchenko}, \\ Center for Natural Language Processing (CENTAL)\\ Université catholique de Louvain -- Belgium \\ { \url{alexander.panchenko@uclouvain.be} }}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>
    \frametitle{Plan}
    \tableofcontents[currentsection,currentsubsection]
  \end{frame}
}

\mode<presentation>
{
%\usetheme{Warsaw}
\usetheme{Singapore}
\usecolortheme{orchid}%whale
\useoutertheme{smoothbars}
%\usefonttheme{serif}
}

\setbeamertemplate{navigation symbols}{%
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \setcounter{tocdepth}{1}
  \frametitle{Plan}
  \tableofcontents
  \setcounter{tocdepth}{2}
	
\end{frame}

\section{Introduction}
\subsection{}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relations}


\begin{itemize}
\item \textbf{Semantic relations} are
useful for NLP/IR applications:
\begin{itemize}
  \item Query expansion (Hsu et al., 2006)
  \item QA systems (Sun et al., 2005)	
  \item Text categorization (Tikk et al, 2003)
  \item Word Sense Disambiguation (Patwardhan et al., 2003)
  \item Short Text Similarity (SemEval 2012)
\end{itemize}

\pause

%\item \textbf{Semantic resources:} thesauri, ontologies, synonymy dictionaries, WordNets, \ldots

\item \textbf{Focus of this work:} 
\begin{itemize}
\item \alert{synonyms}: $\langle car,SYN,vehicle \rangle$ 
\item \alert{hypernyms} : $\langle car,HYPER,Jeep \text{ } Cherokee \rangle$
\item \alert{co-hyponyms}:
$\langle Land  \text{ }Cruiser,COHYPER,Jeep \text{ } Cherokee \rangle$
\end{itemize}
\end{itemize}	

\begin{figure}	
	\centering
		\includegraphics[width=1.0\textwidth]{figures/thesaurus2}
		\caption{Semantic relations in the EuroVoc thesaurus.}
\end{figure}

\end{frame}


\begin{frame}
\frametitle{Semantic Relation Extraction: Task}



%\pause
\begin{itemize}
  
\item \textbf{Manual construction of relations}:
\begin{itemize}
\item $R$ -- relations between terms $C$ \alert{established manually}
\item (+) High precision 
\item (--) Very expensive and time-consuming
\end{itemize}

\pause 

\item \textbf{Relation extraction:}

 \begin{itemize}
  \item \alert{Input:} terms $C$
 \item \alert{Output:} semantic relations $\hat{R} \subset C \times C$
 \item \alert{Criterion:} discovering relations $\hat{R} \subseteq C \times C$ which would be as close to $R$ as possible \textbf{in terms of precision and recall}:
$$
\hat{R}^* = \argmax_{\hat{R}} \frac{Precision(R,\hat{R}) \cdot Recall(R,\hat{R})}{Precision(R,\hat{R}) + Recall(R,\hat{R})},
$$   

 $$Precision(R,\hat{R}) = \frac{|R \cap \hat{R}|}{|\hat{R}|}, Recall(R,\hat{R}) = \frac{|R \cap \hat{R}|}{|R|}.$$
\end{itemize}

\end{itemize}


\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Extraction: The State of Art}
\begin{itemize}
  
  \item \textbf{Existing relation extraction methods}:
\begin{itemize}
  \item quality(manually constructed) > quality(extracted)
     \item $\Longrightarrow$ development of \alert{new relation extraction
methods}  
  \end{itemize}
  
  \pause
 
\item \textbf{Information sources:}
\begin{enumerate}
  
 \item \alert{semantic networks} (Resnik, 1995)
 \item \alert{Web corpus} (Cilibrasi et Vitanyi, 2007)
 \item \alert{traditional corpora} (Lin, 1998b)
 \item \alert{definitions} of \alert{dictionaries} or \alert{encyclopedia} (Zesch et al., 2008)
\end{enumerate}

\pause

\item  \textbf{Measure combination:}

\begin{itemize}
\item Most measures use \alert{one} source of information.
\item Prior research (Sahlgren, 2006; Heylen et al., 2008; Panchenko, 2011) suggests that the sources \alert{are complimentary}.

\item Some \alert{combined measures}: (Curran, 2002; Cederberg et al., 2003; Mihalcea et al., 2006; Agirre et al., 2009; Yang, 2009).


\item These studies are \alert{not benefit of}
all 4 information sources.

\end{itemize}
\end{itemize}
%\textbf{Research Question}: How to combine the baseline similarity measures to improve relation extraction? 

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Contributions}

\begin{itemize}
\item \textbf{A comparative study} of the baseline similarity measures 
\begin{itemize}
  \item \alert{34 similarity measures} 
  \item based on \alert{4 information sources}
\end{itemize}

\item \textbf{Two combined measures} 
\begin{enumerate}
  \item based on semantic networks, Web, corpora, and definitions 
  \item \alert{outperforming all baseline measures} by a wide margin
  \item correlation with human judgements up to 0.887
  \item Precision(20) up to 0.979 for the closed-vocabulary extraction
\end{enumerate}


\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Similarity Measures}
\subsection{}

\begin{frame}
\frametitle{Outline}
\begin{itemize}
\item 34 \textbf{Baseline Measures}:
	\begin{itemize}
	\item 6 \alert{knowledge-based} measures 
	\item 9 \alert{web-based} measures
	\item 13 \alert{corpus-based} measures
	\item 6 \alert{definition-based} measures 
\end{itemize}
\item 6 \textbf{Combined Measures}:
	\begin{itemize}
	\item 2 \alert{combination methods}
	\item 3 \alert{measure sets} 
	\end{itemize}
\end{itemize}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Knowledge-based Measures}

\begin{enumerate}
 \item Wu and Palmer (1994)
 \item Leacock and Chodorow (1998)
 \item Resnik (1995)
 \item Jiang and Conrath (1997)
 \item Lin (1998)
 \item Inverted Edge Count (Jurafsky et Martin, 2009, p. 687)
\end{enumerate}



 \textbf{Data:} WordNet 3.0 +  SemCor corpus

 \textbf{Variables:}
 \begin{itemize}
  \item \alert{Lengths of shortest paths} between terms $c_i$, $c_j$ in the network:
  \begin{itemize}
    \item $len(c_i,c_j)$
    \item $len(c_i, lowest\_common\_subsumer(c_i, c_j))$
    \item $len(c_{root}, lowest\_common\_subsumer(c_i, c_j))$
    \end{itemize}
 \item \alert{Probability of terms} derived from a corpus:
 \begin{itemize}
 \item $P(c)$
 \item $P(lowest\_common\_subsumer(c_i, c_j))$
 \end{itemize}
 \end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Knowledge-based Measures (cont.)}


 \textbf{Coverage:} 155.287 English terms encoded in WordNet 3.0.
 \begin{itemize}
   \item \textit{TALN} is not in WordNet $\Rightarrow$ \alert{no similarities can be calculated}
   \item \alert{Required:} \textit{TALN $\rightarrow$ \{ RECITAL, TAL, NLP, \ldots\}}
   
 \end{itemize}
 \textbf{Complexity:} calculation of a shortest paths between the nodes.

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Web-based Measures}
	
\textbf{Normalized Google Distance (NGD)} (Cilibrasi and Vitanyi, 2007)
\begin{enumerate}
   \item NGD-Bing
   \item NGD-Yahoo! 
   \item NGD-YahooBoss
   \item NGD-Google 
   \item NGD-Google over \url{wikipedia.org} 
\end{enumerate}


\textbf{Pointwise Mutual Information (PMI)} (Turney, 2001)

\begin{enumerate}
   \setcounter{enumi}{5}
\item PMI-Bing
\item PMI-YahooBoss
\item PMI-Google
   \item PMI-Google over \url{wikipedia.org} 
\end{enumerate}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Web-based Measures (cont.)}

\textbf{Data:} number of times the terms co-occur in the documents
as indexed by an IR system.

\textbf{Variables:} 

\begin{itemize}
	\item \alert{number of hits} returned by query $"c_i"$ 
	\item \alert{number of hits} returned by query $"c_i \text{ AND } c_j''$
\end{itemize}

\textbf{Coverage:} huge vocabulary in dozens of languages.

\textbf{Complexity:} constraints of a search engine API:
\begin{itemize}
  \item Google -- 100 queries/day for free or 1000/5\$
  \item Yahoo -- 1000 queries/0.80\$
  \item Bing -- $\approx$ 1000 queries/2\$ 
  \item Number of queries $\approx 0.5*|C|^2$
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Corpus-based Measures}

\textbf{Bag-of-word Distributional Analysis (BDA)} (Sahlgren, 2006) 
\begin{enumerate}
\item BDA-sent-10.000
\item BDA-1-5.000
\item BDA-2-5.000
\item BDA-3-5.000
\item BDA-5-5.000
\item BDA-8-5.000
\item BDA-10-5.000
\end{enumerate}	

\textbf{Syntactic Distributional Analysis (SDA)} (Curran, 2003) 

\begin{enumerate}
\setcounter{enumi}{7}
\item SDA-6-100.000
\item SDA-9-100.000
\item SDA-21-100.000
\end{enumerate}

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Corpus-based Measures (cont.)}

\textbf{Data:} A corpus of \alert{Wikipedia abstracts} (890M tokens) 

\textbf{Variables:} 
\begin{itemize}
  \item feature vector based on the \alert{context window}
  \item feature vector based on the \alert{syntactic context} 

\end{itemize}

\begin{enumerate}
   \setcounter{enumi}{11}
	\item \textbf{Latent Semantic Analysis (LSA)} on TASA corpus (Landauer and Dumais, 1997)
	\item \textbf{NGD} on Factiva corpus (Veksler et al., 2008)
\end{enumerate}
	
\end{frame}


\begin{frame}
\frametitle{Corpus-based Measures (cont.) }

\textbf{Coverage:} a word should occur in the corpora
\begin{itemize}
  \item \textit{TALN} \alert{occur in the corpus} $\Rightarrow$ similarity can be calculated
\end{itemize}

\textbf{Complexity:} 

\begin{itemize}
  \item O(SDA) >> O(BDA) >> O(NGD) $\approx$ O(PMI)
  \item NGD, PMI -- only index of a corpus is needed
  \item BDA, SDA -- pairwise similarity computation
  \item SDA -- dependency parsing
  \item LSA -- SVD of a term-document matrix
\end{itemize}
	
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Definition-based Measures}

Measures based on  \textbf{WordNet glosses}:
\begin{enumerate}
  \item Extended Lesk  (Banerjee and Pedersen, 2003)
  \item GlossVectors (Patwardhan and Pedersen, 2006)
\end{enumerate}

\textbf{Data:} WordNet glosses.
	
\textbf{Variables:}
\begin{itemize}
		\item \alert{bag-of-words vector} of a term $c_i$ derived from the glosses
		\item \alert{relations} between words $(c_i,c_j)$ in the network 
\end{itemize}

\textbf{Coverage:} 117.659 glosses encoded in WordNet 3.0
\begin{itemize}
  \item \textit{TALN} \alert{is not present} in WordNet
\end{itemize}

\textbf{Complexity:} Calculation of a similarity in a vector space.

\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Definition-based Measures (cont.)}

\textbf{WktWiki} -- stems from  (Zesch, 2008)
\begin{enumerate}
 \setcounter{enumi}{2} 
\item Def-Wkt-1.000
\item Def-Wkt-2.500
\item Def-WktWiki-1.000
\item Def-WktWiki-2.500
\end{enumerate}

\textbf{Data}: Wikipedia abstracts, definitions of Wiktionary.

\textbf{Method:}
\begin{itemize}
  \item \alert{Definition} = abstract of Wikipedia with title $"c_i"$ + 
   glosses, examples, quotations, related words, categories from Wiktionary for $c_i$
   \item Represent a definition as a \alert{bag-of-words vector}
   \item Calculate similarities with \alert{cosine}
   \item \alert{Update similarities} according to relations in the Wiktionary.
  
\end{itemize}
   \textbf{Coverage:} Wiktionary: 550K glosses, Wikipedia: 3.8M articles
	\begin{itemize}
  	\item \textit{TALN} \alert{is present} in Wikipedia
	\end{itemize}   
   \textbf{Complexity:} Cosine calculation in a bag-of-words space. 
  
\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Combined Similarity Measures}
\begin{itemize}
\item \textbf{Goal:} to produce ``better'' similarities w.r.t. single measures

\item \textbf{Input:} $\{\mathbf{S}_1,\ldots,\mathbf{S}_K\}$ produced by $K$ single measures 
\item \textbf{Output:} $\mathbf{S}_{cmb}$.
\end{itemize}


\begin{enumerate}
\item \textbf{Similarity Fusion:} a mean of $K$ pairwise similarity scores

$$\mathbf{S}_{cmb}= \frac{1}{K} \sum_{k=1,K} \mathbf{S}_k$$

\end{enumerate}
\end{frame}



\begin{frame}
\frametitle{Combined Similarity Measures (cont.)}

\begin{enumerate}
  \setcounter{enumi}{1}
  \item \textbf{RelationFusion}. Unions the best relations found by each measure separately. A relation  extracted independently by several method has more weight. 

\begin{algorithm}[H]
\SetLine
\KwIn{Similarity matrices of $N$ single measures $\{\mathbf{S}_1,\ldots,\mathbf{S}_N\}$, number of nearest neighbors $K$}
\KwOut{ Combined similarity matrix $\mathbf{S}_{cmb}$  }

\For{i=1,N}{
	$R_i = knn(\mathbf{S}_i, k)$ \;
 	$\mathbf{R}_i = relation\_matrix(R_i)$
 }
$\mathbf{S}_{cmb} = \frac{1}{N} \sum_{i=1}^N \mathbf{R}_i$ \;
\Return $\mathbf{S}_{cmb}$ \;
\label{rfusion}
\end{algorithm}
$$
r^k_{ij} \in \mathbf{R}_k, r_{ij}^k = \left\{ 
  \begin{array}{l l}
    1 & \quad \text{if relation } \langle c_i, c_j \rangle \in R_k \\
    0 & \quad \text{else}\\
  \end{array} \right.
$$

\end{enumerate}

\end{frame}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Combination Sets}

\begin{block}{A problem}
 Number of ways to choose which of 34 single measures to combine:

$$\sum_{m=2}^{34}C_{34}^m=\sum_{m=2}^{34}\frac{34!}{m!(34-m)!}=1.72 \cdot 10^{10}$$
 
\end{block}


\textbf{A preliminary solution} -- an expert choice 

\begin{itemize}
\item \alert{4} = \{WN-Resnik, BDA-3-5000, SDA-21-100000, Def-WktWiki-1000 \}
\item \alert{8} = 4 + \{WN-WuPalmer, LSA-Tasa, Def-GlossVec., and Def-Ext.Lesk\}
\item \alert{14} = 8 + \{WN-LeacockChodorow, WN-Lin, WN-JiangConrath, NGD-Factiva, NGD-Yahoo, and NGD-
GoogleWiki\}

 
\end{itemize}
\end{frame}
  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Evaluation}
\subsection{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Human Judgement Datasets}

\begin{table}[h]\footnotesize
\begin{tabular}{ |c|c|c|c|c|c| }
\hline
  term, $c_i$ & term, $c_j$ & judgement, $\mathbf{s}$  & sim, $\mathbf{s}$  & judgement, $\mathbf{r}$ & sim, $\hat{\mathbf{r}}$  \\ \hline \hline
tiger & cat & 7.35 & 0.85 & 1 & 3 \\
book & paper & 7.46 &  0.95 & 2 & 2 \\
computer & keyboard & 7.62 &  0.81 & 3 & 1 \\
... & ... & ... & ...   & \ldots & \ldots \\
possibility & girl & 1.94 & 0.25 & 64 & 65 \\
sugar & approach & 0.88 & 0.05 & 65 & 23 \\ \hline
\end{tabular}
\end{table}


\textbf{Data:}

\begin{itemize}
	\item WordSim353 -- 353 term pairs (Finkelstein, 2002)  
	\item MC -- 30 term pairs  (Miller & Charles, 1991)
	\item RG -- 65 term pairs (Rubenstein & Goodenough, 1965)  
\end{itemize}

\textbf{Criteria:}
\begin{itemize}
\item Pearson correlation:  $\rho = \frac{cov(\mathbf{s},\hat{\mathbf{s}})}{\sigma(\mathbf{s}) \sigma(\hat{\mathbf{s}})}$

 \item Spearman's correlation: $r = \frac{cov(\mathbf{r},\hat{\mathbf{r}})}{\sigma(\mathbf{r}) \sigma(\hat{\mathbf{r}})}$
 
 \end{itemize}
 
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Datasets: Data}

{ \scriptsize

\begin{table}[h]\footnotesize
\begin{tabular}{ |c|l|l| }
\hline
term, $c_i$ & term, $c_j$ & relation type, $t$  \\ \hline \hline
judge & adjudicate & syn \\
judge & arbitrate & syn \\
%judge & asessor & syn \\
judge & chancellor & syn \\
%judge & gendarmerie & syn \\
judge & sheriff & syn \\
... & ... & ...   \\
judge & pc & random \\ 
judge & fare & random \\
judge & lemon & random \\ \hline
\end{tabular}
\end {table}

}

\begin{itemize}
  \item \textbf{BLESS} (Baroni and Lenci, 2011)
  \begin{itemize}
    \item 26554 relations
    \item hyperonyms, co-hypernyms, meronyms, associations, attributes, random relations
    \end{itemize}  
  \item \textbf{SN} (Semantic Neighbors)
  \begin{itemize}
    \item 14682  relations\
    \item synonyms, random relations
    \end{itemize}
    
    \item $|R_{random}|/|R_{rest}| \approx 0.5$
     
\end{itemize}

\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%\begin{frame}
%\frametitle{Similarity-based Relation Extraction}

%\begin{itemize}
%\item $sim_k$ -- a similarity measure $sim_k(c_i,c_j) \in [0;1], c_i,c_j \in C$
%\item $\mathbf{S}_i$ -- term-term similarity matrix ($C \times C$) 

%\item $knn$ -- $k$-NN thresholding:
%$\hat{R}=\bigcup_{i=1}^{|C|}\left\{\left\langle c_i, c_j \right\rangle :  (c_j
%\in \text{ top }k\% \text{ of } c_i) \wedge (s_{ij} > 0) \right\}.$  
%\item $\mathbf{S}_{cmb}$ -- combined similarity matrix obtained with $ combination\_method(\mathbf{S}_1, \ldots,\mathbf{S}_N) $
%\end{itemize}

%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Datasets: Criteria}

\begin{itemize}
  
  \item Based on the number of correctly (extracted) ranked relations.
  

\item $R$ -- all not random relations 

\item $\hat{R}(k)$ -- extracted relations for $k\%$ nearest neighbors   

\begin{block}{Criteria}

	
	\begin{itemize}
		\item Precision: $P(k)=$$\frac{|R \cap \hat{R}(k)|}{|\hat{R}(k)|}$,
		\item Recall: $R(k)=$$\frac{|R \cap \hat{R}(k)|}{|R|}$,
		\item F1-measure: $F(k)= 2 \cdot \frac{P(k) \cdot R(k)}{P(k) + R(k)}$,
		\item MAP $M(k) = \frac{1}{k}\sum^{k}_{i=1}P(i)$.
	\end{itemize}	
	\end{block}

\item We use $P(10)$, $P(20)$, $P(50)$, $R(50),M(20)$, $M(50)$.
	

\end{itemize}
	
	
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Semantic Relation Datasets: Example}

\begin{itemize}
	\item Precision $P(50\%)= \frac{1}{7} \approx 0.86 $
\end{itemize}


\begin{table}[h]\footnotesize
\begin{tabular}{ |l|l|l|l| }
\hline
 term, $c_i$ &  term, $c_j$ & relation type & \bf $s_{ij}$ \\ \hline \hline

aficionado & enthusiast & syn & 0.07197 \\
aficionado & fan & syn & 0.05195 \\
aficionado & admirer & syn & 0.01964 \\
aficionado & addict & syn & 0.01326 \\
aficionado & devotee & syn & 0.01163 \\
aficionado & foundling & random & 0.00777 \\
aficionado & fanatic & syn & 0.00414 \\ \hline
aficionado & adherent & syn & 0.00353 \\
aficionado & capital & random & 0.00232 \\
aficionado & statute & random & 0.00029 \\
aficionado & blot & random & 0.00025 \\
aficionado & meddler & random & 0.00005 \\
aficionado & enlargement & random &	0.00003 \\
aficionado & bawdyhouse & random & 	0.00000 \\ 
\hline
\end{tabular}
\end {table}

\end{frame}

\section{Results}
\subsection{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}
\frametitle{Human Judgement Datasets}
	\begin{figure}
	\centering
		\includegraphics[width=1.0\textheight]{figures/correlations}
		
\end{figure}
	
\end{frame}

\begin{frame}
\frametitle{Semantic Relations Datasets}
	\begin{figure}
	\centering
		\includegraphics[width=0.85\textheight]{figures/relations}
		
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{}

	\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/box2}
		
		\caption{ Distribution of 1-NN similarity scores of the four best single measures on the BLESS dataset. Here ``random'' and ``relation'' are distributions of scores
	between random and meaningful relations.  }
\end{figure}
	
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
 \frametitle{Hybrid Similarity Measures}
	\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/pr}
		\caption{Precision-Recall graphs of  (a) the best single and combined
	measures; (b) four combined measures.}
\end{figure}
	
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion}
\subsection{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}
\frametitle{Conclusion:}

\begin{itemize}
  
\item \textbf{Compared} 34 knowledge-, corpus-, web-, and definition-based semantic similarity measures 
\begin{itemize}
  \item on the task of predicting semantic similarity scores;
  \item on the task of semantic relation extraction
\end{itemize}  

\item \textbf{Proposed} 6 combined measures:

\begin{itemize}
  \item use corpus, Web, dictionaries, and semantic networks
  \item outperform the single measures on all datasets
  \item a correlation of 0.887 (RG)
  \item Precision(20) of 0.979 on the BLESS dataset
\end{itemize}  
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{}

\Huge \bf Thank you! Questions?
\end{frame}
\end{document}