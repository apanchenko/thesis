#!/bin/bash
# Opens all TEX documents in the directory
find . -name "*.tex" -exec s '{}' \;
find . -name "*.bib" -exec s '{}' \;
