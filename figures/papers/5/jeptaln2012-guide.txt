Comment utiliser le style Latex pour JEP-TALN2012

NOTE: pour les auteurs des JEP et de TALN, l'année 2012 est une année expérimentale 
où les styles ont été optimisés pour de la lecture à l'écran (qui sera l'usage premier
des articles publiés). Nous sommes conscients des changements que cela implique dans 
vos habitudes, mais espérons que celles-ci changeront au bénéfice d'un usage sur écran
et au détriment des impressions sur papier.

Vous aurez besoin des fichiers suivants:

    jeptaln2012.sty     le style de la conférence

Ce style importe et donc requiert la présence des styles suivants:
    apalike-fr.bst	le style utilisé par BibTex, disponible sur CTAN et dans les installations texlive
    geometry.sty        package permettant de spécifier la taille du papier et des marges de manière simple
    microtype.sty       package permettant une meilleure lecture des caractères à l'écran 
    mathdesign.sty      package permeant de spécifier la fonte charter avec intégration des styles mathématiques
    natbib.sty          package pour les citations en style "naturel" (auteur et année) dans le texte
    url.sty             package pour le formattage des URL

Nous vous fournissons un exemple comprenant les fichiers suivants:

    jeptaln-exemple.tex	contenu de l'article
    biblio.bib		un exemple de bibliographie
    jeptaln-exemple.pdf	le document résultant

Comment utiliser BibTex:
    http://www.tug.org/pracjourn/2006-4/fenn/fenn.pdf

Procédure:
    pdflatex jeptaln-exemple
    bibtex jeptaln-exemple
    pdflatex jeptaln-exemple
    pdflatex jeptaln-exemple

----------------------------------------------------------------------

How to use the Latex style for JEP-TALN2012 

NOTE: for the authors of JEP and TALN 2012, please be aware that this year, we set up an experimental 
stylesheet tat is optimized for on screen reading (which will be the first use of the articles). We are
aware of the modification this implies to your usages, but we hope that usage will evolve for onscreen
reading instead of on paper printing.

You will need the following files:
    jeptaln2012.sty     le style de la conférence

This style requires the following additional styles to be present in you install
    apalike-fr.bst	style used for citation. For english paper, you may use apalike.
    geometry.sty        package that allows easy setting of paper size and margins
    microtype.sty       package that provides better character arrangement 
    mathdesign.sty      package that allows the use of charter font, with an correct math font integration
    natbib.sty          package used for natural (author, year) in text citations
    url.sty             package for better url formatting

An example is provided:
    jeptaln-exemple.tex	an example paper
    biblio.bib		an example bibliography
    jeptaln-exemple.pdf	result

How to use BibTex:
    http://www.tug.org/pracjourn/2006-4/fenn/fenn.pdf

Procedure:
    pdflatex jeptaln-exemple
    bibtex jeptaln-exemple
    pdflatex jeptaln-exemple
    pdflatex jeptaln-exemple

GS