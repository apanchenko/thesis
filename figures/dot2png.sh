if ["$1" == ""]; then
	echo "Creates png from dot file"
	echo "Usage: dot2png.sh <input-dot-file>"
else 
	file_dot=$1
	file=${1%%.*}
	echo "Processing " $file_dot
	
	dot -Tpng $file_dot -o "$file.png"
	echo "Done!"
	gnome-open "$file.png"
fi
