\documentclass{beamer}
%\include{handout4pp} % for handout 
%\setbeameroption{show notes} % uncomment to the the notes

\usepackage[T2A]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amssymb,amsfonts,amsmath,mathtext}
\usepackage{cite,enumerate,float,indentfirst}
\usepackage[dvips]{graphicx}
\usepackage[linesnumbered,ruled,vlined]{algorithm2e} % NEW
\usepackage{algorithmic}
\usepackage{listings}

\lstset{
        breaklines=true,
        basicstyle=\footnotesize\ttfamily,
        prebreak =\raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
        postbreak =\raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}}
}


\setbeamertemplate{footline}[page number]

\title[\insertframenumber/\inserttotalframenumber]
{\textbf{Similarity Measures  for \\ Semantic Relation Extraction}}

\author[Alexander Panchenko]
{\textbf{Alexander Panchenko} \\ Université catholique de Louvain  \& \\ Bauman Moscow State Technical University   \\ { \url{alexander.panchenko@uclouvain.be}  }}



\mode<presentation>
{
%\usetheme{Warsaw} % default beamer
%\usetheme{Singapore} % plain white
\usetheme{Luebeck}
\usecolortheme{default}
%\usecolortheme{orchid}% plain white
\useoutertheme{smoothbars}
%\usefonttheme{serif}
}


\setbeamertemplate{navigation symbols}{%
}

\AtBeginSubsection[]
{
	\begin{frame}<beamer>
  	\frametitle{Plan}
  	\tableofcontents[currentsection,currentsubsection,currentsubsubsection]
  	\end{frame}
}


\AtBeginSection[]
{
	\begin{frame}<beamer>
	\frametitle{Plan}
	\tableofcontents[currentsection]
	\end{frame}
}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}
  \setcounter{tocdepth}{1}
  \frametitle{Plan}
  \tableofcontents
  \setcounter{tocdepth}{2}
	
\end{frame}

\section[The Problem]{The Context and the Problem}
\subsection{ }




\begin{frame}
\frametitle{Introduction}

\begin{block}{Motivation}

\begin{enumerate}
\item Synonyms, hypernyms and co-hyponyms \alert{are useful} for:
	\begin{itemize}
	\item text similarity (Šaric et al., 2012);
	\item query expansion (Hsu et al., 2006);
	\item question answering (Sun et al., 2005);
	%\item word sense disambiguation (Patwardhan et al., 2003), etc. 
	\end{itemize}

\item Manual resource construction is prohibitively \alert{expensive}.
\item Extractors \alert{do not meet} quality of the handcrafted resources.
\end{enumerate}
\end{block}

\begin{block}{Focus}
\alert{Similarity-based} semantic relation extraction.
\end{block}

\begin{block}{Research Question}
How to \alert{improve} precision and coverage of such measures?
\end{block}

\note[item]{\ldots}
\note[item]{\ldots}


\end{frame}


\begin{frame}
\frametitle{Object of the Research -- Semantic Relations and Resources}

\begin{block}{Untyped Semantic Relations}
\begin{itemize}
\item A set of relations $R$ is a \textbf{binary relation} $R \subset C \times C$, obtained from a set of typed relations $C \times T \times C$, where $T= \{ \textit{syn, hyper, cohypo} \}$.
 
\item
\begin{itemize}
\item $\langle \textit{car, vechicle} \rangle$ -- $syn$;
\item $\langle \textit{physics, mathematics} \rangle$ -- $cohypo$.
\end{itemize}
\end{itemize}
\end{block}

\begin{table}
\footnotesize
\centering
\begin{tabular}{|l|lll|}
\hline
\bf Type, $t$ & \bf Reflexivity & \bf Symmetricity &  \bf Transitivity \\ \hline \hline
$syn$ &   yes &  yes &  no \\
$hypo/hyper$ &  \textbf{yes} & \textbf{yes} & \textbf{no} \\
$cohypo$ & yes & yes & \textbf{no} \\ \hline
\end{tabular}
\end{table}

\end{frame}



\begin{frame}
\frametitle{Object of the Research -- Semantic Resources}

\begin{block}{Definition}

A \textbf{semantic resource} is an undirected graph $(C, R)$:
\begin{itemize}
\item nodes $C$ represent \textbf{terms};
\item edges $R$ represent untyped \textbf{semantic relations}.
\end{itemize}

\end{block}

\begin{figure}
\centering
\includegraphics[height=0.400\textwidth]{./../figures/sr-29-example-crop2}
\end{figure}

\end{frame}


\begin{frame}

\frametitle{Subject of the Research -- Semantic Relation Extractors}

\begin{itemize}
\item We study the extractors based on two components:
\begin{enumerate}
\item \alert{semantic similarity measures};
\item nearest neighbors procedures.
\end{enumerate}

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{./../figures/ssr-extraction-2}
\end{figure}

\end{itemize}
\end{frame}




\begin{frame}
\frametitle{Subject of the Research -- Semantic Similarity Measures}

\begin{block}{Definition}
  A semantic similarity measure quantifies semantic relatedness input terms $c_i, c_j$ with the similarity score $s_{ij} = sim(c_i,c_j)$:
    $$
  s_{ij} = \left\{ 
   \begin{array}{l l}
     high & \quad \text{if } \langle c_i, c_j \rangle \text{ is a pair of } syn, hyper, cohypo \\
    0 & \quad \text{otherwise}\\
   \end{array} \right.
 $$
 \end{block}


\begin{block}{Properties}
\begin{itemize}    
  \item Nonnegativity: $0 \leq s_{ij} \leq 1$;
  \item Reflexivity: $s_{ij} = 1 \Leftrightarrow c_i = c_j$;
  \item Symmetry: $s_{ij} = s_{ji}$;
  \item Triangle inequality: $s_{ij} \leq s_{ik} + s_{kj}$  
  \end{itemize}
 \end{block}
     
\end{frame}




\begin{frame}
\frametitle{Subject of the Research -- Semantic Similarity Measures}
\begin{itemize}

\item Many dissimilar pairs, few similar pairs: $s_{ij} \sim exp(\lambda)$:

\begin{figure}
\centering
\includegraphics[width=0.5\textwidth]{./../figures/reldist-crop}
\end{figure}

\item Similarity distribution of the term ``doctor'' ($|C| > 200,000$): 

\begin{figure}
\centering
\includegraphics[width=0.6\textwidth]{./../figures/real-extracted}
\end{figure} 

\end{itemize}
\end{frame}




\begin{frame}
\frametitle{Evaluation of the Semantic Similarity Measures}

\begin{enumerate}
\item correlations with human judgments (\textbf{MC}, \textbf{RG}, \textbf{WordSim});
\item semantic relation ranking (\textbf{BLESS}, \textbf{SN});
\item semantic relation extraction;
\item using extracted relations in an application:
\begin{itemize}
\item a short text classification system (\textbf{iCOP});
\item a lexico-semantic search engine (\textbf{Serelex}).
\end{itemize}
\end{enumerate}

\end{frame}

   
   
\section[Single Measures]{Single Semantic Similarity Measures}

\subsection{SDA-MWE: a Measure based on Distributional Analysis}

\begin{frame}
\frametitle{SDA-MWE: Key Points}

\begin{itemize}
\item Is based on \textbf{Syntactic Distributional Analysis};
\item Can handle \textbf{MultiWord Expressions} (MWE):
\begin{figure}
\includegraphics[width=1.0\textwidth]{./../figures/10-figure-3-gray}
\end{figure}

\item XIP parser, PMI, Cosine;

\item Was applied to \textbf{automatic thesaurus construction}.  
  
\end{itemize}
\end{frame}



\begin{frame}
\frametitle{A huge difference between extracted and hand-crafted relations}

\begin{figure}
\centering
\includegraphics[width=1.05\textwidth]{./../figures/sda-mwe}
\end{figure}

\begin{itemize}
  \item 7\% of extracted relations are explicit in the thesaurus;
  \item 35\%-46\% are implicitly encoded in the resource.
\end{itemize}
\end{frame}



\subsection{DefVectors: a Measure based on Definitions}

\begin{frame}
\frametitle{DefVectors: Key Points}

\begin{itemize}
\item A term $c$ is modeled with a vector $\mathbf{f}$ of its definition $d$.

\item Definitions from Wikipedia and Wiktionary. 

\item Cosine similarity.

\item PMI normalization.

\item Account for the known relations $R$.


%\begin{itemize}
%\item IF $c$ is a title of a Wikipedia article, 
%\item THEN $d$ is the abstract of this article;
%\item IF $c$ is in the Wiktionary, 
%\item THEN $d$ include its glosses, examples, quotations, related words, and categories.
\end{itemize}
 
 
 
%\begin{figure}
%\includegraphics[width=0.75\textwidth]{./figures/defvectors}
%\end{figure}

%\end{itemize}

\end{frame}





%\begin{frame}
%\frametitle{Correlations with Human Judgements}

%\begin{itemize}
%  \item Performs \textbf{comparably} to the baselines:
%\end{itemize}

%\begin{table}
%\footnotesize
%\begin{center}
%\begin{tabular}{|l|l|l|l|}
%\hline

%\bf Sim.Measure &  \bf MC $(\rho)$ & \bf RG $(\rho)$ & \bf WordSim $(\rho)$ \\ \hline \hline

%Resnik & \bf 0.784 & 0.757 & \bf 0.330 \\ 
%LeackockChodorow & 0.724 &  \bf \alert{0.789} & 0.295 \\ 
%WuPalmer & 0.742 & 0.775 & 0.330 \\ 
%Lin & 0.754 & 0.619 & 0.203 \\ 
%JiangConrath & 0.719 & 0.587 & 0.175 \\ \hline 

%BDA-3-Cos & 0.692 & 0.782 & 0.465  \\
%SDA-21-Cos & \gc\bf 0.790 & 0.785 & \bf 0.490 \\
%NGD-Bing & 0.063 & 0.181 & 0.058 \\
%NGD-Yahoo & 0.330  & 0.445 & 0.254 \\
%NGD-Google & 0.019 & -0.012 & 0.150 \\ \hline

%GlossVectors & 0.653 & 0.738 & 0.322 \\
%ExtendedLesk & \bf \alert{0.792} & 0.717 & 0.409 \\ 
%DefVectors-WktWiki-1000 & 0.759 &  0.453  & \bf  \alert{0.545} \\
%DefVectors-WktWiki-2500 & 0.759 & 0.754 &  0.520  \\ \hline

%\end{tabular}
%\end{center}
%\end{table}

%\end{frame}



\begin{frame}
\frametitle{Semantic Relation Ranking}

\begin{itemize}
  \item Recall is:
  \begin{itemize}
    \item \textbf{comparable} w.r.t. other definition-based measures;
    \item \textbf{lower} w.r.t. other corpus-based and network-based measures.
    \end{itemize}
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=0.7\textwidth]{./../figures/pr-graph-thesis}
\caption{Precision-Recall graph (the BLESS dataset).}
\end{figure}

\end{frame}




\subsection{PatternSim: a Measure based on Lexico-Syntactic Patterns}

\begin{frame}
\frametitle{PatternSim: Key Points}

\begin{itemize}
\item 18 \textbf{patterns} extracting hypernyms, co-hyponyms, synonyms;

\item \textbf{corpus} Wikipedia+ukWaC: $2.9\cdot10^{12}$ tokens;

\item $3.4\cdot10^9$ \textbf{concordances}:
\begin{itemize}
\item \texttt{such diverse \{[occupations]\} as \{[doctors]\}, \{[engineers]\} and \{[scientists]\}},
\item \texttt{\{traditional[food]\}, such as \{[sandwich]\},\{[burger]\}, and \{[fry]\}}.
\end{itemize}

\item Reranking of the extracted relations.
\end{itemize}

%\begin{figure}	
%\centering
%\includegraphics[width=0.70\textwidth]{figures/algo}
%\end{figure}

\end{frame}




\begin{frame}
\frametitle{Reranking formula Efreq-Rnum-Cfreq-Pnum}

  
$$s_{ij} = \sqrt{p_{ij}} \cdot \frac{2\cdot\mu_b }{b_{i*}+b_{*j}} \cdot \frac{P(c_i,c_j)}{P(c_i)P(c_j)}.$$


\begin{itemize}

%\item $s_{ij}$ -- semantic similarity between terms $c_i, c_j \in C$

\item $P(c_i,c_j)=\frac{e_{ij}}{\sum_{ij}e_{ij}}$ -- extraction probability of the pair $\langle c_i,c_j \rangle$, $e_{ij}$ --  frequency of co-occurrence of $c_i$ and $c_j$ in concordances $K$ 

\item $P(c_i)= \frac{f_i}{\sum_i f_i}$ -- probability of the term $c_i$, $f_i$ -- frequency of $c_i$ 
\item $b_{i*} = \sum_{j:e_{ij} \geq \beta} 1$ -- the number of extractions for term $c_i$ with the frequency $\geq \beta$, $\mu_b = \frac{1}{|C|}\sum_{i=1}^{|C|} b_{i*}$ -- the average number of extractions per term

\item $p_{ij} \in [1;18]$ -- number of distinct patterns which extracted the relation $\langle c_i, c_j \rangle$
  
 
\end{itemize}

\end{frame}




%\begin{frame}
%\frametitle{Semantic Relation Ranking}
%\begin{figure}	
%\centering
%\includegraphics[width=1.0\textwidth]{figures/res-relations-mlg}
%\end{figure}
%\end{frame}




\begin{frame}
\frametitle{Semantic Relation Ranking}

\begin{itemize}
  \item Precision is \textbf{comparable or better} w.r.t. the baselines;
  \item Recall is \textbf{lower} w.r.t. the baselines.
\end{itemize}

\begin{figure}	
	\centering
	\includegraphics[width=0.65\textwidth]{figures/pr2-mlg}
	\caption{Precision-Recall graphs (the BLESS dataset).
	}
\end{figure}

\end{frame}





\begin{frame}
\frametitle{Semantic Relation Extraction}
  \begin{columns}[T]
    \begin{column}{.4\textwidth}
     
% Your text here
\includegraphics[width=0.85\textwidth]{./../figures/eval3}
    
    \end{column}
    \begin{column}{.6\textwidth}
    %\begin{block}{Your image}
\begin{itemize}
  %\item 49 words, three binary annotations;
  \item $Precision@1 \approx 0.80$;
  \item ``Good'' coverage:
  
\end{itemize}

\begin{figure}
\includegraphics[width=0.95\textwidth]{./../figures/wordnet-vs-serelex}
\end{figure}
      
    \end{column}
  \end{columns}
\end{frame}








\section[Comparison]{Comparison of Similarity Measures} 
\subsection{  }

\begin{frame}
\frametitle{Compared Semantic Similarity Measures}

\begin{figure}
\includegraphics[width=1.0\textwidth]{./../figures/measures-classification}
\end{figure}

\begin{itemize}
  \item 37 distinct measures;
  \item \alert{Q1}: Are the measures are complementary?
  \item \alert{Q2}: If yes, in which respects?
\end{itemize}
 
\end{frame}









\begin{frame}
\frametitle{The Best Single Measures (MC, RG, WordSim, BLESS, SN)}

\begin{figure}
\includegraphics[width=1.07\textwidth]{./../figures/best}

\end{figure}

\begin{itemize}
  \item Each one extracts many \alert{co-hyponyms}, e.g.: 
  \begin{itemize}
  \item $\langle Canon, Nikon \rangle$,
  \item $\langle Lamborghini, Ferrari \rangle$,
  \item $\langle Obama, Romney \rangle$.
\end{itemize}
\end{itemize}
   
\end{frame}



\begin{frame}
\frametitle{Further Results}

\begin{columns}[T]

\begin{column}{.6\textwidth}
  
\begin{block}{Most dissimilar measures}
\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{./../figures/papers/7/figures/clusters-gray-2}   
\caption{21 measures grouped according to their relation distributions. }
\end{figure}
\end{block}
\end{column}

\begin{column}{.4\textwidth}
\begin{block}{Measures are \alert{indeed complementary} w.r.t.:}
\begin{itemize}
  \item lexical coverage;
  \item performances;
  \item types of semantic relations they extract. 
\end{itemize}
\end{block}   
\end{column}

\end{columns}

\end{frame}





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section[Hybrid Measures]{Hybrid Semantic Similarity Measures}

\subsection{}

%### A multitude of \textbf{complimentary measures} were proposed to extract synonyms, hypernyms,
%and co-hyponyms

%### Most of them are based on \textbf{one} of the \textbf{5 key approaches}: 
%\begin{enumerate}
%\item distributional analysis (Lin, 1998b)
%\item web as a corpus (Cilibrasi and Vitanyi, 2007)
%\item lexico-syntactic patterns (Bollegala et al., 2007)
%\item semantic networks (Resnik, 1995)
%\item definitions of dictionaries or encyclopedias (Zesch et al., 2008a)
%\end{enumerate}

%\item Some attempts were made to \textbf{combine measures} (Curran, 2002; Cederberg and Widdows, 2003; Mihalcea et al., 2006;
%Agirre et al., 2009; Yang and Callan, 2009)

%### \item However, most studies are still \textbf{not taking into account} all 5 existing extraction approaches.


\begin{frame}
\frametitle{Hybrid vs Single Measures}

\begin{figure}
\centering
\includegraphics[width=0.65\textwidth]{./../figures/papers/4/src/figures/single-and-hybrid-2}
\caption{ Semantic relation extractor based on:
\begin{itemize}
\item \textbf{(a)} a \alert{single} similarity measure;
\item \textbf{(b)} a \alert{hybrid} similarity measure. 
\end{itemize}}
\end{figure}
\end{frame}





\begin{frame}
\frametitle{16 Features = 16 Single Similarity Measures}

	\begin{itemize}
	
	\item 5 \textbf{network-based} measures :
	\begin{enumerate}
	  \item WuPalmer;
	  \item Leacock and Chodorow;
	  \item Resnik;
	  \item Jiang and Conrath;
	  \item Lin.
	\end{enumerate} 
	\item 3 \textbf{web-based} measures (NGD-Yahoo/Bing/Google); 
		
	\item 5 \textbf{corpus-based} measures: 
	\begin{itemize}
	  \item 2 distributional (BDA, SDA)
	  \item 1 lexico-syntactic patterns (PatternSim)
	  \item 2 other co-occurence based (LSA, NGD-Factiva)
	\end{itemize}
	
	\item 3 \textbf{definition-based} measures
	\begin{enumerate}
	  \item ExtendedLesk;
	  \item GlossVectors;
	  \item DefVectors-WktWiki.
	\end{enumerate}
	 
\end{itemize}

\end{frame}








\begin{frame}
\frametitle{Unsupervised Combination Methods}

\begin{enumerate}
  
%a mean of $K$ pairwise similarity scores;
\item \textbf{Mean}: $s_{ij}^{cmb}= \frac{1}{K}\sum_{k=1,K} s_{ij}^k;$

%-- a mean of scores having non-zero value;
\item \textbf{Mean-Nnz}: $s_{ij}^{cmb}= \frac{1}{|k:s_{ij}^k >0,k=1,K|}\sum_{k=1,K} s_{ij}^k;$ 

%-- A mean of scores transformed into Z-scores;
\item \textbf{Mean-Zscore}: $\mathbf{S}_{cmb} = \frac{1}{K} \sum_{k=1}^K \frac{\mathbf{S}_k -
\mu_k}{\sigma_k};$

% where $\mu_k$ and $\sigma_k$ are a mean and a standard deviation of the scores of the $k$-th measure ($\mathbf{S}_k$).

%. A median of $K$ pairwise similarities:
\item \textbf{Median}: $s_{ij}^{cmb}= median(s_{ij}^1,\ldots,s_{ij}^K);$

% A maximum of $K$ pairwise similarities:
\item \textbf{Max}: $s_{ij}^{cmb}= max(s_{ij}^1,\ldots,s_{ij}^K);$

%A mean of scores converted to ranks:
\item \textbf{RankFusion}: $s_{ij}^{cmb}= \frac{1}{K}\sum_{k=1,K} r_{ij}^k;$

%where  $r^k_{ij}$ is the rank corresponding to the similarity score $s^k_{ij}$.
\item \textbf{RelationFusion} (Panchenko and Morozova, 2012).
\end{enumerate}

\end{frame}





\begin{frame}
\frametitle{Supervised Combination Methods}

\begin{enumerate}
  \setcounter{enumi}{7}
\item \textbf{Logit}, \textbf{Logit-L1}, \textbf{Logit-L2}. 

\begin{itemize}
  \item  A binary \textbf{logistic regression};

\item \textbf{Positive examples} -- synonyms,
hyponyms, co-hyponyms from BLESS/SN;
\item \textbf{Negative examples} -- random relations from BLESS/SN;

  \item A relation $\langle c_i,t, c_j \rangle \in R$ is represented with a \textbf{vector of pairwise similarities}: $\mathbf{x} = (s_{ij}^1,\ldots,s_{ij}^N), N=\overline{2,16}$; 

\item Category $y_{ij}$:
$$
y_{ij} = \left\{ 
  \begin{array}{l l}
    0 & \quad  \text{ if } \langle c_i,t, c_j \rangle \text{ is a random relation} 
    \\
    1 & \quad  \text{ otherwise }\\
  \end{array} \right
  .
$$

\item \textbf{Using the model} $(w_1,\ldots,w_K)$ for combination: 
$$s^{cmb}_{ij} = \frac{1}{1 + e^{-z}}, z = \sum_{k=1}^K w_k s^k_{ij} + w_0.$$

\end{itemize}
\end{enumerate}

\end{frame}



\begin{frame}
\frametitle{Supervised Combination Methods}

\begin{enumerate}
  \setcounter{enumi}{8}
\item \textbf{SVM}. 


\begin{columns}
  \begin{column}{0.5\textwidth}
\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{./../figures/svm}
%\caption{SVM: maximal margin hyperplane. }
\end{figure}
    
  \end{column}

  \begin{column}{0.5\textwidth}
\begin{itemize}
\item The weights $\mathbf{w}$ and the support vectors $SV$: 

$$
\mathbf{w} = \sum_{x_i \in SV} \alpha_i y_i \mathbf{x}_i.  
$$

\item \textbf{Using the model} 

$$
s_{ij}^{cmb} = \mathbf{w}^T\mathbf{x} + b = \sum_{k=1}^K w_i s_{ij}^k + b.
$$

\end{itemize}    
  \end{column}
\end{columns}





\end{enumerate}

\end{frame}









\begin{frame}
\frametitle{Hybrid Similarity Measures}
\begin{figure}
\centering
\includegraphics[width=1.0\textwidth]{figures/pr}
%\caption{

%}
\end{figure}

Precision-Recall graphs calculated on the BLESS dataset:
\begin{itemize}
  \item \textbf{(a)} 16 single measures and the best hybrid measure Logit-E15;
  \item \textbf{(b)} 8 hybrid measures.
\end{itemize}

\end{frame}



\begin{frame}
\frametitle{Hybrid Similarity Measure Logit-E15}
\begin{figure}
\centering
\includegraphics[width=0.32\textwidth]{figures/acacia-resnik} 
\includegraphics[width=0.32\textwidth]{figures/acacia-bda}
\includegraphics[width=0.32\textwidth]{figures/acacia-sda}

\includegraphics[width=0.32\textwidth]{figures/acacia-lsa}
\includegraphics[width=0.32\textwidth]{figures/acacia-ww}
\includegraphics[width=0.32\textwidth]{figures/acacia}
\caption{ Similarity scores between 74 words related to the word ``acacia''. }
\label{fig:hybrid-complimentary-discussion}
\end{figure}

\end{frame}


\begin{frame}
\frametitle{Supervised Hybrid Similarity Measures}

	\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{figures/hybrid-table}
		
		%\caption{ Performance of the hybrid supervised semantic similarity measures. }
\end{figure}
\end{frame}


\begin{frame}
\frametitle{Supervised Hybrid Similarity Measures (cont.)}
\begin{figure}
\centering
\includegraphics[height=0.4\textwidth]{./../figures/sn-accuracy}
\includegraphics[height=0.4\textwidth]{./../figures/bless-accuracy}
%\includegraphics[height=0.025\textwidth]{./../figures/spacer}
%\includegraphics[height=0.36\textwidth]{./../figures/bless-precision10}
%\includegraphics[height=0.36\textwidth]{./../figures/bless-precision20}
%\includegraphics[height=0.025\textwidth]{./../figures/spacer}
%\includegraphics[height=0.36\textwidth]{figures/bless-precision50}
%\includegraphics[height=0.36\textwidth]{figures/bless-recall50}     
     
\caption{ Meta-parameter optimization with the grid search of the C-SVM-radial-E15 measure.  }
\label{fig:radial-optimization}
\end{figure}
\end{frame}


\section[Applications]{Applications of Semantic Similarity Measures}
\subsection{Lexico-Semantic Search Engine ``Serelex''}

  


\begin{frame}
\frametitle{Search for Related Words: the List and the Graph}

\begin{itemize}
\item \url{http://serelex.cental.be/}
\end{itemize}


\begin{figure}	
	\centering
	\includegraphics[width=1.0\textwidth]{figures/python}
\end{figure}

\end{frame}



\begin{frame}
\frametitle{Search for Related Words: the Images}

\includegraphics[width=1.0\textwidth]{./../figures/citroyen}

\end{frame}

\begin{frame}

\begin{figure}
\frametitle{Evaluation of the Serelex}

\center
\includegraphics[width=0.6\textwidth]{./../figures/satisfaction}

\caption{Users' satisfaction with the top 20 results.}
\end{figure}
\end{frame}


\subsection{Filename Categorization System ``iCOP''}

\begin{frame}[fragile]
\frametitle{Examples of the Vocabulary Projection}

\begin{lstlisting}
18XGirls Yulia
>>18xgirls (-) 
>>yulia (3) = ekaterina, sonya, daughter.

HD Widgets Android
>>hd (@)
>>widget (3) = gadget, menu, button.
>>android (@)

mokey30 IRIX
>>mokey30 (-)
>>irix (5) = solaris, freebsd, nix, bsd, tru.

Plan9 unix
>>plan9 (-)
>>unix (9) = linux, macintosh, solaris, freebsd, bsd, window, platform, novell, sco.
\end{lstlisting}

\end{frame}


\begin{frame}
\frametitle{Evaluation of the Vocabulary Projection}
\begin{table}
\tiny

%\footnotesize
\centering
\begin{tabular}{|l|l|l|l|}

\hline
\bf Training Dataset & \bf Test Dataset & \bf Accuracy  & \textbf{Accuracy (voc. projection)} \\ \hline    

Gallery (train) & Gallery  & 96.41 & \textbf{96.83} (+0.42) \\
PirateBay Title+Desc+Tags & PirateBay Title+Desc+Tags &  \textbf{98.92} &  98.86 (--0.06)\\
PirateBay Title+Tags & PirateBay Title+Tags & \textbf{97.73} & 97.63 (--0.10) \\
Gallery & PirateBay Title+Desc+Tags & 90.57 & \textbf{91.48} (+0.91) \\
\alert{Gallery}  & \alert{PirateBay Title+Tags}  & \alert{84.23} & \alert{\textbf{88.89}} \alert{(+4.66)} \\
PirateBay Title+Desc+Tags & Gallery  & 88.83 & \textbf{89.04} (+0.21) \\
PirateBay Title+Tags & Gallery & 91.16 & \textbf{91.30} (+0.14) \\
\hline

\end{tabular}
\caption{ Performance of a SVM classifier (10-fold cross validation). }
\label{tbl:results2}

\end{table}
\end{frame}




\section{Conclusion}
\subsection{}


\begin{frame}
\frametitle{The Key Contributions}

%### This dissertation explored several strategies to semantic relation extraction with similarity measures. 

% ### This work brings several contributions to the field of computational lexical semantics:

\begin{enumerate}
\item A corpus-based measure \textbf{SDA-MWE}: 
\begin{itemize}
%\item performs comparably to the baselines;
\item can deal with both single words and multiword expressions.
%\item was applied to automatic thesaurus construction.
\end{itemize}

\item A definition-based measure \textbf{DefVectors}:
\begin{itemize}
%\item performs comparably to the baselines;
\item operates on a small-scale set of definitions.
%\item an open source implemention.
\end{itemize}
   
\item A corpus-based measure \textbf{PatternSim}:
\begin{itemize}
%\item performs comparably to the baseline measures;
\item requires no semantic resources.
%\item an open source implemention.
\end{itemize}

\item A large-scale \textbf{comparative study} of the measures:
\begin{itemize}
\item comparison w.r.t. semantic relations provided by measures.
%\item evaluation scripts/datasets are available to the community.
\end{itemize}


\end{enumerate}

\end{frame}

\begin{frame}
\frametitle{The Key Contributions (cont.)}

\begin{enumerate}

\setcounter{enumi}{4}



\item Hybrid supervised semantic similarity measures \textbf{Logit-E15}, \textbf{C-SVM-linear-E15}, \textbf{C-SVM-radial-E15}, etc.

\begin{itemize}
  \item based on the 5 types of resources;
  \item outperforms baselines and other hybrid measures.

\end{itemize}

\item \textbf{Applications} which rely on the measure \textbf{PatternSim}:

\begin{itemize}
\item lexico-semantic search engine \textbf{Serelex};
\begin{itemize}
%\item helps users discover similar words interactively;
\item 70\% of users's satisfaction;
%\item an open source implementation.
\end{itemize}

\item Filename Categorization System system \textbf{iCOP}:
\begin{itemize}
%\item categorizaton of file names;
\item the measure refines accuracy of the baseline up to 5\%;
%\item an open source implementation. 
\end{itemize}
 \end{itemize}

\end{enumerate}

\end{frame}







\begin{frame}
\frametitle{}

\Huge \bf Thank you! Questions?
\end{frame}
\end{document}