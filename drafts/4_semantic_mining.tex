\chapter{Mining semantic relations between key concepts}\label{phasetwo}



This chapter provides an  overview of the technique for discovering semantic relations between key concepts on the thesaurus. The sections 4.1-4.6 describe the method for mining semantic relations, the section 4.7 discuss its evaluation, and the section 4.8 presents plans for the future research. 

At this stage we would like to discover relations between descriptors of the thesaurus 
\begin{equation}
\label{eq:relations}
\hat{R}=\{r_{1},...,r_{n}\}, r_{i}=\left\langle d_{1},d_{2} \right\rangle, d_{i} \in D.
\end{equation}
which would be as close to the set of the manually relations $R$  as possible. To avoid prorogation of the errors from the term extraction stage, we rely on the vocabulary of the manually constructed thesaurus $D$ in our experiments. 

We apply a model from Vector Algebra to define semantic distance between descriptors \cite{citeulike:370340}, \cite{citeulike:34540} and estimate the parameters of the model with help of cooccurrence statistics of syntactic contexts of the descriptors in the text \cite{sahlgren}. The approach rely on the well-known distributional hypothesis proposed by Firth \cite{firth}, which states that words with a similar meaning appear in similar contexts. 

 
The procedure of calculating the set of candidate relations $\hat{R}$, we propose, comprises several steps:
\begin{enumerate}
	\item Preprocessing vocabulary and corpus
	\item Indexing descriptors of the thesaurus
	\item Extracting syntactic tuples
	\item Calculation of the feature matrix
	\item{Calculation of the similarities between descriptors}
	\item{Calculation of relations between descriptors}
\end{enumerate}

\section{Preprocessing vocabulary and corpus}

The goal of the first step is to \textit{clean} the dataset: we use regular expressions to normalize whitespaces, remove ``bad'' character sequences, and some meta information, such as document identifiers, from the texts. Also at this step we \textit{deaccent} the text documents and the vocabulary by substituting the characters with French diacritic symbols such as ``�'' or ``�'' with their non accented equivalents.\\

\section{Indexing descriptors of the thesaurus}

The goal of this step is to find all occurrences of the descriptors in the corpus and save information about them in an index file. In order to deal  with linguistic variation and some typos we use a function returning a regular expression for every given descriptor. Basically it performs the following modifications of the initial string with descriptor:
\begin{enumerate}
	\item Adds \verb@(\w){0,2}@ to the end of every word, except abbreviations
	\item Replaces articles and prepositions ``de'', ``du'', ``a'', ``la'', ``le'', ``les'',``des'', ``aux'', ``et'', ``au'', ``en'', ``pour'' with  \verb@(de|du|a|la|le|les|des|aux|et|au|en|pour|\s*)@
	\item Replaces ``d' '', ``l' '' with construction \verb@(d'\s*|l'\s*|d\s+|l\s+)(?=\.)@
	\item Replaces dots with \verb@(\.|\s|)@, except some ambiguous cases like c.e.s.
	\item Replaces whitespaces with \verb@ \s+ @
	\item Replaces dashes with \verb@(-|\s|)@	
\end{enumerate}
 
As a result, with help of this function the descriptor \textit{``convention internationale''} will be transformed into the regular expression
\begin{verbatim}
\bconvention(\w){0,2}\s+internationale(\w){0,2}(\b|\W)
\end{verbatim}
which captures both singular form \textit{``convention internationale''} and plural form \textit{``conventions internationales''} of the phrase. Similarly, both phrases \textit{``marches a terme''} and	\textit{``marche aux termes''} will be found with help of the following automatically generated regular expression:
\begin{verbatim}
\bmarches(\w){0,2}\s+(de|du|a|la|le|les|des|aux|et|au|en|pour|\s*)\s+
terme(\w){0,2}(\b|\W)
\end{verbatim}

One record of the index stores information about one occurrence of the descriptor $d_{i}$ in the document $\delta_{j}$ starting at the position $b$
\begin{equation}
\label{eq:indextuple}
 \left\langle \delta_{i},b,e,d_{j}\right\rangle , {doc}_{i} \in DOC, \{b,e\}\in \mathbb{N}, d_{j}\in D.
\end{equation}
Set of all index records compose an index $I$. The following example illustrates the format of the index file:
\begin{lstlisting}
2;529;552;personnel de l'aeroport
2;544;552;aeroport
2;637;647;transport
2;742;751;travail
2;756;764;aeroport
2;786;791;livre
2;796;808;marchandise
2;871;879;ministre
2;1052;1059;depute
2;1093;1099;depute
2;1133;1156;president de la chambre
2;1149;1168;chambre des deputes
2;1161;1168;depute
\end{lstlisting}

The procedure of indexing descriptors have revealed the fact that only 82\% (3894 of 4771)  of descriptors could be found in the corpus, moreover just 72\% (3451) of the descriptors were found more than 5 times in the text. Calculating the cooccurence statistics for the terms which are not present in the corpus is obviously not possible, furthermore taking into account terms which occured just a few times does not seems to be a good idea neither.  The histogram of the descriptors frequency distribution is depicted on the figure~\ref{fig:indexdist}. At this step we keep all $n=3894$ descriptors, with aim to figure out how much the sparsity of the representation affects the quality of the model.

\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{pics/index.eps}
	\caption{ Descriptors frequency distribution.}
	\label{fig:indexdist}
\end{figure}

\section{Extracting syntactic tuples}

At this step we parse the document collection with natural language parser \cite{xip} so as to collect statistics about\textit{ syntactic contexts} of the descriptors. The parser basically, returns us for every given sentence a syntactic tree, which reveals the grammatical structure of the sentence; for instance, the parsing tree for the sentence \textit{``Le Conseil d�sapprouve cette approche'}' is depicted on the figure~\ref{parsetreetok}. Furthermore, for every sentence $s$ parser outputs a set of syntactic dependencies 
\begin{equation}
\label{eq:syntuples}
SR_{s}=\{sr_{1},sr_{2},... \},
\end{equation}
where every dependency $sr$ denoted as
\begin{equation}
\label{eq:syntuple}
\left\langle w_{i}, b_{i}, t_{j}, w_{k}, b_{k} \right\rangle, 
\end{equation}
tells us that word $w_{i}$, starting at the position $b_i$ and word $w_{j}$, starting at the position $b_j$ are linked with the syntactic dependency of type $t_{j} \in T$. Thereby, for the previous sentence parser will return the following set of syntactic dependencies (we use the XIP parser \cite{xip}, another parser could produce different set of dependencies):

\begin{eqnarray*}
 			\left\langle \mbox{Conseil}, 4, DETERM, \mbox{Le}, 0 \right\rangle \\
			\left\langle \mbox{d�sapprouve},11, DETERM, \mbox{cette},22 \right\rangle \\
			\left\langle \mbox{d�sapprouve},11, SUBJ, \mbox{Conseil},3 \right\rangle \\
			\left\langle \mbox{d�sapprouve},11, OBJ, \mbox{approche}, 29\right\rangle \\
\end{eqnarray*}

\begin{figure}
\label{parsetreetok}
\Tree 
[.TOP 
		[.SC 
		[.NT [.N $w_{1}=Le$ ] [.NOUN $w_{2}=Conseil$ ]] 
				[.FV [.VERB $w_{3}=desapprouve$ ]] 
		] 
		[.NP [.DET $w_{4}=cette$ ] [.NOUN $w_{5}=approche$ ]]		
] 
\caption{Syntactic tree for the sentence ``Le Conseil d�sapprouve cette approche.''}
\end{figure}

In spite of the fact that parser, provides us with various dependencies, some of them just capture auxiliary structures of the language. For instance, the relation which links a nominal head and a determiner  ($t_{i}=DETERM$) is known to bring little information about the semantic meaning of the head. We select 9 types of syntactic dependencies for our model listed in the table ~\ref{tab:xipdep}, which are known to be most informative due to the previous studies e.g., \cite{citeulike:281047} or \cite{piersman}.

\begin{table}
	\centering	
	\caption{Syntactic dependencies used in the model.}
		\begin{tabular}{ | l | p{11cm}|}
    \hline 
    Dependency name & Description \\  \hline \hline
		\textit{ADJMOD} & Attaches the modifier of adjective to the adjective itself. \\  \hline
		\textit{CONNECT} & Links the verb of a finite clause to the grammatical word that introduces the clause. \\  \hline
		\textit{COORDITEMS} & This binary relation links coordinated elements. \\  \hline
		\textit{DEEPOBJ} & This dependency attaches a deep object to the verb. \\  \hline
		\textit{DEEPSUBJ} & This dependency attaches a deep subject to the verb. \\  \hline
		\textit{NMOD} & Attaches a modifier to the noun it modifies. \\  \hline
		\textit{OBJ} &  Attaches a direct object to its verb. \\ \hline
		\textit{SUBJ}  &  Attaches the surface subject to the verb, including infinitive verbs.  \\ \hline
		VMOD & Attaches a modifier of a verb to the verb itself. The modifier can be an indirect complement or an adjunct of the verb. \\  \hline		
		\end{tabular}
		\label{tab:xipdep}
\end{table}

%
\section{Calculation of the feature matrix}
The first step at this stage is defining a set of features 
\begin{equation}
\label{eq:features}
B = \{\beta_{1},...,\beta_{n}\}
\end{equation}
which are relevant for representing the semantic information associated with a terms of the thesaurus. Thereafter we represent the descriptors of the thesaurus in this feature space. We are interested in such feature spaces where distances between semantically related terms are small and distances between semantically unrelated terms are large, with respect to some similarity metric. In general, we are also interesting in minimizing number of features because of computational performance reasons and the general ``Occam's razor'' principle \cite{occam}. \\

We will use the \textit{syntactic features} composed from a token and type of syntactic dependency, denoted as follows
\begin{equation}
\label{eq:synfeature}
\beta=\left\langle t,w \right\rangle, t \in T, w \in W.
\end{equation}

Provided set of syntactic tuples ~\ref{eq:syntuples} one way of inducing set of features is to employ the function $f_{SR-B}$ which maps every syntactic tuple $\left\langle w_{i},b_i, t_{j}, w_{k},w_k \right\rangle$ to the syntactic feature $\left\langle t_{j}, w_{k} \right\rangle$, thereby
\begin{equation}
\label{eq:featuremap}
B=\bigcup_{sr \in SR}f_{SR-B}(sr).
\end{equation}
As a result we will obtain a set $B$ containing $n$ syntactic features; every descriptor $d_{i}$ in our model will be represented with a characteristic vector $\mathbf{f_{i}}$ in the standard $n-$dimensional space $\mathbb{R}^n$
\begin{equation}
\label{eq:fvector}
\mathbf{f}_{i}= \sum_{j=1}^{n} f_{ij}\mathbf{b}_{j} = (f_{i1},...,f_{in}).
\end{equation} 
Thus in this representation we map every syntactic feature $\beta_{i}$ to a standard basis vector $\mathbf{b_{i}}$ of the standard $n-$dimensional basis $\mathbf{b}=(\mathbf{b}_{1},...,\mathbf{b}_{n})$ 
\begin{eqnarray*}
\label{eq:stbasis}
\mathbf{b}_{1} = (1,0,...,0), \\
\mathbf{b}_{2} = (0,1,...,0), \\
...		 \\
\mathbf{b}_{n} = (0,0,...,1).
\end{eqnarray*}

A system of the characteristic vectors for all descriptors of the thesaurus $(\mathbf{f}_{1},...,\mathbf{f}_{n})^T$ compose a \textit{feature matrix} denoted as follows
\begin{equation}
\mathbf{F}= \left(
\label{eq:fmatrix}
\begin{array}{cccc}
f_{11} & f_{12} & \ldots & f_{1n} \\
f_{21} & f_{22} & \ldots & f_{2n} \\
\vdots & \vdots & \ddots & \vdots \\
f_{m1} & f_{m2} & \ldots & f_{mn} \\                                                                                     
\end{array}
\right).
\end{equation} 
\par

In this model, every descriptor modeled as a point in the feature space; to calculate the coordinates of every term we use the cooccurrence statistics of the syntactic contexts  of the descriptors. One syntactic context in the model defined as follows
\begin{equation}
c_{ij}=\left\langle d_{i},\beta_{j} \right\rangle, d_i \in D, \beta_j \in B.
\end{equation}
One syntactic context establishes a relation between a syntactic tuple and some descriptor. We induce a multiset of syntactic contexts $C$ from the set of syntactic tuples~\ref{eq:syntuples} and index of descriptors ~\ref{eq:indextuple} with help of the algorithm~\ref{alg:contexts}. The coordinate $f_{ij}$ of the vector modeling the descriptor $d_{i}$ defined as number of syntactic contexts $\left\langle d_{i},\beta_{j} \right\rangle$ extracted from the corpus, what equals \textit{multiplicity} $m(c_{ij})$ of the context $c_{ij} \in C$. Thus the coordinates of a descriptor $d_i$ in the feature space calculated as follows
\begin{equation}
\label{eq:featurevector}
\mathbf{f}_{i}=\sum^{n}_{j=1}m(\left\langle d_{i},\beta_{j} \right\rangle)\mathbf{b}_j.
\end{equation}

%Algorithm

\begin{algorithm}
\caption{Calculation of multiset of syntactic contexts $C$}
\label{alg:contexts}
\algsetup{indent=3em}
\begin{algorithmic}[1]
     \REQUIRE $SR=\bigcup_{s=1}^{N} SR_s -$ set of syntactic tuples, where $SR_s$ is set of syntactic tuples for the sentence $s$,\\
	$I-$ vocabulary index,\\
	$StopPOS-$ list of stop POS such as determiner, pronoun etc.
	$StopWORD-$ list of stopwords.
     
  	\STATE $C \leftarrow \emptyset$
  	\STATE $w_{context} \leftarrow$ ""
  	\STATE $w_{descr} \leftarrow $ ""
  	\FOR{$s=1$ to $N$}         
				\FORALL{$\left\langle w_{i}, b_{i}, t_{j}, w_{k}, b_{k} \right\rangle \in SR_{s}$}
        		\IF{ $\exists \left\langle \delta,b,e,d \right\rangle \in I: b_i \in [b;e]$ }
        		\STATE $w_{context} \leftarrow w_i$ 
        		\STATE $w_{desc} \leftarrow d$         		
        		\ELSIF{ $\exists \left\langle \delta,b,e,d \right\rangle \in I: b_k \in [b;e]$ }
        		\STATE $w_{context} \leftarrow w_k$ 
        		\STATE $w_{desc} \leftarrow d$         		
        		\ELSE
        		\STATE \textbf{continue}
        		\ENDIF
        		
        		\IF{$w_{context} \notin w_{desc}$ \textbf{and} $w_{desc} \notin w_{context}$ \textbf{and} \\ 			
        		$\FUNC{POS(w_{context})} \notin StopPOS$ \textbf{and} $w_{context} \notin StopWORD$}
        		\STATE $\beta \leftarrow \left\langle t_j,w_{context}  \right\rangle$
        		\STATE $C \leftarrow C \cup \leftarrow \left\langle w_{desc}, \beta  \right\rangle$
        		\ENDIF
        		
        \ENDFOR
     \ENDFOR
     \RETURN $C$
\end{algorithmic}
\end{algorithm}
The intuition behind the algorithm is as follows: for every descriptor find all corresponding features which would provide us with some piece of information about the semantics of the term. To make more clear how the set of syntactic contexts is calculated, let us look at details of calculating $C_s$ for the sentence \textit{``La proposition de loi se r�f�re aux candidats au permis de conduire et ne contient aucune disposition relative � des v�hicules''}. This sentence $s$ composed of four descriptors of the vocabulary and consequently the index $I_s$ contain the four following records
\begin{eqnarray*}
	\left\langle 1,3,21,\mbox{proposition de loi}\right\rangle \\ 
	\left\langle 1,25,30,\mbox{refere} \right\rangle \\ 
	\left\langle 1,49,67,\mbox{permis de conduire} \right\rangle \\ 
	\left\langle 1,108,116,\mbox{v�hicule} \right\rangle  
\end{eqnarray*}
For this sentence the set of syntactic tuples $SR_s$, extracted with help of the parser, looks like as follows
\begin{eqnarray*}
 			\left\langle \mbox{r�f�re},25, SUBJ, \mbox{proposition},3 \right\rangle \\
			\left\langle \mbox{permis},49, NMOD, \mbox{conduire},59 \right\rangle \\
			\left\langle \mbox{r�f�re},25,COORDITEMS, \mbox{contient},75 \right\rangle \\
			\left\langle \mbox{contient},75, OBJ, \mbox{disposition}, 91 \right\rangle \\
			\left\langle \mbox{disposition}, 91, NMOD, \mbox{relative}, 106 \right\rangle \\
			\left\langle \mbox{relative}, 106, ADJMOD, \mbox{v�hicules},121 \right\rangle \\
			\left\langle \mbox{proposition}, 3, NMOD, \mbox{loi},17 \right\rangle \\
			\left\langle \mbox{candidats},37, NMOD, \mbox{permis},49 \right\rangle \\
			\left\langle \mbox{r�f�re},25, VMOD, \mbox{candidats},37 \right\rangle \\
			\left\langle \mbox{contient},75, SUBJ, \mbox{proposition},3 \right\rangle 
\end{eqnarray*}
and finally, the algorithm produce the following set of syntactic contexts $C_s$ 
\begin{eqnarray*}
 			\left\langle \mbox{proposition de loi}, \left\langle SUBJ, \mbox{contenir} \right\rangle \right\rangle \\
			\left\langle \mbox{permis de conduire}, \left\langle NMOD, \mbox{candidat} \right\rangle \right\rangle \\
			\left\langle \mbox{vehicule}, \left\langle ADJMOD, \mbox{relatif} \right\rangle \right\rangle \\
			\left\langle \mbox{r�f�re}, \left\langle SUBJ, \mbox{proposition} \right\rangle \right\rangle \\
			\left\langle \mbox{r�f�re}, \left\langle VMOD, \mbox{candidat} \right\rangle \right\rangle \\
			\left\langle \mbox{r�f�re}, \left\langle COORDITEMS, \mbox{contenir} \right\rangle \right\rangle 
\end{eqnarray*}

The result of applying the alrogithm~\ref{alg:contexts} on the dataset is a feature matrix of the size $[3894\times 55036]$, what means that 55036 various features were extracted from the corpus. In fact, not all of these features are equally useful, for example, the feature $\left\langle SUBJ, \mbox{etablir} \right\rangle$ occurred 995 times and intuitively seems to contain more semantic information, rather that the feature $\left\langle NMOD;\mbox{n�136} \right\rangle$ which was found in the whole corpus just once.\\
Another issue with this feature space lies in the fact that some of the features capture exact dates, numbers, and amount what introduce some additional sparsity. For instance, there is little chance that we will find the feature  $\left\langle NMOD,\mbox{37 millions} \right\rangle$ many times, but we can find many closely related features such as  $\left\langle NMOD,\mbox{71,33 millions} \right\rangle$ or  $\left\langle NMOD;\mbox{133 millions}\right\rangle$. For reducing the sparsity of this representation we merge several features in one. Finally we do the following data cleansing  of the features $B$ with help of regular expressions:
\begin{enumerate}
	\item Group features which contain percent sign (\%) into the feature $\left\langle NMOD;\mbox{@percent} \right\rangle$ 
	\item Group features which represent sum of money into the feature $\left\langle NMOD;\mbox{@sum-of-money} \right\rangle$
	\item Delete features which contain identifiers, for example numbers of the document etc.
	\item Group features which represent dates into the feature $\left\langle NMOD;\mbox{@date-time} \right\rangle$
	\item Group features which contain ordinal numbers, like ``quatrieme'', ``cinquieme'' or ``4ieme'' in the feature $\left\langle NMOD;\mbox{@ordinal-number} \right\rangle$
	\item Group features which represent real numbers into the feature $\left\langle NMOD;\mbox{@number} \right\rangle$
	\item Delete features which contain special characters
	\item Delete features which contain words from the stoplist	 
\end{enumerate}
All these actions already allows us reduce amount of features till $m=43406$, but still this representation is quite sparse. Indeed, just 16\% (7294) of the extracted features occur more than 30 times. The figure~\ref{featuresdist} depicts the histogram of feature frequency distribution.
 
\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{pics/featuresdist.eps}
	\caption{Features frequency distribution.}
	\label{featuresdist}
\end{figure}

In order to reduce the number of the features we also can remove the features which occurs just a few times. The number $t$ here is an arbitrary threshold; we will experiment with various values of this parameter.

Finally, we normalize the feature matrix $\mathbf{F}$ as follows
\begin{equation}
\widetilde{f_{ij}} = \frac{f_{ij}}{\sum^n_{j=1}f_{ij} \cdot o_i },  
\end{equation}
where $o_i$ is number of occurrences of the descriptor $d_i$ in the corpus. After the normalization of the matrix, we obtain a new normalized matrix $\widetilde{\mathbf{F}}$ where every element $\widetilde{f_{ij}} \in [0;1]$.

\section{Calculation of similarities between descriptors}
Semantic similarity between two descriptors $d_{i}$ and $d_j$ defined in the model as similarity between their respective characteristic vectors $\mathbf{f}_i$ and $\mathbf{f}_j$. We use cosine between corresponding vectors as a measure of semantic similarity 
\begin{equation}
sim(d_i,d_j)=s_{ij}=\frac{\widetilde{\mathbf{f}}_i \cdot \widetilde{\mathbf{f}}_j}{\left| \widetilde{\mathbf{f}}_i \right| \cdot  \left| \widetilde{\mathbf{f}}_j \right|}=
\frac{\sum^{n}_{k=1}\widetilde{f}_{ik}\cdot \widetilde{f}_{jk}}{\sqrt{\sum^{n}_{k=1}\widetilde{f}_{ik}^2 \cdot \sum^{n}_{k=1}\widetilde{f}_{jk}^2}}}.
\end{equation}
We calculate the similarity matrix $\mathbf{S}$ with help of this proximity measure; an element $s_{ij} \in [0;1]$ of this matrix shows a measure of semantic similarity between descriptor $d_i$ and $d_j$.  


\section{Calculation of relations between descriptors}
We calculate relations between descriptors by thresholding the similarity matrix $S$ with a certain boundary $s_t$:
\begin{equation}
\hat{R} = \{ \left\langle d_i,d_j \right\rangle  \ : s_{ij} \geq s_t\}.
\end{equation}
The amount of relations between the descriptors is completely depends on the value the threshold $s_t$, that is why we will experiment with several values of this parameter. \\
Our goal is to reconstruct the set of relations of the original thesaurus $R$. Therefore we would like to obtain approximately the same number of relations as was in the manually constructed thesaurus. The figure~\ref{smatrix} depicts two similarity matrices which were calculated with help of the method. The left matrix $\mathbf{S}_{0.15}$ was thresholded with the boundary $s_t=0.15$ what gave 24,848 relations, and the right one $\mathbf{S}_{0.45}$ was thresholded with boundary $s_t=0.45$, what gave 5662 relations. 
\begin{figure}
	\centering
		\includegraphics[width=1.0\textwidth]{pics/s.eps}
	\caption{Similarity matrices $\mathbf{S}_{0.15}$ (on the left) and $\mathbf{S}_{0.45}$ (on the right).}
	\label{smatrix}
\end{figure} 

\section{Evaluation}\label{firstresults}

\subsection{Assessment protocol}
The original thesaurus contains 3221 hierarchical and associative relations and 5705 synonymy relations. This thesaurus is a hand crafted linguistic resource done by an expert. This means that if another expert would constructed the same thesaurus he would created a different semantic dictionary. This makes a certain problem for evaluation of our results which could be demonstrated with the following example. Consider the next set of automatically discovered relations:
\\
$d_1=$ alimentation animale \\
$R_1=\{$ elevage, production agricole, agriculture, alimentation} $\}$ \\
$\hat{R}_1=\{$  terre agricole, \mbox{boisson} $\}$ \\ \\
$d_2=$ australie \\
$R_2=\{$ pays etranger, monde politique $\}$ \\
$\hat{R}_2=\{$ benin, guinee-bissau, madagascar, moldavie, voirie de l'etat $\}$ \\ \\
$d_3=$ analyse medicale \\
$R_3=\{$ biologie medicale, medecine, sante  $\}$ \\
$\hat{R}_3=\{$ assistance medicale, procreation medicalement assistee, biologie medicale, deontologie, organisation medicale, profession medicale $\}$ \\ \\
$d_4=$ ecole de musique \\
$R_4=\{$ enseignement musical, education artistique, enseignement $\}$ \\
$\hat{R}_4=\{$ musique militaire $\}$ \\
This set of relations contains the only one exact match with the original thesaurus such that 
\begin{equation}
R_i \cap \hat{R}_i \neq \emptyset, i = \overline{1,4},
\end{equation}
the relation ``analyse medicale''$\leftrightarrow$``biologie medicale''. However, we can argue that other relations also make sense. In effect, there is no direct relation in the thesaurus between the descriptors ``australie'' and ``benin'', but the short transit path between them: 
\begin{quote}
``australie'' $\leftrightarrow$ ``monde politique'' $\leftrightarrow$ ``benin''.
\end{quote}
Existence of the short paths between descriptors could show the relatedness between the descriptors. 

We use this idea to evaluate the results more flexibly. We construct two \textit{relaxed} versions of the original set of relations $R$: the $R_{fuzzy3}$ and the $R_{fuzzy4}$. We calculate these fuzzy relations from the original thesaurus with help of the Floyd algorithm \cite{floyd}, which finds all the shortest paths for a given graph. We represent the relaxed versions of the thesaurus with the adjacency matrix $\mathbf{W}$ such that
\begin{equation*}
w_{ij} =\left\{ 
\begin{array}{rl}
1 & \text{if } \exists \left\langle d_{i},d_j \right\rangle \in NT \\
2 & \text{if } \exists \left\langle d_j,d_{i} \right\rangle \in NT \\
1 & \text{if } \exists \left\langle d_i,d_j \right\rangle \in RT \\
1 & \text{if } \exists \left\langle d_j,d_{i} \right\rangle \in RT \\
0 & \text{otherwise }
\end{array}
\end{equation*}
Provided this adjacency matrix we could calculate the matrix of shortest paths $\mathbf{P}$ where $p_{ij}$ is a shortest path in the thesaurus from the concept $c_i$ to the concept $c_j$. We calculate  the relaxed version of the thesaurus $R_{fuzzy3}$ by thresholding the matrix of shortest paths $\mathbf{P}$ with the threshold equals 3 
\begin{equation}
\hat{R}_{fuzzy3} = \{ \left\langle d_i,d_j \right\rangle  \ : p_{ij} \leq  3\},
\end{equation}
the fuzzy version of the thesaurus $R_{fuzzy4}$ is calculated in the same way, but the threshold in this case equals 4. We obtained using the described technique 80.641 relations in the $R_{fuzzy3}$ and 254.441 relations in the $R_{fuzzy4}$. Finally, We evaluate our model with the three measures: the pessimistic one
\begin{equation}
Precision^{++}= \frac{| \hat{R} \cap R |}{\left| \hat{R} \right|} 
\end{equation}
the medium one
\begin{equation}
Precision^{+}= \frac{| \hat{R} \cap R_{fuzzy3} |}{\left| \hat{R} \right|} 
\end{equation}
and the optimistic one 
\begin{equation}
Precision^{+-}= \frac{| \hat{R} \cap R_{fuzzy4} |}{\left| \hat{R} \right|}.
\end{equation}


\subsection{Results}
Below we present some relations between descriptors which were mined with the model. Further results produced by the method could be found in the App.~\ref{outputs}. \\ \\
$d_1=$ administration de l'enregistrement et des domaines \\
$R_1=\{$ finances; administration de l'etat;  $\}$ \\
$\hat{R}_1=\{$ administration des contributions; administration du cadastre et de la topographie; centre socio-educatif de l'etat; enseignement public; institution culturelle; institut d'hygiene et de sante publique; station viticole de l'etat; petange; materiel de bureau;  $\}$ \\ \\
$d_2=$administration des contributions  \\
$R_2=\{$ administration des contributions, administration de l'etat $\}$ \\
$\hat{R}_2=\{$ administration du cadastre et de la topographie, centre socio-educatif de l'etat, enseignement public, institution culturelle, institut d'hygiene et de sante publique, station viticole de l'etat $\}$ \\ \\
$d_3=$ admission aux etudes \\
$R_3=\{$ organisation scolaire, education; admission a l'emploi $\}$ \\
$\hat{R}_3=\{$ archives de l'etat, brevet d'instituteur, programme d'etudes $\}$ \\ \\
$d_4=$ assistance medicale  \\
$R_4=\{$ organisation medicale $\}$ \\
$\hat{R}_4=\{$ aide medicale urgente, analyse medicale, procreation medicalement assistee, hygiene, institut viti-vinicole, organisation medicale, profession medicale, vaccination $\}$ \\ \\
$d_5=$ aide medicale urgente \\
$R_5=\{$ assistance medicale, organisation medicale $\}$ \\
$\hat{R}_5=\{$ analyse medicale, assistance medicale, procreation medicalement assistee, deontologie, organisation medicale, profession medicale, responsabilite civile, service incendie, vaccination $\}$ \\ \\
$d_6=$ bourse d'etudes \\
$R_6=\{$ vie scolaire, education $\}$ \\
$\hat{R}_6=\{$ mouvement de jeunesse $\}$ \\ \\

The figure~\ref{perf} plots the optimistic, medium and optimistic estimations of the precision, depending on the value of the thresholds $t$ and $s_t$. We can see from the figure that the combination of the similarity matrix threshold $s_t=0.4$ with the feature matrix threshold $t=75$ provided us  with the best results among all the combinations, namely
\begin{eqnarray*}
Precision^{++} = 7\%, \\
Precision^{+} = 35\%, \\
Precision^{+-} = 46\% .
\end{eqnarray*}
Finally, the fourth diagram (d) of the figure~\ref{perf} illustrates the relationship between value of the threshold $t$ and the number of minded relations. 
\begin{figure}
	\centering
		\includegraphics[width=1.2\textwidth]{pics/eval.eps}
	\caption{The optimistic (a), medium (b), and oprimistic (c) estimations of the precision of the method. }
	\label{perf}
\end{figure} 

\section{Conclusion and future work}\label{futurework}

The first results showed us that using cooccurrence statistics for discovering semantic relations between descriptors of thesaurus is a viable approach. Though, the moderate value of the precision suggests further refinements. The following steps of this research will involve:
\begin{itemize}
	\item Trying to discover relations between the automatically extracted concepts
	\item Experimenting with other similarity metrics
	\item Normalizing the feature matrix $\mathbf{F}$ with the TF-IDF ratio
	\item Experimenting with more advanced feature selection methods
	\item Trying to cluster the similarity matrix $\mathbf{S}$ 
	\item Experimenting with the LSA method, trying to adopt the the tensor LSA method 
	\item Trying to formulate the supervised problem, where the manually constructed thesaurus will be used as training dataset 
	\item Investigate what types of syntactic relations provide better results
\end{itemize}
