\chapter{Extracting key concepts from text documents}\label{key}

This chapter provides an overview of the approach for  key concept extraction from the corpus. The two principal components of a thesaurus are set of concepts and set of relations between them. Every concept $c$ represent one important notion of the domain and consist of set of descriptors  $\{ d_1, d_2,...\}$ -- key words or phrases in the domain. For instance, the domain ``Transport'' include the three following concepts 
\begin{eqnarray*}
 			c_1 =  \{ \text{motocycle}, \mbox{scooter}, \mbox{velomoteur}, \text{motocyclette}, \text{cyclomoteur} \} \\
 			c_2 = \{  \text{aeroport}, \mbox{aerogare}, \mbox{aerodrome}\} \\			
 			c_3 = \{  \text{transport par chemin de fer}, \mbox{transport par rail}, \mbox{transport ferroviaire} \} 			
\end{eqnarray*}
Note that the concepts $c_1$ and $c_2$ consist of words (monolexical terminological units) such as ``aerodrome'', while the concept $c_3$ consist of phrases (polylexical terminological units) like ``transport par rail''. In our approach we firstly extract set of keywords $\hat{W}_{key}$. Secondarily, we extract set of key phrases. The union of all the keywords and the key phrases comprise the \textsl{extracted vocabulary} $\hat{D}= \hat{W}_{key} \cup \hat{NP}_{key}$. 

The section~\ref{key:keywords} of this chapter deals with the keyword extraction methods and their automatic evaluation. Section~\ref{key:keyphrases} describes how we extract the key phrases from the corpus. Finally, the section ~\ref{key:conclusion} contains a discussion and outlines directions for future research in term extraction.

%-----------------------------------------------------------
\section{Extracting keywords}\label{key:keywords}

At this step we would like to extract set of keywords $\hat{W}_{key}$ from the corpus $DOC$, which would be as close to the set of words from the manually created thesaurus $W_{key}$ as possible. The method for keywords extraction comprise the following steps:
\begin{enumerate}
	\item Preprocessing of vocabulary and corpus
	\item Filtering words based on linguistic information
	\item Filtering words based on statistical information
\end{enumerate}
The following subsections describe in detail every step of this approach and its evaluation. 

%-----------------------------------------------------------
\subsection{Preprocessing of vocabulary and corpus}

At the first step we tokenize, lemmatize the document collection and perform part-of-speech tagging with the natural language parser \cite{xip}. Thereby, for example the sentence ``Dans ce contexte j'aimerais poser les questions suivantes aux Ministres comp�tents'' would be transformed into the set of following lemmas:
\begin{equation}
\label{eq:wsnt}
W_{snt} = \{ \text{dans}, \text{ce}, \text{contexte}, \text{je}, \text{aimer},\\ \text{poser}, \text{le}, \text{question}, \text{suivant}, \text{a}, \text{ministre}, \text{competent} \}.
\end{equation}
The described procedure returns for every document $doc_i$ set of lemmatized and normalized words $W_i$, extracted from this document. The
 output of the preprocessing stage is set of lemmatized and normalized words which were detected in the corpus 
\begin{equation}
W = \bigcup^{|DOC|}_{i=1}W_i = \{ w_1,w_2,... \}
\end{equation}
For our corpus of political texts we collected around 149.500 of different ``words'', including punctuation marks, special symbols, words with typos etc. 

%-----------------------------------------------------------
\subsection{Filtering words based on linguistic information}

Our goal is to keep the words which represent key notions of the domain and leave out all the rest terms. At this stage we perform filtering of the extracted words $W$ based on the linguistic information, associated with every word $w$as follows:

\begin{enumerate}
	\item Delete stop-words and stop-characters (we use 580 manually selected words in the stoplist). 
	\item Delete numerals (we use list of French numerals and tags coming from the parser). 
	\item Delete dates (filter based on tags coming from parser and regular expressions). 
	\item Delete personal named entities (filter based on tags coming from parser).
	\item Keep only the nouns and adjectives (filter based on tags coming from parser). 
\end{enumerate}
  
This filter removes terms which could not be keywords in most of cases for most of document collections: prepositions, auxiliary verbs etc. Of course there could be exceptions, for instance if one would like to extract keywords from the Wikipedia page about French articles, the stoplist would leave out the relevant keywords ``le'', ``la'', and ``les''. Nevertheless, this relatively ``close'' filter seems to be appropriate for the terminology extraction task and for our current corpus of political letters. 

The output of this step is set of lemmatized and normalized words, passed the linguistic filter. For our dataset this filter removed about half of words from $W$, therefore at the end of this step we obtained around 78.250 words. Thus, for instance the set of words ~\eqref{eq:wsnt} would looks like as follows after passing this filter:

\begin{equation}
\label{eq:wsntlf}
W_{snt} = \{ \text{contexte}, \text{question}, \text{ministre}, \text{competent} \}.
\end{equation}


%-----------------------------------------------------------
\subsection{Filtering words based on statistical information}

The previous step removes terms, which are normally could not be keywords. The goal of this stage is to select from the words passed the linguistic filter, the keywords $\hat{W}_{key}$. To do this firstly we rank the words with help of statistical information, associated with every word $w$. For ranking words we use the ranking function $\Re$ which associates each word $w \in W$  with a real nonnegative real number -- its rank:

\[ \Re :W \to [0;+\infty) \]
The bigger the rank the more salient the word. The ranking function $\Re$ defines a partial order $\leq$ on the set of words $W$. Therefore, the ranking function and the set of words define a \textit{partially ordered set}:
\begin{equation}
W^{\Re} = (W,\leq).
\end{equation}

A ranking function in our case is a function used to order words in the corpus according to their relevance to a specific domain, concerned by the corpus. Thus, for instance, if we deal with the domain ``Transport'' the ideal ranking function would associate high ranks to the words which represent important concepts of the domain such as  ``motocycle'', ``autobus''  or ''aeroport'', and low ranks to the words which are not related to the domain like ``homme'', ``question'' or ``resistance''. 

Secondly, we put the words from the top of the ranked list to the set of keywords $\hat{W}_{key}$. A combination of ranking function and way we select the words from the ranked list makes a \textit{statistic filter}. For the moment we experiment with three baseline statistic filters \cite{manning_nlp}: 
\begin{enumerate}
	%-----------------------------------------------------------
	\item \textbf{Filter by frequency}
	
The ranking function of this filter orders words by number of occurrences in the whole corpus. Thus, rank of the word $w_i$ exactly equals to the number of times $n_i$ the word occurred in the corpus:	
	\begin{equation}
	rank_i = n_i.
	\end{equation}
We include into the set of keywords $\hat{W}_{key}$ the first $x\%$ of words in the partial ordered set $W^{\Re}$, but not more than $y$ words:
	
\begin{equation}	
\label{eq:thresholdbypercent}
\hat{W}_{key} = \left\{ w_i \in W^{\Re}: i \geq  \min \left( \frac{x}{{100}}|W|, y \right) \right\} .
\end{equation}
	
%-----------------------------------------------------------
\item \textbf{Filter by ``global'' TF-IDF}
	
The ranking function of this filter is the TF-IDF \cite{tfidf}. This function take into account not only frequency, but also \textit{specificity} of the word with help of the inverse document frequency. Basically, the function would rank higher a word which occurred just in a few documents with respect to the word which occurred in every document of the document collection. The main rationale behind the TF-IDF is what the general words, like ``homme'' or ``monsieur'' tend to appear in more documents than the specific ones, like ``bicyclette'' or ``chauffeur''. Rank of the word $w_i$ calculated with this function equals
	
	\begin{equation}	
	\label{eq:gtfidf}
	rank_{i} = \frac{{{n_i}}} {{\sum\limits_{j = 1}^{|W|} {{n_j}} }}\log \left( {\frac{{\left| {DOC} \right|}} {{\left| {\left\{ {\left. {doc_k:{w_i} \in W_k} \right\}} \right.} \right|}}} \right)	.
	\end{equation}
	
The formula ~\eqref{eq:gtfidf} combines information about the ``global'' frequencies $n_i$ with the inverse document frequencies (logarithmic term). In this filter we use the same method for keyword selection as in the previous one~\eqref{eq:thresholdbypercent}. 

%-----------------------------------------------------------
\item \textbf{Filter by ``local'' TF-IDF}
	
This filter ranks extracted words $W_i$ from the document $doc_i$ with the TF-IDF and outputs set of keywords $\hat{W}_{key}^i$ for the given document. The rank of the word $w_i$ for the document $doc_j$ equals
\begin{equation}	
\label{eq:ltfidf}
rank_{ij} = \frac{{{n_{ij}}}} {{\sum\limits_{k = 1}^{|W_j|} {{n_{kj}}} }}\log \left( {\frac{{\left| {DOC} \right|}} {{\left| {\left\{ {\left. {doc_k:{w_i} \in W_k} \right\}} \right.} \right|}}} \right)	
\end{equation}

This ranking formula takes into account the local frequencies of words and the previous one~\eqref{eq:gtfidf} takes into accound the global frequencies. This filter selects the keywords with help of the same thresholding technique as the previous filters~\eqref{eq:thresholdbypercent}. In order to obtain the global list of keywords we join keywords extracted from all the documents: 
\begin{equation}	
\hat{W}_{key} = \bigcup\limits_{i = 1}^{|{DOC}|}{\hat{W}_{key}^i}. 
\end{equation}	

\end{enumerate}

%-----------------------------------------------------------
\subsection{Evaluation and the results}

To evaluate quality of keyword extraction methods we use 1590 keywords from the manually constructed thesaurus $W_{key}$. We assess the results with two measures of quality: Precision and Recall. Precision is defined as the number of relevant words retrieved by a keyword extraction method divided by the total number of words retrieved by that method
\begin{equation}
Precision = \frac{{\left| { {{W_{key}}}  \cap  {{\hat{W}_{key}}} } \right|}}
{{\left| { {{\hat{W}_{key}}} } \right|}}
\end{equation}

Recall is defined as the number of relevant keywords retrieved by a keyword extraction method divided by the total number of existing relevant words (which should have been retrieved):
\begin{equation}
Recall = \[\frac{{\left| { {{W_{key}}}  \cap  {{\hat{W}_{key}}} } \right|}}
{{\left| { {{W_{key}}} } \right|}}
\end{equation}

\begin{table}
\label{tab:keywords}
\caption{Evaluation keywords extraction methods}
	\centering
		\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|}
			\hline 
			Filter & No & \multicolumn{3}{|c|}{Frequency} & \multicolumn{3}{|c|}{``Global'' TF-IDF} & \multicolumn{3}{|c|}{``Local'' TF-IDF} \\  \hline \hilne
			$x$ & 100\% & 10\% & 15\% & 33\% & 10\% & 15\% & 33\% & 10\% & 15\% & 33\% \\ \hline
			Recall & 92	& 62	& 74	& 87	& 62	& 73	& 87	& 24	& 24	& 24 \\ \hline
			Precision & 2	& 13	& 10	& 5	& 13	& 10	& 5	& 17	& 17	& 17 \\ \hline
			$|\hat{W}_{key}|$ & 73513	& 7351	& 11027	& 24259	& 7351	& 11027	& 24259	& 2198	& 2198	& 2198 \\ \hline
	
		\end{tabular}
\end{table}

The results of our experiments are presented in the Table~\ref{tab:keywords}. The very first thing we can learn from the table is that the linguistic filter passes only 92\% of relevant words. Secondly, one can see that the longer toplist (the bigger the $x$), the higher the recall, and the lower the precision of a statistical filter. These first results show us that ranking with frequency provides higher recall, while local TF-IDF shows better precision. 

%-----------------------------------------------------------
\section{Extracting key phrases}\label{key:keyphrases}

Key concepts of a given domain could be represented with  words (mostly nouns) like ``aeroport'' or with noun phrases such as ``personnel de l'aeroport''. 
At this stage we would like to identify set of noun phrases $\hat{NP}_{key}$ which are assigned to the key concepts of the domain. For instance, for the domain, ``Transport'' we might be interested in noun phrases like ``centre de formation pour conducteur'', ``circulation aerienne'' or ``permis international de conduire'' and not in the phrases, like ``heure actuelle'', ``dates fixes'' or ``produit laitier''. We can evaluate quality of the extracted nominal phrases by comparing them with the set of phrases from the manually constructed thesaurus $NP_{key}$. The method for key phrase extraction comprise the following steps:

\begin{enumerate}
	\item Extracting noun phrases from the corpus
	\item Grouping phrases, based on their surface
	\item Filtering phrases based on the statistical information
\end{enumerate}
The following subsections describe in detail every step of this approach. 

%-----------------------------------------------------------
\subsection{Extracting noun phrases from the corpus}

A noun phrase (NP) is a constituent of a sentence that consists of a noun and any modifiers it may have, a noun clause, or a word, such as a pronoun, that takes the place of a noun \cite{collins}. At this step we extract word combinations with help of the finite state transducer, depicted on the Figure~\ref{fig:transducer}. The transducer defines local grammar which recognizes word combinations that are described along the paths between the initial and the final states. Ideally, this local grammar has to generate all French noun phrases we are interested in. The transducer implicitly encodes information about various lexical patterns which the French noun phrases could match. For instance, it contains the path <N><A> which corresponds with the lexical pattern \textit{Noun+Adjective}. This pattern let us retrieve noun phrases such as ``communaut� europe�nne'' or ``administration centrale''. Similarly, the path  <N><A><N> defines the lexical pattern \textit{Noun+Adjective+Noun} which let us retrieve noun phrases like ``�lections communales ordinaires''. 

\begin{figure}
	\centering
		\includegraphics[width=1.2\textwidth]{pics/transducer.eps}
	\caption{ Finite state transducer for extracting noun phrases. <N> - noun, <A> - adjective, <E> - empty sequence.}
	\label{fig:transducer}
\end{figure}

The transducer detects various noun phrases, both the short ones such as ``capitale europeenne'' and the long ones such as ``s�cretaire de legation au Ministere des Affaires Etrangeres''. We use the Unitex software \cite{unitex} to work with this kind of transducers.

The described procedure returns set of noun phrases $NP_i$ for every document $doc_i$. For example, it will identify the following nominal phrases in the sentence ``Dans ce contexte j'aimerais poser les questions suivantes aux Ministres comp�tents'':
\begin{equation}
\label{eq:npsnt}
{NP}_{snt} = \{ 	\text{ministres competents}, \text{questions suivantes} \}.
\end{equation}
The output of this stage is set of noun phrases, extracted from the whole corpus:
\begin{equation}
NP = \bigcup^{|DOC|}_{i=1}NP_i = \{ np_1,np_2,... \}
\end{equation}
We have extracted around 155.800 different noun phrases from our corpus of political texts. 
 
%-----------------------------------------------------------
\subsection{Grouping phrases, based on their surface}

At the previous step we have extracted big number of nominal phrases. The goal of the following steps is to select form them the most salient ones. To do this, firstly we perform grouping of the phrases by we finding $K$ longest common substrings ${NP}^*$. For instance, the automatically extracted phrases ``expertise de publications scientifiques'', ``objet de publications scientifiques'', ``publications scientifiques'' and ``publications scientifiques du type'' could be grouped by the phrase ``publications scientifiques''. 

To solve the $K$-common substrings problem efficiently we use the data structure called \textit{generalized suffix tree}. The suffix tree for a string $S$ is a tree whose edges are labeled with strings, such that each suffix of $S$ corresponds to exactly one path from the tree's root to a leaf \cite{gusfield}. An example of the suffix tree for string ``xabxac'' is depicted on the Figure~\ref{fig:suffixtree}. We use the Ukkonen's algorithm to construct the suffix tree efficiently \cite{ukkonen} and group the noun phrases with help of the algorithm~\ref{alg:grouping}. 
\begin{figure}
	\centering
		\includegraphics[width=0.6\textwidth]{pics/suffixtree.eps}
	\caption{ Suffix tree for the string ``xabxac''.}
	\label{fig:suffixtree}
\end{figure}

%---------------------------------------------------------------------

\begin{algorithm}
\caption{Grouping of noun phrases}
\label{alg:grouping}
\algsetup{indent=3em}
\begin{algorithmic}[1]

		\REQUIRE $NP=\{{np}_1,{np}_2,...,{np}_N\}$ set of noun phrases\\
    \COMMENT{Lemmatizing noun phrases}
  	\STATE ${NP}^{LEM} \leftarrow \emptyset$
  	\STATE ${NP}^{LEM} \leftarrow \bigcup_{np \in NP} \textsc{Lemmatize}(np) $
  	
  	\COMMENT{Concatenating the lemmatized noun phrases}
  	\STATE $unp \leftarrow \sum_{np \in NP^{LEM}}(np + ``\$'')$
  	
  	\COMMENT{Building the suffix tree}
  	\STATE $T \leftarrow \textsc{CreateSuffixTree}(unp)$
  	
  	\COMMENT{Calculating the occurrence matrix $\mathbf{O}$}
  	\FORALL{${np}_i \in {NP}^{LEM}$}
  			\STATE ${GP}_i \leftarrow \textsc{T.FindOccurences}({np}_i)$
  			\FORALL{${np}_j \in {GP}_i$}
  					\STATE $o_{ij} \leftarrow |{np}_i|$   			
        \ENDFOR
    \ENDFOR
    
    \COMMENT{Calculating the longest common substring}
    \STATE ${NP}^* \leftarrow \emptyset$   			
    \REPEAT 
    		\STATE ${np}^*_j \leftarrow \operatorname*{arg\,max}_{j = \overline{1,N}} \sum^N_{i=1}o_{ij}$
    		\STATE ${NP}^* \leftarrow {NP}^* \cup {np}_j^*$   			
    		
    		\COMMENT{Set to zero columns}
    		\FORALL{${np}_i \in {GP}_j$}
    				\FOR{$k=1$ to $N$}         
     						\STATE $o_{ki} \leftarrow 0$   			
     				\ENDFOR				
    		\ENDFOR
    \UNTIL{$\sum^N_{i=1}o_{ij} > 0 $}
    
    \COMMENT{Calculating  the phrase groupings}
    \FORALL{${np}_i \in {GP}_j$}
    		\IF{${np}_i \notin {NP}^* $} 
    				\STATE ${GP}_i \leftarrow \emptyset$   			
    		\ENDIF    		
    \ENDFOR
    \RETURN ${NP}^*, \{{GP}_1,{GP}_2, ...,{GP}_N\}$
\end{algorithmic}
\end{algorithm}
First of all the algorithm lemmatizes the noun phrase and deletes stopwords from them (lines 1-2). Then the procedure merges all the phrases in one string (line 3) and constructs a generalized suffix tree from this string\cite{ukkonen} (line 4). Thereafter, the algorithm calculates the occurrence matrix $\mathbf{O}$ (lines 5-10). An element of this matrix $o_{ij}$ equals to the length of the phrase ${np}_i$ if this phrase is substring of the phrase ${np}_j$:
\begin{equation*}
o_{ij} = \left\{
\begin{array}{rl}
|{np}_i| & \text{if } {np}_i \subseteq {np}_j,\\
0 & \text{else }.
\end{array} \right.
\end{equation*}
At the next step the algorithm calculates set of the longest common substrings ${NP}^*$ based on the information from the occurrence matrix (lines 11-20). Thereafter the algorithm deletes all the groups which are not grouped by a longest common substring (lines 21-25). Finally, the algorithm returns $K$ noun phrases which group other phrases 
\begin{equation}
{NP}^K = \{ {np}_1 ,...,{np}_K  \}.
\end{equation}
Furthermore, the algorithm returns a phrase group ${GP}_{i}$ for every phrase ${np}_i \in NP$:
\begin{equation}
{GP}_i = \{np: np \in NP \wedge {np}_i \subseteq np \}
\end{equation}
Some of phrase groups which were extracted from our corpus of political texts are listed in the App.~\ref{app:key}. 

%-----------------------------------------------------------
\subsection{Filtering phrases based on the statistical information}

At this step we rank noun phrases $NT$ with help of the statistical information. We introduce notion of the \textit{grouping coefficient}. The \textit{grouping coefficient} of a nominal phrase shows how good the phrase groups other phrases. The grouping coefficient $g_i$ of the noun phrase ${np}_i$ is in direct proportion to the number of phrases which groups phrase $|{GP}_i|$ and the length of the phrase ${np}_i$:
\begin{equation}
g_i= \log(|{np}_i|) \cdot |{GP}_i|
\end{equation}
We rank the noun phrases with the modified TF-IDF formula which takes into account the grouping coefficients:
\begin{equation}	
\label{eq:grouptfidf}
rank_{i} = g_i \frac{{{n_i}}} {{\sum\limits_{j = 1}^{|W|} {{n_j}} }}\log \left( {\frac{{\left| {DOC} \right|}} {{\left| {\left\{ {\left. {doc_k:{w_i} \in W_k} \right\}} \right.} \right|}}} \right)	.
\end{equation}	
The formula ~\eqref{eq:grouptfidf} take into account global frequency of the noun phrase $n_i$, its specificity via the inverse document frequency, and its \textit{generality} via the grouping coefficient $g_i$. The set of key phrases ${NP}_{key}$ is the first $x\%$ of words from the ranked list of phrases:
\begin{equation}	
\label{eq:thresholdphrases}
\hat{NP}_{key} = \left\{ np_i: i \geq \frac{x}{{100}}|W| \right\} .
\end{equation}
We obtained 15.580 noun phrases for our dataset with the described phrase extraction procedure. The first 10 noun phrases form this list are the following: ``conseil d'etat'', ``projet de loi'', ``r�glement grand-ducal'', ``union europ�enne'', ``etat membre'', ``chambre de commerce'', ``chambre des deputes session ordinaire'', ``projet de r�glement grand-ducal'', ``parlement europ�en'', ``question parlementaire''. 

%-----------------------------------------------------------
\section{Conclusion and future work}\label{key:conclusion}

We have implemented the baseline methods for keywords and key phrases extraction. The first experiments have shown that its hard to collect exactly the same vocabulary as in the manually constructed thesaurus. The main reason is in principal differences between technology of manual and automatic vocabulary collection. The main directions for the future research in  are the following: 
\begin{itemize}
	\item Thoroughly evaluate quality of the phrase extraction procedure
	\item Try noun phrases extraction procedure, based on syntactic analysis \cite{syntacticnp}
	\item Try noun phrases extraction, based on the Machine Learning techniques \cite{crfparsing}, \cite{svmchunking} or \cite{hmmparsing}
	\item Cluster documents by domain, before term extraction
\end{itemize}
