\chapter{Introduction}\label{introduction}

This document describes the research conducted by Alexander Panchenko  from 1st April 2009 till 28th February 2010 at Center for Natural Language Processing, UCL \cite{cental} in the framework of the research program of Wallonie-Bruxelles International. The document gives an overview of the methodology, reports the progress in the research program and presents plan for the future research. \\

%-----------------------------------------------------------
\section{Context and the problem}\label{introduction:context}

Today every contemporary enterprise needs a sustainable information system for supporting its business needs whatever the specialization of the company. Fast and efficient access to the business data and information plays an invaluable role in the market competition. One of the well-established ways to organize corporate information and power the enterprise-level information retrieval system is to use semantic resources, such as thesauri, reflecting the main concepts of the company's domain of expertise. Such a semantic resource provides a normalized vocabulary for the indexing procedure, allow for better navigation and categorization of the document collection. Furthermore it could be employed for reformulation of the search queries (see Fig.~\ref{fig:application}).
\begin{figure}
	\centering
		\includegraphics[width=0.9\textwidth]{pics/application.eps}
	\caption{Using thesaurus in a corporate information seeking system.}
	\label{fig:application}
\end{figure}

The technology of manual thesaurus construction involve big amount of manual labor to detect main key concepts and relations between them. The manual technology is known to be very time consuming, costly and does not allow for an easy way to keep such resources aligned with the document collection. One of the solutions for this problem is to \textit{automatize} some routines of the documentalist with help of the decision support system -- Automated Thesaurus Construction system (see Fig.~\ref{fig:technology}). The goal of the automatized technology is to discover set of \textit{canditate} key concepts and \textit{candidate} semantic relations between them. In this case the information professional would need to simply refine the automatically constructed resource instead of creating it from scratch. 

\begin{figure}
	\centering
		\includegraphics[width=0.9\textwidth]{pics/technology.eps}
	\caption{Technology of manual and automatized thesaurus construction.}
	\label{fig:technology}
\end{figure}

The objective of this research is to investigate novel techniques for mining semantic information from corporate text corpora and use it for fulfilling information retrieval needs. The main goal of this research is to streamline the procedure of constructing and maintaining the corporate semantic resources in order to improve the quality of the information seeking for numerous domains, especially the ones concerned by the �Marshall plan�. We plan to transfer the results of this research to the industry by integrating them into corporate information seeking systems.
 
The formal definition of the problem we are solving is as follows: given a  text document collection $DOC=\{doc_1,doc_2,...\}$ construct a \textit{semantic network} $\hat{T}=(\hat{C},\hat{R})$ consisting of vertices $\hat{C}=\{c_1,c_2,...\}$, representing key concepts from the document collection, and edges $\hat{R}=\{r_1,r_2,...\}, r=\left\langle c_i,c_j \right\rangle$ representing semantic relations between those concepts. We would like to choose among all the possible semantic networks $NETS = \{ \hat{T}_1,\hat{T}_2,... \}$ the one which is as close to the manually constructed semantic network (thesaurus) $T=(C,R)$ as possible:
\[
\hat{T}^{*}= \operatorname*{arg\,max}_{\hat{T}_i \in NETS} Similarity(T,\hat{T}_i)
\]
Another important constraint is that the method must be domain-independent: it have to discover new semantic knowledge and not \textit{only} rely on some base of predefined semantic relations. At the time, we focus on analysis of texts in French, with goal in mind to extend our approach to English and Russian languages. To solve the defined problem we propose an information technology of processing text documents. It involves various Natural Language Processing techniques and comprise the following main steps (see Fig.~\ref{fig:steps}):
\begin{enumerate}
	\item Preprocessing text documents  
	\item Extracting key concepts from corpus
	\item Editing the concepts
	\item Calculation of semantic relations between the concepts	
	\item Editing the semantic relations	
\end{enumerate}

Finally, we would like to note that the technology of semantic network induction from text could be used in various different contexts: for word sense disambiguation \cite{disambiguation}, for visualization of text document collections \cite{BlazDocAtlas}, for powering the domain-specific search engines \cite{ir_application} or for improving navigation on the document collection \cite{Ruan200083}. 
 
\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{pics/steps.eps}
	\caption{Technology of automatized thesaurus construction.}
	\label{fig:steps}
\end{figure} 
 
%-----------------------------------------------------------
\section{Contents}\label{introduction:contents}

The main body of this report is divided as follows: the second chapter introduces the dataset we are working with. The third chapter describes our method for key concepts extraction. In the fourth chapter we discuss the approach for discovering semantic relations between key concepts. Finally, the last chapter briefly overviews the progress in the doctoral program, coupled with this research program.

