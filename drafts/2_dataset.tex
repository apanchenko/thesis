\chapter{Dataset }\label{dataset}

The dataset we are working with comes from a real document management project of one Belgian enterprise. It comprise $n=11.386$ political text documents in French and a manually constructed semantic resource (thesaurus). We have chosen this corpus because it allow for automatic evaluation of our algorithms with help of the predefined semantic resource. This chapter describes the contents and structure of this dataset. The first part of the dataset is a collection of  text documents 

\begin{equation}
DOC = \bigcup^{n}_{i=1} doc_{i}.
\label{eq:doc}
\end{equation}

The average document length in the corpus approximately equals to 2500 words (about $\sim$10 pages), and the median of this value equals to 420 words (about one page). The distribution of the document size in the collection is depicted on the Fig.~\ref{fig:doclengths}. The corpus contains set of $l=149,505$ distinct \textit{lemmanized} words $w$ and $20,665,146$ tokens:
\begin{equation}
\label{eq:tokens}
W = \bigcup^{l}_{i=1} w_{i}
\end{equation}

\begin{figure}
	\centering
		\includegraphics[width=1.5\textwidth]{pics/tokens_hist.eps}
	\caption{Distribution of document size in the collection.}
	\label{fig:doclengths}
\end{figure}
 
The corpus consist of documents of governmental institutions, such as requests of deputies to ministers, protocols of parliamentary sessions, activity reports etc. The most common types of documents in the corpus are the following: written question, international convention, regulation, activity reports, and proposition of new law. The following paragraph presents one short anonymized text document from the dataset:

\begin{lstlisting}
Monsieur xxx,
		Par la pr�sente je vous prie de bien vouloir transmettre la
question parlementaire suivante � Monsieur Ministre de
l'Int�rieur.
		La Ville de xxx se propose de r�aliser, et ce contrairement
� la volont� de la population indig�ne, une communication directe
entre la rue de xxx et le parking sis au xxx, moyennant un
pont routier sur l'xxx.
		Un tel ouvrage traversera une des plus importantes zones inondables
et zones de r�tention sur le territoire de la commune de
xxx, indiqu�es sur le plan d'am�nagement partiel d�clar�
obligatoire par le r�glement du xx janvier xxxx.
		Dans ces conditions, j'aimerais savoir si le projet en question est
r�alisable � d�faut d'une modification du plan pr�d�crit, �tant
donn� qu'une telle construction ne saurait �tre consid�r�e comme
construction nouvelle ayant pour finalit� de combler une lacune
dans le tissu urbain comme existant �, au sens de l'article xxx
r�glement xxx.
		Veuillez agr�er, Monsieur xxx, l'expression de mes
sentiments distingu�s.
xxx
D�put�.
\end{lstlisting}

The second part of the dataset contains the domain-specific thesaurus, constructed based on the analysis of the same corpus. The thesaurus contains $k=2514$ \textit{concepts}  $C=\{c_{1},...,c_{k}\}$; every concept $c_{i}$ represented with a set of synonyms $\{d_{i1},...,d_{ij}\}$, also called \textit{descriptors} in this context. For example, the concept ``Aeronef'' comprised of eight descriptors looks like as follows (see  Table~\ref{tab:concept}): 

\begin{equation}
c_{i}= \{d_{i1},...,d_{i8}\} =\{"Aeronef",...,"Dirigeable"\}.
\end{equation}
    
\begin{table}
    \centering
    \begin{tabular}{ | l | l|}
    \hline
    \multicolumn{2}{|c|}{ Descriptors of the ``Aeronef'' concept ($c_{i}$) } \\ \hline \hline
    Aeronef & $d_{i1}$ \\  \hline
		Aeronef ultra-leger motorise & $d_{i2}$ \\  \hline
		Aerostat & $d_{i3}$ \\  \hline
		Avion & $d_{i4}$ \\  \hline
		Ballon a air chaud & $d_{i5}$ \\  \hline
		Planeur leger & $d_{i6}$ \\  \hline
		Objet spatial & $d_{i7}$ \\  \hline
		Dirigeable & $d_{i8}$ \\  \hline
     
    \end{tabular}
    \caption{Example of a concept from the thesaurus.}
		\label{tab:concept}
\end{table}

The descriptors compose the key part of the thesaurus -- its vocabulary; they reflect key concepts of the domain. The vocabulary $D$ of the thesaurus comprise $m=4771$ descriptors:
\begin{equation}
\label{eq:voc}
D=\bigcup_{i=1}^m c_{i}=\{ d_{1},...,d_{m} \}
\end{equation}
Most of the descriptors (65\%) are noun phrases, like ``aeronef ultra-leger motorise'' or ``ballon a air chaud'', and the rest 35\% are nouns, like ``avion'' or ``aerostat''. We will denote the monolexical part of vocabulary as $W_{key}$ and the polylexical part as $NP_{key}$, therefore set of all descriptors comprises union of the monolexical and the polylexical parts
\begin{equation}
D = W_{key} \cup P_{key}
\end{equation}

The  concepts are organized hierarchically 2456 hyponymy/hyperonymy relations 
\begin{equation}
NT=\{nt_{1},...,nt_{2456}\},
\end{equation}
 where every relation is an ordered pair of concepts $nt_{i}= \left\langle c_{i1},c_{i2} \right\rangle ,c_{ij} \in C$. Figure~\ref{fig:nthierarchy} depicts several concepts of the thesaurus, interconnected with the hierarchical relations. The concepts are also linked with the 1530 associative relations $RT=\{rt_{1},{rt}_2,...\}$. The oriented graph  $T=(C,R)$ composed of set of edges $R&=RT \cup NT$ and set of nodes $C$ forms a \textit{network}. 

The concepts of the thesaurus comes from the 12 various domains; the terms of every domain are grouped in the so-called \textit{microthesauri}. Additional information about the domains and the microthesauri is presented in the Appendix~\ref{microthesauri}. 

\begin{figure}
	\centering
		\includegraphics[width=1.2\textwidth]{pics/nthierarchy.eps}
	\caption{Concepts hierarchy.}
	\label{fig:nthierarchy}
\end{figure}

Finally, we would like to mention that this dataset will not be the only one we would like to work with. The main limitations of this dataset are its privateness and relatively small size. That is why the next step in this research will involve experimenting with one of the freely available thesaurus e.g. EuroVOC\cite{eurovoc}, AgroVOC\cite{agrovoc} or EuroWordNet\cite{eurowordnet} and bigger text collections.
