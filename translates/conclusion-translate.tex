\chapter{Conclusion}


This dissertation explored several strategies to semantic relation extraction with similarity measures. Such measures are designed to quantify semantic relatedness of lexical units, such as nouns, noun phrases and multiword expressions. The measures assign high values for the pairs of terms in a semantic relation (synonyms, hypernyms or co-hyponyms) and near-zero values for all other pairs. 

%Three measures described in this chapter, perform comparably to the  baselines, each with its pros and cons in terms of precision and coverage. We conclude that one way to significantly improve over the baselines could be to use the complementarity of different measures. This idea is developed in the next chapter.   

%Chapter 1 begins with a description of the \textit{object} of the research -- semantic relations and resources. First, we define these objects formally and provide examples of resources commonly used in text processing systems, such as taxonomies, thesauri, lexical databases, and ontologies. Second, the \textit{subject} of the research is introduced -- semantic relation extractors based on similarity measures. Finally, the chapter presents benchmarks designed to assess performance of this kind of extraction systems.  
 
This work brings several contributions to the field of computational lexical semantics: 

\begin{enumerate}
  \item Section~\ref{similarity-measures-based-on-syntactic-distributional-analysis} presented a new similarity measure based on the syntactic distributional analysis. The measure performs comparably to the baselines, but in contrast to the similar approaches, it can deal with both single words and multiword expressions. We compared  relations extracted with this measure with relations of a thesaurus. A small fraction (7\%) of the extracted relations is explicitly encoded in the thesaurus. More extracted relations (35\%-46\%) are implicitly present in the resource.

\item Sections~\ref{similarity-measure-based-on-definitions} and~\ref{definition-based-measures} presented a new semantic relation extraction method, which relies on the texts of Wikipedia articles, k-nearest neighbors procedures $k$-NN and $mk$-NN, and similarity measures Cosine and Jaccard Index. The methods performs comparably to the baselines, but operates on a small-scale set of definitions. The proposed techniques are efficiently implemented in an open source system.~\footnote{\url{https://github.com/jgc128/serelex}}

\item Section~\ref{similarity-measures-based-on-syntactic-distributional-analysis} describes a novel corpus-based semantic similarity measure \textit{PatternSim} based on lexico-syntactic patterns. Experimental results have shown that our  measure performs comparably to the baseline measures, but requires no semantic resources such as WordNet or dictionaries. Implementation of the proposed approach has been made available to the community.~\footnote{\url{https://github.com/cental/patternsim}}

\item Chapter 3, presents a large-scale comparative study of 34 baseline similarity measures based on corpora, definitions and networks. We go further than most of the surveys and compare the measures with respect to the semantic relation types they provide. Evaluation system used in these experiments has been made available to the community.~\footnote{\url{https://github.com/alexanderpanchenko/sim-eval}} The main findings of this study are the following: 
\begin{itemize}
\item First, the \textit{BDA-3-Cos} and the \textit{SDA-21-Cos} measures provided the best performance among corpus-based measures. The \textit{Resnik} measure performed best among the network-based measures. The \textit{WktWiki-1000} scored best in the group of definition-based measures. All these measures provide the scores which let clearly separate correct relations from the random ones.

\item Second, semantic relation distributions of different measures  are significantly different. However, all measures extract many co-hyponyms. 

\item Third, the studied measures are highly heterogeneous in terms of their lexical coverage, performances, and semantic relation distributions. There is no single measure which outperforms all others on  all benchmarks.

\end{itemize} 

\item Chapter 4 provides two main contributions:

\begin{itemize}
  \item First, a systematic analysis of combinations of 16 baseline measures with 8 fusion methods and 3 techniques for the combination set selection. We are first to propose hybrid similarity measures based on all main types of resources -- corpora, Web corpus, semantic networks, dictionaries, and encyclopedias. 
  
  \item Second, a hybrid similarity measure \textit{Logit-E15}, which combines 15 baseline measures in a statistical model trained on a set of semantic relations from a dictionary. According to our experiments, outperforms by a large margin both baselines  and combined measures on all the benchmarks.

\end{itemize}

\item Chapter 5 presents two text processing systems which use the semantic similarity measure \textit{PatternSim}: 

\begin{itemize}
\item The first system is a lexico-semantic search engine, which help users discover semantically similar words easily and interactively.~\footnote{\url{http://serelex.cental.be}}~\footnote{\url{http://apps.microsoft.com/webpdp/en-US/app/lsse/48dc239a-e116-4234-87fd-ac90f030d72c}} For a given input query the system returns a list of related terms and visualizes them with a graph or a set of images. Building of such system would not be possible without semantic similarity measures. Implementation of the system  has been made available to the community.~\footnote{\url{https://github.com/pomanob/lsse}}~\footnote{\url{https://github.com/jgc128/serelex4win}}  

\item The second system performs categorization of filenames on P2P networks to detect child sexual abuse media. In contrast to the first application, in this case, a similarity measure refines a baseline system. We show that the extracted  relations  improve the accuracy of the application with the help of the \textit{vocabulary projection} technique.  Implementation this text classification system has been made available to the community.~\footnote{\url{https://github.com/alexanderpanchenko/stc}} 
 \end{itemize}
  
\end{enumerate}

This thesis improved understanding of the existing approaches to semantic similarity and proposed several new ones. These novel techniques perform well according both to extrinsic and intrinsic evaluations. We conclude that the developed measures can be useful in a wide range of natural language processing and information retrieval applications.   










