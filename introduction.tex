\chapter{Introduction}

\begin{flushright}

\textit{ La dernière chose qu'on trouve en laissant un ouvrage \\
 est de savoir celle qu'il faut mettre la première.} 

-- Blaise Pascal, \textit{Pensée}, 1670
\end{flushright}

Semantic relations, such as synonyms, hypernyms and co-hyponyms proved to be useful for text processing applications, including text similarity, query expansion, question answering and  word sense disambiguation. Such relations are practical because of the gap between lexical surface of the text and its meaning. Indeed,  the same concept is often represented by different terms. However, existing resources often do not cover a vocabulary required by a given system. Manual resource construction is prohibitively expensive for many projects. On the other hand, precision of the existing extractors still do not meet quality of the handcrafted resources. All these factors motivate the development of novel extraction methods. 

This thesis deals with similarity measures for semantic relation extraction. The main research question we address, is how to improve precision and coverage of such measures. First, we perform a large-scale study the baseline techniques. Second, we propose four novel measures. One of them significantly outperforms the baselines, the others perform comparably to the state-of-the-art techniques. Finally, we successfully apply one of the novel measures in two text processing systems.

Chapter 1 begins with a description of the \textit{object} of the research -- semantic relations and resources. First, we define these objects formally and provide examples of resources commonly used in text processing systems, such as taxonomies, thesauri, lexical databases and  ontologies. Second, we introduce the \textit{subject} of the research -- semantic relation extractors based on similarity measures. Finally, the chapter presents benchmarks designed to assess performance of this kind of extraction systems.  

Chapter 2 deals with semantic similarity measures which rely on one resource (a corpus, a dictionary, etc.) and one extraction method (distributional analysis, lexico-syntactic patterns, etc.). The chapter begins with an overview of related work. Then, we describe three experiments. In the first one, we propose a new similarity measure \textit{SDA-MWE}, which stems from the syntactic distributional analysis, and  apply it to the task of automatic thesaurus construction. The second one presents a new similarity measure \textit{DefVectors} based on definitions.  Finally, in the third experiment, we propose a new similarity measure \textit{PatternSim}, which extracts ``definitions'' from a huge corpus with lexico-syntactic patterns. Three measures described in this chapter, perform comparably to the  baselines, each with its pros and cons in terms of precision and coverage. We conclude that one way to significantly improve over the baselines could be to use the complementarity of different measures. This idea is developed in the next chapter.  

Chapter 3 evaluates a wide range of baseline semantic similarity measures to identify their systematic advantages and disadvantages. The existing measures differ both in the kinds of information they use and in the ways this information is transformed into a similarity score. First, in this chapter, we present a comparative study of heterogeneous baseline measures. Several authors already compared existing approaches, but we perform a study on a large scale, as we compare 37 similarity measures based on semantic networks, text corpora,  Web as a corpus, dictionaries and encyclopedia. Second, we go further than most of the surveys and compare the measures with respect to the semantic relation types they provide (hypernyms, meronyms, etc.). Our results suggest that the studied approaches are highly heterogeneous in terms of  precision, coverage and semantic relation distributions. We address the problem of measure combination  in the next chapter.

Chapter 4 describes several hybrid semantic similarity measures combining evidence from complementary sources. First, in this chapter, we present a systematic analysis of 16 baseline measures combined with 9 fusion methods and 3 measure selection techniques. Some attempts were already made to combine the baseline measures to improve the performance. However, we are first to propose hybrid similarity measures based on all main types of resources -- semantic networks, text corpora, Web as a corpus, dictionaries and encyclopedia. Second, we describe several novel hybrid measure which combine 15 baselines  with the supervised learning:  \textit{Logit-E15}, \textit{C-SVM-linear-E15}, \textit{C-SVM-radial-E15}, etc. They outperform all tested single and unsupervised hybrid measures by a large margin. Our results show that measures based on complementary sources of information indeed significantly outperform the baselines measures. 

Chapter 5 presents two applications of semantic similarity measures to text processing. Both systems rely on the \textit{PatternSim} measure introduced in Chapter 2. First, we describe \textit{Serelex}, a system that given a query, provides a list of related terms and displays them in a form of an interactive graph or a set of images. Second, we describe a new short text categorization system, developed for processing filenames of P2P networks. We show that the relations extracted with the similarity measure \textit{PatternSim} improve the accuracy of the classification with the help of the \textit{vocabulary projection} technique. Finally, in this chapter,  we provide a list of further text processing applications, where semantic similarity measures may be  useful. We conclude that the developed semantic similarity measures can  indeed be practical for the real text processing systems.   







