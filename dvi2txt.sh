#!/bin/bash
catdvi -e 1 -U thesis.dvi | sed -re "s/\[U\+2022\]/*/g" | sed -re "s/\[U\+....\]/ /g" | sed -re "s/([^^[:space:]])\s+/\1 /g" | sed -re "s/\s+/ /g" > txt
