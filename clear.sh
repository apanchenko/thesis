#!/bin/bash
echo Deleting all LaTex output in this folder such as bld, log, dvi, pdf, bbl etc.
rm -f *.blg
rm -f *.log
rm -f *.dvi
rm -f *.bbl
rm -f *.aux
rm -f *.pdf
rm -f *.cb
rm -f *.synctex.gz
rm -f *.toc
rm -f *.out
rm -f *.cb2
rm -f *.bbl
rm -f *.lof
rm -f *.lot
rm -f ./tmp/*
echo Done.
